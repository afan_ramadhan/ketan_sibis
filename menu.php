<?php
$mod = (isset($_GET['mod']) ? $_GET['mod']: "");

$levelUser = mysql_query("SELECT id_level FROM user WHERE id = '$_SESSION[id]'");
$ambilLevelUser = mysql_fetch_array($levelUser);

$menuAkses = mysql_query("SELECT menu.nama_menu, hak_akses.r FROM hak_akses LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE id_level = '$ambilLevelUser[id_level]'");
while($ambilMenuAkses = mysql_fetch_array($menuAkses))
{
	$view[$ambilMenuAkses['nama_menu']] = $ambilMenuAkses['r'];
}
?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<?php
				if($ambil_data_adminweb['gambar']=="")
				{
					echo "<img class='img-circle' src='images/user_kosong.jpg'/>";
				}
				else
				{
					echo "<img class='img-circle' src='images/user/$ambil_data_adminweb[gambar]'/>";
				}
				?>
			</div>
			<div class="pull-left info">
				<p><?=$ambil_data_adminweb['nama_lengkap'];?></p>
				<small><?=$ambil_data_adminweb['nama_level'];?></small>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header" style="text-align: center;"><?=strtoupper($lihat_konfigurasi['nama_aplikasi']);?> MENU</li>
			<li <?php if(!isset($_GET['mod'])){echo "class='active'";} ?>><a class="loading" href="index.php"><i class="fa fa-dashboard" style="margin-right: 10px;"></i><span>Dashboard</span></a></li>
			<li class="treeview <?php if($mod == "jurusan" or $mod == "tahun_ajar" or $mod == "kurikulum" or $mod == "rombel" or $mod == "siswa"){echo "active menu-open";} ?>" style="<?php if((!isset($view['jurusan']) or $view['jurusan'] == 0) and (!isset($view['tahun_ajar']) or $view['tahun_ajar'] == 0) and (!isset($view['kurikulum']) or $view['kurikulum'] == 0) and (!isset($view['rombel']) or $view['rombel'] == 0) and (!isset($view['siswa']) or $view['siswa'] == 0)){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-briefcase" style="margin-right: 10px;"></i><span>Master</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "jurusan"){echo "class='active'";} ?> style="<?php if(!isset($view['jurusan']) or $view['jurusan'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=jurusan"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Jurusan</span></a></li>
					<li <?php if($mod == "tahun_ajar"){echo "class='active'";} ?> style="<?php if(!isset($view['tahun_ajar']) or $view['tahun_ajar'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=tahun_ajar"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Tahun Ajar</span></a></li>
					<li <?php if($mod == "kurikulum"){echo "class='active'";} ?> style="<?php if(!isset($view['kurikulum']) or $view['kurikulum'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=kurikulum"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Kurikulum</span></a></li>
					<li <?php if($mod == "rombel"){echo "class='active'";} ?> style="<?php if(!isset($view['rombel']) or $view['rombel'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=rombel"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Rombel</span></a></li>
					<li <?php if($mod == "siswa"){echo "class='active'";} ?> style="<?php if(!isset($view['siswa']) or $view['siswa'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=siswa"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Siswa</span></a></li>
				</ul>
			</li>
			<li <?php if($mod == "nilai"){echo "class='active'";} ?> style="<?php if(!isset($view['nilai']) or $view['nilai'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=nilai"><i class="fa fa-percent" style="margin-right: 10px;"></i><span>Nilai Siswa</span></a></li>
			<li <?php if($mod == "data_induk"){echo "class='active'";} ?> style="<?php if(!isset($view['data_induk']) or $view['data_induk'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=data_induk"><i class="fa fa-address-book" style="margin-right: 10px;"></i><span>Data Induk</span></a></li>
			<li class="treeview <?php if($mod == "level" or $mod == "user"){echo "active menu-open";} ?>" style="<?php if((!isset($view['level']) or $view['level'] == 0) and (!isset($view['user']) or $view['user'] == 0)){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-lock" style="margin-right: 10px;"></i><span>Manajemen Akses</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "level"){echo "class='active'";} ?> style="<?php if(!isset($view['level']) or $view['level'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=level"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Level</span></a></li>
					<li <?php if($mod == "user"){echo "class='active'";} ?> style="<?php if(!isset($view['user']) or $view['user'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=user"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>User</span></a></li>
				</ul>
			</li>
			<li <?php if($mod == "riwayat"){echo "class='active'";} ?> style="<?php if(!isset($view['riwayat']) or $view['riwayat'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=riwayat"><i class="fa fa-clock-o" style="margin-right: 10px;"></i><span>Riwayat Aktivitas</span></a></li>
			<li <?php if($mod == "konfigurasi"){echo "class='active'";} ?> style="<?php if(!isset($view['konfigurasi']) or $view['konfigurasi'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=konfigurasi"><i class="fa fa-cogs" style="margin-right: 10px;"></i><span>Konfigurasi</span></a></li>
		</ul>
	</section>
</aside>