<?php
include "../../config/database.php";

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
}
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 0, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Rombel</h4>
</div>
<div class="modal-body">
	<h3 style="margin-top: 0;">Daftar Siswa</h3>
	<div class="scrolling">
		<table class="table table-bordered table-hover data_custom">

			<thead>
				<tr>
					<th style="width: 1px;">No.</th>
					<th>Nama Lengkap</th>
					<th>NIPD</th>
					<th>NISN</th>
					<th>Jenis Kelamin</th>
					<th>Agama</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$data = mysql_query("SELECT * FROM siswa WHERE id_rombel = '$id' ORDER BY siswa.nama_lengkap");
				while($getData = mysql_fetch_array($data))
				{
					$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");
					echo "
					<tr>
						<td align='center'>$x</td>
						<td>$getData[nama_lengkap]</td>
						<td>$getData[nipd]</td>
						<td>$getData[nisn]</td>
						<td>$jenis_kelamin</td>
						<td>$getData[agama]</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
		</table>
	</div>
	
</div>
<div class="modal-footer">
</div>