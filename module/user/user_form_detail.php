<?php
include "../../config/database.php";

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail User</h4>
</div>
<div class="modal-body">
	<table class="table table-stripped table-hover data">
		<tbody>
			<?php
			$data = mysql_query("SELECT user.*, level.nama_level FROM user LEFT JOIN level ON user.id_level = level.id WHERE user.id = '$id'");
			$getData = mysql_fetch_array($data);

			$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");
			
			if($getData['gambar']=="")
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/user_kosong.jpg' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			else
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/user/$getData[gambar]' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			
			echo "
			<tr>
				<th style='width: 1px;'>Nama&nbsp;Lengkap</th>
				<th style='width: 1px;'>:</th>
				<td>$getData[nama_lengkap]</td>
			</tr>
			<tr>
				<th style='width: 1px;'>NIP</th>
				<th style='width: 1px;'>:</th>
				<td>$getData[nip]</td>
			</tr>
			<tr>
				<th>Username</th>
				<th>:</th>
				<td>$getData[username]</td>
			</tr>
			<tr>
				<th>Jenis&nbsp;Kelamin</th>
				<th>:</th>
				<td>".($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan")."</td>
			</tr>
			<tr>
				<th>Tempat&nbsp;Lahir</th>
				<th>:</th>
				<td>$getData[tempat_lahir]</td>
			</tr>
			<tr>
				<th>Tanggal&nbsp;Lahir</th>
				<th>:</th>
				<td>$getData[tanggal_lahir]</td>
			</tr>
			<tr>
				<th>Agama</th>
				<th>:</th>
				<td>$getData[agama]</td>
			</tr>
			<tr>
				<th>Alamat</th>
				<th>:</th>
				<td>$getData[alamat]</td>
			</tr>
			<tr>
				<th>Email</th>
				<th>:</th>
				<td>$getData[email]</td>
			</tr>
			<tr>
				<th>Nomor&nbsp;Telepon</th>
				<th>:</th>
				<td>$getData[nomor_telepon] <a class='btn btn-success btn-sm' href='https://api.whatsapp.com/send?phone=".str_replace("08", "628", substr($getData['nomor_telepon'], 0, 2)).substr($getData['nomor_telepon'], 2)."' target='_blank' style='margin-left: 10px;'><i class='fa fa-whatsapp' aria-hidden='true' style='margin-right: 10px;'></i>Kirim Pesan</a></td>
			</tr>			
			<tr>
				<th>Level</th>
				<th>:</th>
				<td>$getData[nama_level]</td>
			</tr>";
			?>
		
		</tbody>
		
	</table>
	
</div>
<div class="modal-footer">
</div>