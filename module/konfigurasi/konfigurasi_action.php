<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "konfigurasi";

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_instansi = mysql_real_escape_string($_POST['nama_instansi']);
	$kecamatan = mysql_real_escape_string($_POST['kecamatan']);
	$kabupaten = mysql_real_escape_string($_POST['kabupaten']);
	$provinsi = mysql_real_escape_string($_POST['provinsi']);
	$alamat = mysql_real_escape_string($_POST['alamat']);
	$kode_pos = mysql_real_escape_string($_POST['kode_pos']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$email = mysql_real_escape_string($_POST['email']);
	$website = mysql_real_escape_string($_POST['website']);
	$logo = (isset($_FILES['logo']['name']) ? $_FILES['logo']['name'] : "");
	$id_user = mysql_real_escape_string($_POST['id_user']);
	
	if(empty($_POST['nama_instansi']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Instansi Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_instansi = '$nama_instansi', kecamatan = '$kecamatan', kabupaten = '$kabupaten', provinsi = '$provinsi', alamat = '$alamat', kode_pos = '$kode_pos', nomor_telepon = '$nomor_telepon', email = '$email', website = '$website', id_user = '$id_user', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		if(!empty($_FILES['logo']['name']))
		{
			$jenis_file = array("image/jpg", "image/jpeg", "image/png", "image/gif");
			if(in_array($_FILES['logo']['type'], $jenis_file))
			{
				$ambilKonfigurasi = mysql_fetch_array(mysql_query("SELECT * FROM $nama_tabel WHERE id = '$id'"));
				
				if($ambilKonfigurasi['logo'] != "")
				{
					unlink ("../../images/konfigurasi/$ambilKonfigurasi[logo]");		
				}
				
				function UploadFile($logo)
				{
					$vdir_upload = "../../images/konfigurasi/";
					$vfile_upload = $vdir_upload . $logo;
						
					move_uploaded_file($_FILES["logo"]["tmp_name"], $vfile_upload);
				}
						
				$lokasi_file = $_FILES['logo']['tmp_name'];
				$nama_file = $_FILES['logo']['name'];
				$acak = rand(1,99999);
				$nama_file_unik = $acak . $nama_file;
				UploadFile($nama_file_unik);
					
				mysql_query("UPDATE $nama_tabel SET logo = '$nama_file_unik' WHERE id = '$id'");
			}
		}
		
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Konfigurasi";
			$keterangan = mysql_real_escape_string("Memperbarui Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Konfigurasi Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}
?>