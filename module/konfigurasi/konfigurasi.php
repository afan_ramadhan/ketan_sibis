<?php
$nama_menu = "konfigurasi";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Konfigurasi";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$('#tampilData').load("module/konfigurasi/konfigurasi_database.php");
		})
		
		//Edit Data
		function editData(id)
		{
			var mod = "editData";
			$.ajax({
				type	: "POST",
				url		: "module/konfigurasi/konfigurasi_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData(id)
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "perbaruiData");
			data.append("id", id);
			
			data.append("nama_instansi", $("#nama_instansi").val());
			data.append("kecamatan", $("#kecamatan").val());
			data.append("kabupaten", $("#kabupaten").val());
			data.append("provinsi", $("#provinsi").val());
			data.append("alamat", $("#alamat").val());
			data.append("kode_pos", $("#kode_pos").val());
			data.append("nomor_telepon", $("#nomor_telepon").val());
			data.append("email", $("#email").val());
			data.append("website", $("#website").val());
			data.append("logo", $('#logo')[0].files[0]);
			data.append("id_user", $("#id_user").val());
			$.ajax({
				type		: "POST",
				url			: "module/konfigurasi/konfigurasi_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/konfigurasi/konfigurasi_database.php");
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Konfigurasi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Konfigurasi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Konfigurasi</h3>
						</div>
						<div class="box-body">
							<button type="button" class="btn btn-warning btn-md" onclick="editData(1)" style="margin-right: 10px; <?=$u;?>"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i>Edit Konfigurasi</button>	
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog">
									<div id="formContent" class="modal-content"></div>
									<div id="notifikasi"></div>
								</div>
							</div>			
							<br/>
							<br/>  
							<div id="tampilData" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Konfigurasi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Konfigurasi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>