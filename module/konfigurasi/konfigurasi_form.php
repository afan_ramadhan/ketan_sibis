<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM konfigurasi WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Edit Konfigurasi
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Instansi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_instansi" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_instansi'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kecamatan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kecamatan" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['kecamatan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kabupaten</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kabupaten" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['kabupaten'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Provinsi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="provinsi" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['provinsi'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alamat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="alamat" maxlength="250" value="<?php if($_POST['mod']=="editData"){echo $getData['alamat'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kode Pos</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kode_pos" maxlength="250" value="<?php if($_POST['mod']=="editData"){echo $getData['kode_pos'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nomor Telepon</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_telepon" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_telepon'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Email</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="email" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['email'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Website</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="website" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['website'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Logo</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="logo" name="logo"/>
				<p class="help-block">File Gambar.</p>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kepala Sekolah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_user">
					<?php
					$user = mysql_query("SELECT * FROM user ORDER BY nama_lengkap");
					while($getUser = mysql_fetch_array($user))
					{
						$selected = ($getData['id_user'] == $getUser['id'] ? "selected" : "");
					?>
						<option value="<?=$getUser['id'];?>" <?=$selected;?>><?=$getUser['nama_lengkap'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="perbaruiData" onclick="perbaruiData(<?=$getData['id'];?>)"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Perbarui</button>
</div>