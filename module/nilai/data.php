<?php
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;

$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "nilai";
$sTableJoin1 = "siswa";
$sTableJoin2 = "tahun_ajar";
$sTableJoin3 = "kurikulum";
$aColumns = array('id', 'nama_lengkap', 'nipd', 'nisn', 'tingkat', 'semester', 'tahun_ajar', 'nama_kurikulum'); 
$sIndexColumn = "$sTable.id";
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sWhere = "WHERE
	$sTableJoin1.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin1.nipd LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin1.nisn LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin3.tingkat LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin2.semester LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin2.tahun_ajar LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTableJoin3.nama_kurikulum LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%'";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTableJoin1.nama_lengkap, $sTableJoin1.nipd, $sTableJoin1.nisn, $sTableJoin3.tingkat, $sTableJoin2.semester, $sTableJoin2.tahun_ajar, $sTableJoin3.nama_kurikulum
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_siswa = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_tahun_ajar = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.id_kurikulum = $sTableJoin3.id
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_siswa = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_tahun_ajar = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.id_kurikulum = $sTableJoin3.id";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>