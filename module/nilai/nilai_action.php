<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "nilai";

if($_POST['mod']=="simpanData")
{
	$id_siswa = $_POST['id_siswa'];
	$id_tahun_ajar = $_POST['id_tahun_ajar'];
	$id_kurikulum = $_POST['id_kurikulum'];
	
	if(empty($_POST['id_siswa']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_tahun_ajar']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_kurikulum']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$cekNilai = mysql_num_rows(mysql_query("SELECT id_siswa FROM $nama_tabel WHERE id_siswa = '$id_siswa' AND id_tahun_ajar = '$id_tahun_ajar'"));
		if($cekNilai > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Sudah Ada!');
			</script>";
		}
		else
		{
			$siswa = mysql_fetch_array(mysql_query("SELECT nama_lengkap FROM siswa WHERE id = '$id_siswa'"));
			
			$simpanData = mysql_query("INSERT INTO $nama_tabel (id_siswa, id_tahun_ajar, id_kurikulum, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_siswa', '$id_tahun_ajar', '$id_kurikulum', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
				
			if($simpanData)
			{
				$aktivitas = "Tambah Nilai";
				$keterangan = mysql_real_escape_string("Menambahkan '$siswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Ditambahkan!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
				</script>
				";
			}
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_siswa = $_POST['id_siswa'];
	$id_tahun_ajar = $_POST['id_tahun_ajar'];
	$id_kurikulum = $_POST['id_kurikulum'];
	
	if(empty($_POST['id_siswa']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_tahun_ajar']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_kurikulum']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$cekNilai = mysql_num_rows(mysql_query("SELECT id_siswa FROM $nama_tabel WHERE id_siswa = '$id_siswa' AND id_tahun_ajar = '$id_tahun_ajar' AND id_kurikulum = '$id_kurikulum'"));
		if($cekNilai > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Sudah Ada!');
			</script>";
		}
		else
		{
			$siswa = mysql_fetch_array(mysql_query("SELECT nama_lengkap FROM siswa WHERE id = '$id_siswa'"));
			
			$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_siswa = '$id_siswa', id_tahun_ajar = '$id_tahun_ajar', id_kurikulum = '$id_kurikulum', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
				
			if($perbaruiData)
			{
				$aktivitas = "Perbarui Nilai";
				$keterangan = mysql_real_escape_string("Memperbarui '$siswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Diperbarui!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
				</script>
				";
			}
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT id_siswa FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['id_siswa'];
	
	$siswa = mysql_fetch_array(mysql_query("SELECT nama_lengkap FROM siswa WHERE id = '$field'"));
	
	$hapusNilaiDetail = mysql_query("DELETE FROM nilai_detail WHERE id_nilai = '$id'");
		
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Nilai";
		$keterangan = mysql_real_escape_string("Menghapus '$siswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Nilai Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Nilai Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="simpanNilaiDetail")
{
	$id_nilai = $_POST['id_nilai'];
	
	if(isset($_POST['kelompok_mapel']))
	{
		$hapusNilaiDetail = mysql_query("DELETE FROM nilai_detail WHERE id_nilai = '$id_nilai'");
		
		$x = 0;
		$tersimpan = 0;
		foreach($_POST['kelompok_mapel'] as $kelompok_mapel)
		{	
			$kode_mapel = mysql_real_escape_string($_POST['kode_mapel'][$x]);
			$nama_mapel = mysql_real_escape_string($_POST['nama_mapel'][$x]);
			$kkm = mysql_real_escape_string($_POST['kkm'][$x]);
			$urutan_mapel = mysql_real_escape_string($_POST['urutan_mapel'][$x]);
			$nilai_p = mysql_real_escape_string($_POST['nilai_p'][$x]);
			$nilai_k = mysql_real_escape_string($_POST['nilai_k'][$x]);
			
			if(!empty($nilai_p) and !empty($nilai_k))
			{
				$simpanData = mysql_query("INSERT INTO nilai_detail (id_nilai, kelompok_mapel, kode_mapel, nama_mapel, kkm, urutan_mapel, nilai_p, nilai_k, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_nilai', '$kelompok_mapel', '$kode_mapel', '$nama_mapel', '$kkm', '$urutan_mapel', '$nilai_p', '$nilai_k', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
					
				if($simpanData)
				{
					$aktivitas = "Tambah Nilai Detail";
					$keterangan = mysql_real_escape_string("Menambahkan '$nama_mapel' Pada 'id_nilai = $id_nilai' Pada Tabel 'nilai_detail'");
					$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
					$tersimpan++;
				}
			}
			
			$x++;
		}
		
		if($tersimpan > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nitai Detail Ditambahkan!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
		
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
		</script>
		";
	}
}

if($_POST['mod']=="simpanDataImport")
{
	if(!empty($_FILES['file']['name']))
	{
		$jenis_file = array("application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		if(in_array($_FILES['file']['type'], $jenis_file))
		{
			$target_dir = "../../uploads/import/";
			$name_file = basename($_FILES['file']['name']) ;
			$target_file = $target_dir . $name_file;
			move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

			require_once "../../libraries/PHPExcel.php";
			require_once "../../libraries/PHPExcel/IOFactory.php";

			$file = $target_dir . $name_file;

			$objPHPExcel = PHPExcel_IOFactory::load($file);
			
			$success = 0;
			$error = 0;
			$keterangan_error = "";
			
			foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
			{
				$totalrow = $worksheet->getHighestRow();
				
				$tahun_ajar = mysql_escape_string($worksheet->getCellByColumnAndRow(0, 5)->getValue());
				$tahun_ajar = explode("] ", $tahun_ajar);
				$id_tahun_ajar = str_replace("Tahun Ajar: [", "", $tahun_ajar[0]);
				
				$kurikulum = mysql_escape_string($worksheet->getCellByColumnAndRow(4, 6)->getValue());
				$kurikulum = explode("] ", $kurikulum);
				$id_kurikulum = str_replace("[", "", $kurikulum[0]);
				
				for($row = 9; $row <= $totalrow; $row++)
				{
					$siswa = mysql_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$siswa = explode("] ", $siswa);
					$id_siswa = str_replace("[", "", $siswa[0]);
					
					$siswa = mysql_fetch_array(mysql_query("SELECT nama_lengkap FROM siswa WHERE id = '$id_siswa'"));
					
					$cekNilai = mysql_num_rows(mysql_query("SELECT id FROM $nama_tabel WHERE id_siswa = '$id_siswa' AND id_tahun_ajar = '$id_tahun_ajar'"));
					if($cekNilai > 0)
					{
						$nilai = mysql_fetch_array(mysql_query("SELECT id FROM nilai WHERE id_siswa = '$id_siswa' AND id_tahun_ajar = '$id_tahun_ajar'"));
						
						$hapusNilaiDetail = mysql_query("DELETE FROM nilai_detail WHERE id_nilai = '$nilai[id]'");
						
						$perbaruiDataImport = mysql_query("UPDATE $nama_tabel SET id_kurikulum = '$id_kurikulum', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$nilai[id]'");
						
						$i = 4;
						$mapel = mysql_query("SELECT * FROM mapel WHERE id_kurikulum = '$id_kurikulum' ORDER BY kelompok_mapel, urutan_mapel");
						while($ambilMapel = mysql_fetch_array($mapel))
						{
							$nilai_p = mysql_escape_string($worksheet->getCellByColumnAndRow($i, $row)->getValue());
							$nilai_k = mysql_escape_string($worksheet->getCellByColumnAndRow($i + 1, $row)->getValue());
							
							$simpanMapel = mysql_query("INSERT INTO nilai_detail (id_nilai, kelompok_mapel, kode_mapel, nama_mapel, kkm, nilai_p, nilai_k, urutan_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nilai[id]', '$ambilMapel[kelompok_mapel]', '$ambilMapel[kode_mapel]', '$ambilMapel[nama_mapel]', '$ambilMapel[kkm]', '$nilai_p', '$nilai_k', '$ambilMapel[urutan_mapel]', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
							
							$i += 2;
						}
							
						if($perbaruiDataImport)
						{
							$aktivitas = "Perbarui Nilai";
							$keterangan = mysql_real_escape_string("Memperbarui '$siswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
							$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
								
							$success++;
						}
						else
						{
							$error++;
							$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
						}
					}
					else
					{
						$simpanDataImport = mysql_query("INSERT INTO $nama_tabel (id_siswa, id_tahun_ajar, id_kurikulum, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_siswa', '$id_tahun_ajar', '$id_kurikulum', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
						
						$id_nilai = mysql_fetch_row(mysql_query("SELECT MAX(id) FROM nilai WHERE id_siswa = '$id_siswa' AND id_tahun_ajar = '$id_tahun_ajar'"));
						
						$i = 4;
						$mapel = mysql_query("SELECT * FROM mapel WHERE id_kurikulum = '$id_kurikulum' ORDER BY kelompok_mapel, urutan_mapel");
						while($ambilMapel = mysql_fetch_array($mapel))
						{
							$nilai_p = mysql_escape_string($worksheet->getCellByColumnAndRow($i, $row)->getValue());
							$nilai_k = mysql_escape_string($worksheet->getCellByColumnAndRow($i + 1, $row)->getValue());
							
							$simpanMapel = mysql_query("INSERT INTO nilai_detail (id_nilai, kelompok_mapel, kode_mapel, nama_mapel, kkm, nilai_p, nilai_k, urutan_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_nilai[0]', '$ambilMapel[kelompok_mapel]', '$ambilMapel[kode_mapel]', '$ambilMapel[nama_mapel]', '$ambilMapel[kkm]', '$nilai_p', '$nilai_k', '$ambilMapel[urutan_mapel]', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
							
							$i += 2;
						}
							
						if($simpanDataImport)
						{
							$aktivitas = "Tambah Nilai";
							$keterangan = mysql_real_escape_string("Menambahkan '$siswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
							$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
							
							$success++;
						}
						else
						{
							$error++;
							$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
						}
					}
				}
			}
		
			unlink($target_file);
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Nilai Ditambahkan, $error Error! $keterangan_error');
				$('#form__').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Format File Tidak Valid!');
			</script>
			";
		}
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}
?>