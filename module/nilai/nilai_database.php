<?php
session_start();
include "../../config/database.php";

$nama_menu = "nilai";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		$('.data tfoot .filter').each(function(){
			var title = $(this).text();
			$(this).html( '<input type="text"/>' );
		});
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 1, "asc" ], [ 4, "asc" ], [ 5, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/nilai/data.php",
			"aoColumns": [
				{
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "<input type='checkbox' id='" + data + "' class='terpilih' onclick='tampilTombolHapus(" + data + ")'/>";
						
						return aksi;
					}
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"bSortable": false,
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "";
						aksi += "<div class='dropdown'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown'>Action.. <span class='caret'></span></button><ul class='dropdown-menu' style='padding: 10px;'>";
						aksi += "<li style='<?=$u;?>'><button type='button' class='btn btn-primary btn-sm' onclick='tambahNilaiDetail(" + data + ")' style='margin-bottom: 10px;'><i class='fa fa-plus' aria-hidden='true' style='margin-right: 10px;'></i>Tambah Nilai Detail</button></li>";
						aksi += "<li style='<?=$u;?>'><button type='button' class='btn btn-warning btn-sm' onclick='editData(" + data + ")' style='margin-bottom: 10px;'><i class='fa fa-pencil' aria-hidden='true' style='margin-right: 10px;'></i>Edit</button></li>";
						aksi += "<li style='<?=$d;?>'><button type='button' class='btn btn-danger btn-sm' onclick='hapusData(" + data + ")'><i class='fa fa-trash' aria-hidden='true' style='margin-right: 10px;'></i>Hapus</button></li>";
						aksi += "</ul></div>";
						
						return aksi;
					}
				}
			]
		});
		
		table.columns().every(function(){
			var that = this;
 
			$('input', this.footer()).on('keyup change',function(){
				if (that.search() !== this.value){
					that
					.search(this.value)
					.draw();
				}
			});
		});
	});
	
	//Disable Tombol Hapus Terpilih
	$(document).ready(function(){
		$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
	});
	
	//Select All
	$(document).ready(function(){
		$('#pilihSemua').change(function(){
			if($('#pilihSemua').is(':checked'))
			{
				$('.terpilih').prop('checked', true);
				$('#hapusDataTerpilih').removeClass("disabled").prop('disabled', false);
			}
			else
			{
				$('.terpilih').prop('checked', false);
				$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
			}
		});
	});
	
	//Tampil Tombol Hapus Terpilih
	var banyaknya = 0;
	
	function tampilTombolHapus(id)
	{
		var terpilih = $("#"+id+":checked").val();
		
		if(terpilih == "on")
		{
			banyaknya += 1;
		}
		else
		{
			banyaknya -= 1;
		}
		
		if(banyaknya > 0)
		{
			$('#hapusDataTerpilih').removeClass("disabled").prop('disabled', false);
		}
		else
		{
			$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
		}
	}
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<th style="width: 1px;">#</th>
			<th>Siswa</th>
			<th>NIPD</th>
			<th>NISN</th>
			<th>Tingkat</th>
			<th>Semester</th>
			<th>Tahun Ajar</th>
			<th>Kurikulum</th>
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<tfoot>
		<tr>
			<th>#</th>
			<th class="filter">Nama Siswa</th>
			<th class="filter">NIPD</th>
			<th class="filter">NISN</th>
			<th class="filter">Tingkat</th>
			<th class="filter">Semester</th>
			<th class="filter">Tahun Ajar</th>
			<th class="filter">Kurikulum</th>
			<th>#</th>
		</tr>
	</tfoot>
	
</table>