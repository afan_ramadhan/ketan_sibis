<?php
session_start();
require_once "../../config/database.php";
require_once "../../libraries/fungsi_waktu.php";
require_once "../../libraries/PHPExcel.php";

function indeksKolom($indeks)
{
	$kolom = array("", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");
	
	return $kolom[$indeks];
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$data_adminweb = mysql_query("SELECT user.*, level.nama_level FROM user LEFT JOIN level ON user.id_level = level.id WHERE user.id = '$_SESSION[id]'");
$ambil_data_adminweb = mysql_fetch_array($data_adminweb);

$namaFile = "format_import_nilai-" . $lihat_konfigurasi['nama_instansi'] . "-" . date("Y-m-d H i s") . ".xlsx";


$excel = new PHPExcel();

$excel->setActiveSheetIndex(0);

$sheet = $excel->getActiveSheet()->setTitle('Format Import Nilai');

$sheet->setCellValue("A1", "Format Import Nilai");
$sheet->getStyle("A1")->getFont()->setBold(true)->setSize(14);

$sheet->setCellValue("A2", strtoupper($lihat_konfigurasi['nama_instansi']));
$sheet->getStyle("A2")->getFont()->setBold(true)->setSize(14);

$sheet->setCellValue("A3", "Kecamatan $lihat_konfigurasi[kecamatan], Kabupaten $lihat_konfigurasi[kabupaten], Provinsi $lihat_konfigurasi[provinsi]");
$sheet->getStyle("A3")->getFont()->setSize(12);

$sheet->setCellValue("A4", "Tanggal Unduh: " . date("Y-m-d H:i:s"));
$sheet->setCellValue("C4", "Pengunduh: $ambil_data_adminweb[nama_lengkap] ($ambil_data_adminweb[email])");

$ambilTahunAjar = mysql_fetch_array(mysql_query("SELECT * FROM tahun_ajar WHERE id = '$_POST[id_tahun_ajar]'"));

$sheet->setCellValue("A5", "Tahun Ajar: [$ambilTahunAjar[id]] $ambilTahunAjar[tahun_ajar]");
$sheet->setCellValue("C5", "Semester: $ambilTahunAjar[semester]");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	),
	'font' => array(
		'name' => 'Calibri',
		'bold' => true,
		'size' => '12'
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap' => TRUE
	)
);

$headerNo = 0;

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(6);
$sheet->setCellValue(indeksKolom($headerNo) . "6", "No")->getStyle(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "6", "Nama Lengkap")->getStyle(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "6", "NISN")->getStyle(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "6", "NIPD")->getStyle(indeksKolom($headerNo) . "6:".indeksKolom($headerNo)."8")->applyFromArray($styleArray);

$ambilKurikulum = mysql_fetch_array(mysql_query("SELECT * FROM kurikulum WHERE id = '$_POST[id_kurikulum]'"));
$jumlahMapel = mysql_num_rows(mysql_query("SELECT * FROM mapel WHERE id_kurikulum = '$_POST[id_kurikulum]'"));

if($jumlahMapel > 0)
{
	$jumlahMapel = $jumlahMapel * 2 - 1;

	$headerNo++;
	$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "6:".indeksKolom($headerNo + $jumlahMapel)."6");
	$sheet->setCellValue(indeksKolom($headerNo) . "6", "[$ambilKurikulum[id]] $ambilKurikulum[nama_kurikulum]")->getStyle(indeksKolom($headerNo) . "6:".indeksKolom($headerNo + $jumlahMapel)."6")->applyFromArray($styleArray);

	$mapel = mysql_query("SELECT * FROM mapel WHERE id_kurikulum = '$_POST[id_kurikulum]' ORDER BY kelompok_mapel, urutan_mapel");
	while($ambilMapel = mysql_fetch_array($mapel))
	{
		$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "7:".indeksKolom($headerNo + 1)."7");
		$sheet->setCellValue(indeksKolom($headerNo) . "7", "$ambilMapel[kode_mapel]")->getStyle(indeksKolom($headerNo)."7:".indeksKolom($headerNo + 1)."7")->applyFromArray($styleArray);
		
		$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(10);
		$sheet->setCellValue(indeksKolom($headerNo) . "8", "P")->getStyle(indeksKolom($headerNo)."8")->applyFromArray($styleArray);
		
		$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo + 1))->setWidth(10);
		$sheet->setCellValue(indeksKolom($headerNo + 1) . "8", "K")->getStyle(indeksKolom($headerNo + 1)."8")->applyFromArray($styleArray);
		
		$headerNo += 2;
	}
}

$i = 9;

$dataSiswa = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.keluar_karena = '' AND siswa.id_rombel = '$_POST[id_rombel]'");
while($ambilDataSiswa = mysql_fetch_array($dataSiswa))
{
	$dataNo = 0;

	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $i-7);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, "[$ambilDataSiswa[id]] $ambilDataSiswa[nama_lengkap]");
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nisn']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nipd']);
	
    $i++;
}

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=$namaFile");

$data = PHPExcel_IOFactory::createWriter($excel, "Excel2007");
$data->save('php://output');

exit;
?>