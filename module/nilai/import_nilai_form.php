<?php
include "../../config/database.php";
?>

<script>
	$(document).ready(function(){
		ambilKurikulumDariRombel();
	})
	
	function ambilKurikulumDariRombel()
	{
		var mod = "ambilKurikulumDariRombel";
		var id_rombel = $("#id_rombel").val();
		$.ajax({
			type	: "POST",
			url		: "module/nilai/nilai_response.php",
			data	: "mod=" + mod +
					  "&id_rombel=" + id_rombel,
			success: function(response)
			{
				const data = JSON.parse(response);
				var option = "";
				var i;
                
				for(i = 0; i < data.length; i++)
				{
					option += "<option value='" + data[i].id + "'>" + data[i].nama_kurikulum + " | Tingkat : " + data[i].tingkat + "</option>";
                } 
			
				$("#id_kurikulum").empty();
				$("#id_kurikulum").append(option);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Import Nilai
	</h4>
</div>
<div class="modal-body">
	<form method="post" action="module/nilai/form_nilai_export.php" target="_blank">
		<table class="table table-hover">
			<tr>
				<td style="border: none;">
					<label class="control-label">Rombel</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="id_rombel" name="id_rombel" onchange="ambilKurikulumDariRombel()">
						<?php
						$rombel = mysql_query("SELECT rombel.*, jurusan.nama_jurusan FROM rombel LEFT JOIN jurusan ON rombel.id_jurusan = jurusan.id ORDER BY rombel.nama_rombel ASC, rombel.tingkat ASC, jurusan.nama_jurusan ASC");
						while($getRombel = mysql_fetch_array($rombel))
						{
						?>
							<option value="<?=$getRombel['id'];?>" ><?=$getRombel['nama_rombel'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Tahun Ajar</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="id_tahun_ajar" name="id_tahun_ajar">
						<?php
						$tahunAjar = mysql_query("SELECT * FROM tahun_ajar ORDER BY tahun_ajar DESC, semester DESC");
						while($getTahunAjar = mysql_fetch_array($tahunAjar))
						{
						?>
							<option value="<?=$getTahunAjar['id'];?>"><?=$getTahunAjar['tahun_ajar'];?> | Semester : <?=$getTahunAjar['semester'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Kurikulum</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="id_kurikulum" name="id_kurikulum">
						<?php
						$kurikulum = mysql_query("SELECT kurikulum.*, jurusan.nama_jurusan FROM kurikulum LEFT JOIN jurusan ON kurikulum.id_jurusan = jurusan.id ORDER BY nama_kurikulum ASC, tingkat ASC, nama_jurusan ASC");
						while($getKurikulum = mysql_fetch_array($kurikulum))
						{
						?>
							<option value="<?=$getKurikulum['id'];?>"><?=$getKurikulum['nama_kurikulum'];?> | Tingkat : <?=$getKurikulum['tingkat'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="3" align="right" style="border: none;">
					<button type="submit" class="btn btn-info"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-right: 10px;"></i>Download Format Import</button>
				</td>
			</tr>
		</table>
	</form>
	<table class="table table-hover">		
		<tr>
			<td style="border: none;">
				<label class="control-label">File</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="file" name="file"/>
				<p class="help-block">File Harus Berformat Excel.</p>
			</td>
		</tr>		
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanDataImport" onclick="simpanDataImport()"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Upload</button>
</div>