<?php
include "../../config/database.php";

$id_kurikulum = 0;

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM nilai WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
	
	$id_kurikulum = $getData['id_kurikulum'];
}
?>

<script>
	$(document).ready(function(){
		ambilSiswa();
	})
	
	function ambilSiswa()
	{
		var mod = "ambilSiswa";
		var id_rombel = $("#id_rombel").val();
		$.ajax({
			type	: "POST",
			url		: "module/nilai/nilai_response.php",
			data	: "mod=" + mod +
					  "&id_rombel=" + id_rombel,
			success: function(response)
			{
				const data = JSON.parse(response);
				var option = "";
				var i;
                
				for(i = 0; i < data.length; i++)
				{
					option += "<option value='" + data[i].id + "'>" + data[i].nama_lengkap + "</option>";
                } 
			
				$("#id_siswa").empty();
				$("#id_siswa").append(option);
		
				ambilKurikulumDariSiswa();
			}
		})
	}
	
	function ambilKurikulumDariSiswa()
	{
		var mod = "ambilKurikulumDariSiswa";
		var id_siswa = $("#id_siswa").val();
		var id_kurikulum =  <?=$id_kurikulum;?>;
		$.ajax({
			type	: "POST",
			url		: "module/nilai/nilai_response.php",
			data	: "mod=" + mod +
					  "&id_siswa=" + id_siswa,
			success: function(response)
			{
				const data = JSON.parse(response);
				var option = "";
				var i;
                
				for(i = 0; i < data.length; i++)
				{
					var selected = (id_kurikulum == data[i].id ? "selected" : "");
					option += "<option value='" + data[i].id + "' " + selected + ">" + data[i].nama_kurikulum + " | Tingkat : " + data[i].tingkat + "</option>";
                } 
			
				$("#id_kurikulum").empty();
				$("#id_kurikulum").append(option);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Nilai";}else{echo "Tambah Nilai";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr <?php if($_POST['mod']=="editData"){echo "style='display: none;'";} ?>>
			<td style="border: none;">
				<label class="control-label">Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_rombel" name="id_rombel" onchange="ambilSiswa()">
					<?php
					$rombel = mysql_query("SELECT rombel.*, jurusan.nama_jurusan FROM rombel LEFT JOIN jurusan ON rombel.id_jurusan = jurusan.id ORDER BY rombel.nama_rombel ASC, rombel.tingkat ASC, jurusan.nama_jurusan ASC");
					while($getRombel = mysql_fetch_array($rombel))
					{
					?>
						<option value="<?=$getRombel['id'];?>" ><?=$getRombel['nama_rombel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Siswa</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_siswa" <?php if($_POST['mod']=="editData"){echo "disabled";} ?> onchange="ambilKurikulumDariSiswa()">
					<?php
					$siswa = mysql_query("SELECT * FROM siswa WHERE keluar_karena = '' ORDER BY nama_lengkap");
					while($getSiswa = mysql_fetch_array($siswa))
					{
						$selected = ($getData['id_siswa'] == $getSiswa['id'] ? "selected" : "");
					?>
						<option value="<?=$getSiswa['id'];?>" <?=$selected;?>><?=$getSiswa['nama_lengkap'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Ajar</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_tahun_ajar">
					<?php
					$tahunAjar = mysql_query("SELECT * FROM tahun_ajar ORDER BY tahun_ajar DESC, semester DESC");
					while($getTahunAjar = mysql_fetch_array($tahunAjar))
					{
						$selected = ($getData['id_tahun_ajar'] == $getTahunAjar['id'] ? "selected" : "");
					?>
						<option value="<?=$getTahunAjar['id'];?>" <?=$selected;?>><?=$getTahunAjar['tahun_ajar'];?> | Semester : <?=$getTahunAjar['semester'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kurikulum</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kurikulum">
					<?php
					$kurikulum = mysql_query("SELECT kurikulum.*, jurusan.nama_jurusan FROM kurikulum LEFT JOIN jurusan ON kurikulum.id_jurusan = jurusan.id ORDER BY nama_kurikulum ASC, tingkat ASC, nama_jurusan ASC");
					while($getKurikulum = mysql_fetch_array($kurikulum))
					{
						$selected = ($getData['id_kurikulum'] == $getKurikulum['id'] ? "selected" : "");
					?>
						<option value="<?=$getKurikulum['id'];?>" <?=$selected;?>><?=$getKurikulum['nama_kurikulum'];?> | Tingkat : <?=$getKurikulum['tingkat'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>