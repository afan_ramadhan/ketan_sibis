<?php
include "../../config/database.php";

if($_POST['mod']=="tambahNilaiDetail")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nilai.*, siswa.nama_lengkap, tahun_ajar.tahun_ajar, tahun_ajar.semester, kurikulum.nama_kurikulum FROM nilai LEFT JOIN siswa ON nilai.id_siswa = siswa.id LEFT JOIN tahun_ajar ON nilai.id_tahun_ajar = tahun_ajar.id LEFT JOIN kurikulum ON nilai.id_kurikulum = kurikulum.id WHERE nilai.id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Daftar Nilai</h4>
</div>
<div class="modal-body">

	<div class="detail scrolling">
		<form id="formNilaiDetail">
			<table class="table table-bordered table-hover data_custom_1">

				<thead>
					<tr>
						<th style="width: 1px;">No.</th>
						<th>Kelompok Mapel</th>
						<th>Kode Mapel</th>
						<th>Nama Mapel</th>
						<th>KKM</th>
						<th>Nilai P</th>
						<th>Nilai K</th>
					</tr>
				</thead>
				
				<input type="hidden" name="mod" value="simpanNilaiDetail">
				<input type="hidden" name="id_nilai" value="<?=$id;?>">

				<tbody id="dataMapel">

					<?php
					$x = 1;
					$mapel = mysql_query("SELECT mapel.*, CASE WHEN mapel.kelompok_mapel = 1 THEN 'Kelompok A' WHEN mapel.kelompok_mapel = 2 THEN 'Kelompok B' WHEN mapel.kelompok_mapel = 3 THEN 'Kelompok C' ELSE 'Kelompok D' END AS kelompok_mapel_, nilai_detail.nilai_p, nilai_detail.nilai_k FROM mapel LEFT JOIN nilai_detail ON mapel.kode_mapel = nilai_detail.kode_mapel AND nilai_detail.id_nilai = '$id' WHERE id_kurikulum = '$getData[id_kurikulum]' ORDER BY mapel.kelompok_mapel, mapel.urutan_mapel");
					while($getMapel = mysql_fetch_array($mapel))
					{
						echo "
						
						<input type='hidden' name='kelompok_mapel[]' value='$getMapel[kelompok_mapel]'>
						<input type='hidden' name='kode_mapel[]' value='$getMapel[kode_mapel]'>
						<input type='hidden' name='nama_mapel[]' value='$getMapel[nama_mapel]'>
						<input type='hidden' name='kkm[]' value='$getMapel[kkm]'>
						<input type='hidden' name='urutan_mapel[]' value='$getMapel[urutan_mapel]'>
						
						<tr>
							<td align='center'>$x</td>
							<td>$getMapel[kelompok_mapel_]</td>
							<td>$getMapel[kode_mapel]</td>
							<td>$getMapel[nama_mapel]</td>
							<td>$getMapel[kkm]</td>
							<td>
								<input type='text' class='form-control' name='nilai_p[]' maxlength='3' value='$getMapel[nilai_p]' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
							</td>
							<td>
								<input type='text' class='form-control' name='nilai_k[]' maxlength='3' value='$getMapel[nilai_k]' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
							</td>
						</tr>";
						$x++;
					}
					?>
					
				</tbody>
				
			</table>
		</form>
	</div>
	
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="tambahNilaiDetail")
	{
		echo "<button type='button' class='btn btn-success' id='simpanNilaiDetail' onclick='simpanNilaiDetail()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>