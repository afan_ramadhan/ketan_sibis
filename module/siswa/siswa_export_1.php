<?php
session_start();
require_once "../../config/database.php";
require_once "../../libraries/fungsi_waktu.php";
require_once "../../libraries/PHPExcel.php";

function indeksKolom($indeks)
{
	$kolom = array("", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ", "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");
	
	return $kolom[$indeks];
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$data_adminweb = mysql_query("SELECT user.*, level.nama_level FROM user LEFT JOIN level ON user.id_level = level.id WHERE user.id = '$_SESSION[id]'");
$ambil_data_adminweb = mysql_fetch_array($data_adminweb);

$namaFile = "daftar_pd-" . $lihat_konfigurasi['nama_instansi'] . "-" . date("Y-m-d H i s") . ".xlsx";


$excel = new PHPExcel();

$excel->setActiveSheetIndex(0);

$sheet = $excel->getActiveSheet()->setTitle('Daftar Peserta Didik Aktif');

$sheet->setCellValue("A1", "Daftar Peserta Didik");
$sheet->getStyle("A1")->getFont()->setBold(true)->setSize(14);

$sheet->setCellValue("A2", strtoupper($lihat_konfigurasi['nama_instansi']));
$sheet->getStyle("A2")->getFont()->setBold(true)->setSize(14);

$sheet->setCellValue("A3", "Kecamatan $lihat_konfigurasi[kecamatan], Kabupaten $lihat_konfigurasi[kabupaten], Provinsi $lihat_konfigurasi[provinsi]");
$sheet->getStyle("A3")->getFont()->setSize(12);

$sheet->setCellValue("A4", "Tanggal Unduh: " . date("Y-m-d H:i:s"));
$sheet->setCellValue("C4", "Pengunduh: $ambil_data_adminweb[nama_lengkap] ($ambil_data_adminweb[email])");

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	),
	'font' => array(
		'name' => 'Calibri',
		'bold' => true,
		'size' => '12'
	),
	'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
		'wrap' => TRUE
	)
);

$headerNo = 0;

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(6);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tanggal Pendaftaran")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Nama Lengkap")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(25);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Nama Panggilan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jenis Kelamin")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tempat Lahir")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tanggal Lahir")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Agama")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Golongan Darah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Berat Badan (kg)")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tinggi Badan (cm)")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Lingkar Kepala (cm)")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Anak Ke Berapa")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jumlah Saudara Kandung")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jumlah Saudara Tiri")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jumlah Saudara Angkat")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kebutuhan Khusus")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(25);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Hobi")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NIK")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. KK")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(25);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. Registrasi Akta Lahir")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Alamat Jalan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(10);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "RT")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(10);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "RW")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Dusun")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kelurahan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kecamatan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kode Pos")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kewarganegaraan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Lintang")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Bujur")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jarak Rumah (km)")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tempat Tinggal")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Moda Transportasi")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Nama Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NIK Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tahun Lahir Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jenjang Pendidikan Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Pekerjaan Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Penghasilan Bulanan Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kebutuhan Khusus Ayah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Nama Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NIK Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tahun Lahir Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jenjang Pendidikan Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Pekerjaan Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Penghasilan Bulanan Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kebutuhan Khusus Ibu")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Nama Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NIK Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tahun Lahir Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jenjang Pendidikan Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Pekerjaan Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setAutoSize(true);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Penghasilan Bulanan Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Kebutuhan Khusus Wali")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. Telepon Rumah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. HP")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(30);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Email")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Jenis Pendaftaran")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(55);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Sekolah Asal")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(55);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Sekolah Asal Pindahan")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. Peserta Ujian Nasional")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. SKHUN")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(25);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. Seri Ijazah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NISN")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "NIPD")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Tanggal Masuk Sekolah")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);


$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Penerima KIP")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. KIP")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Penerima KPS")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. KPS")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. KKS")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Layak PIP")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(30);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Alasan Layak PIP")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Bank")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "No. Rekening Bank")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(35);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Rekening Atas Nama")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$headerNo++;
$excel->getActiveSheet()->mergeCells(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6");
$excel->getActiveSheet()->getColumnDimension(indeksKolom($headerNo))->setWidth(20);
$sheet->setCellValue(indeksKolom($headerNo) . "5", "Rombel")->getStyle(indeksKolom($headerNo) . "5:".indeksKolom($headerNo)."6")->applyFromArray($styleArray);

$i = 7;

$dataSiswa = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.keluar_karena = ''");
while($ambilDataSiswa = mysql_fetch_array($dataSiswa))
{
	$dataNo = 0;

	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $i-6);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tanggal_pendaftaran']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_lengkap']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_panggilan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenis_kelamin']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tempat_lahir']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tanggal_lahir']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['agama']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['golongan_darah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['berat_badan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tinggi_badan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['lingkar_kepala']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['anak_ke_berapa']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jumlah_saudara_kandung']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jumlah_saudara_tiri']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jumlah_saudara_angkat']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kebutuhan_khusus']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['hobi']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nik']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_kk']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_registrasi_akta_lahir']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['alamat']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['rt']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['rw']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['dusun']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kelurahan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kecamatan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kode_pos']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kewarganegaraan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['lintang']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['bujur']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jarak_rumah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenis_tinggal']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['alat_transportasi']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nik_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tahun_lahir_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenjang_pendidikan_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['pekerjaan_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['penghasilan_ayah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kebutuhan_khusus_ayah']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nik_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tahun_lahir_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenjang_pendidikan_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['pekerjaan_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['penghasilan_ibu']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kebutuhan_khusus_ibu']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nik_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tahun_lahir_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenjang_pendidikan_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['pekerjaan_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['penghasilan_wali']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['kebutuhan_khusus_wali']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_telepon']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_hp']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['email']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['jenis_pendaftaran']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['sekolah_asal']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['sekolah_asal_pindahan']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_peserta_ujian_nasional']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['skhun']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_seri_ijazah']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nisn']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nipd']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['tanggal_masuk_sekolah']);
	
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['penerima_kip']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_kip']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['penerima_kps']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_kps']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_kks']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['layak_pip']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['alasan_layak_pip']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['bank']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nomor_rekening_bank']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['rekening_atas_nama']);
	$dataNo++;
	$sheet->setCellValue(indeksKolom($dataNo) . $i, $ambilDataSiswa['nama_rombel']);
	
    $i++;
}

header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment; filename=$namaFile");

$data = PHPExcel_IOFactory::createWriter($excel, "Excel2007");
$data->save('php://output');

exit;
?>