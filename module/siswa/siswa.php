<?php
include "libraries/fungsi_waktu.php";

$nama_menu = "siswa";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Siswa";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$('#tanggal_kelulusan_').datetimepicker({
				format: 'YYYY-MM-DD',
			});
			$("#tampilData1").load("module/siswa/siswa_database_1.php");
			$("#tampilData2").load("module/siswa/siswa_database_2.php");
		})
		
		//Tambah Data
		function tambahData()
		{
			var mod = "tambahData";
			$.ajax({
				type	: "POST",
				url		: "module/siswa/siswa_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent1").html(html);
					$("#form1").modal();
				}
			})
		}
		
		//Simpan Data
		function simpanData()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanData");
			
			data.append("tanggal_pendaftaran", $("#tanggal_pendaftaran").val());
			data.append("nama_lengkap", $("#nama_lengkap").val());
			data.append("nama_panggilan", $("#nama_panggilan").val());
			data.append("nipd", $("#nipd").val());
			data.append("tanggal_masuk_sekolah", $("#tanggal_masuk_sekolah").val());
			data.append("jenis_kelamin", $("#jenis_kelamin").val());
			data.append("nisn", $("#nisn").val());
			data.append("tempat_lahir", $("#tempat_lahir").val());
			data.append("tanggal_lahir", $("#tanggal_lahir").val());
			data.append("nik", $("#nik").val());
			data.append("agama", $("#agama").val());
			data.append("alamat", $("#alamat").val());
			
			data.append("rt", $("#rt").val());
			data.append("rw", $("#rw").val());
			data.append("dusun", $("#dusun").val());
			data.append("kelurahan", $("#kelurahan").val());
			data.append("kecamatan", $("#kecamatan").val());
			data.append("kode_pos", $("#kode_pos").val());
			data.append("kewarganegaraan", $("#kewarganegaraan").val());
			data.append("jenis_tinggal", $("#jenis_tinggal").val());
			data.append("alat_transportasi", $("#alat_transportasi").val());
			data.append("nomor_telepon", $("#nomor_telepon").val());
			data.append("nomor_hp", $("#nomor_hp").val());
			
			data.append("email", $("#email").val());
			data.append("skhun", $("#skhun").val());
			data.append("penerima_kps", $("#penerima_kps").val());
			data.append("nomor_kps", $("#nomor_kps").val());
			data.append("nama_ayah", $("#nama_ayah").val());
			data.append("tahun_lahir_ayah", $("#tahun_lahir_ayah").val());
			data.append("jenjang_pendidikan_ayah", $("#jenjang_pendidikan_ayah").val());
			data.append("pekerjaan_ayah", $("#pekerjaan_ayah").val());
			data.append("penghasilan_ayah", $("#penghasilan_ayah").val());
			data.append("nik_ayah", $("#nik_ayah").val());
			data.append("kebutuhan_khusus_ayah", $("#kebutuhan_khusus_ayah").val());
			
			data.append("nama_ibu", $("#nama_ibu").val());
			data.append("tahun_lahir_ibu", $("#tahun_lahir_ibu").val());
			data.append("jenjang_pendidikan_ibu", $("#jenjang_pendidikan_ibu").val());
			data.append("pekerjaan_ibu", $("#pekerjaan_ibu").val());
			data.append("penghasilan_ibu", $("#penghasilan_ibu").val());
			data.append("nik_ibu", $("#nik_ibu").val());
			data.append("kebutuhan_khusus_ibu", $("#kebutuhan_khusus_ibu").val());
			data.append("nama_wali", $("#nama_wali").val());
			data.append("tahun_lahir_wali", $("#tahun_lahir_wali").val());
			data.append("jenjang_pendidikan_wali", $("#jenjang_pendidikan_wali").val());
			data.append("pekerjaan_wali", $("#pekerjaan_wali").val());
			
			data.append("penghasilan_wali", $("#penghasilan_wali").val());
			data.append("nik_wali", $("#nik_wali").val());
			data.append("kebutuhan_khusus_wali", $("#kebutuhan_khusus_wali").val());
			data.append("nomor_peserta_ujian_nasional", $("#nomor_peserta_ujian_nasional").val());
			data.append("nomor_seri_ijazah", $("#nomor_seri_ijazah").val());
			data.append("penerima_kip", $("#penerima_kip").val());
			data.append("nomor_kip", $("#nomor_kip").val());
			data.append("nama_kip", $("#nama_kip").val());
			data.append("nomor_kks", $("#nomor_kks").val());
			data.append("nomor_registrasi_akta_lahir", $("#nomor_registrasi_akta_lahir").val());
			data.append("bank", $("#bank").val());
			
			data.append("nomor_rekening_bank", $("#nomor_rekening_bank").val());
			data.append("rekening_atas_nama", $("#rekening_atas_nama").val());
			data.append("layak_pip", $("#layak_pip").val());
			data.append("alasan_layak_pip", $("#alasan_layak_pip").val());
			data.append("kebutuhan_khusus", $("#kebutuhan_khusus").val());
			data.append("hobi", $("#hobi").val());
			data.append("jenis_pendaftaran", $("#jenis_pendaftaran").val());
			data.append("sekolah_asal", $("#sekolah_asal").val());
			data.append("sekolah_asal_pindahan", $("#sekolah_asal_pindahan").val());
			data.append("anak_ke_berapa", $("#anak_ke_berapa").val());
			data.append("lintang", $("#lintang").val());
			data.append("bujur", $("#bujur").val());
			data.append("nomor_kk", $("#nomor_kk").val());
			
			data.append("golongan_darah", $("#golongan_darah").val());
			data.append("berat_badan", $("#berat_badan").val());
			data.append("tinggi_badan", $("#tinggi_badan").val());
			data.append("lingkar_kepala", $("#lingkar_kepala").val());
			data.append("jumlah_saudara_kandung", $("#jumlah_saudara_kandung").val());
			data.append("jumlah_saudara_tiri", $("#jumlah_saudara_tiri").val());
			data.append("jumlah_saudara_angkat", $("#jumlah_saudara_angkat").val());
			data.append("jarak_rumah", $("#jarak_rumah").val());
			
			data.append("foto", $('#foto')[0].files[0]);
			
			$.ajax({
				type		: "POST",
				url			: "module/siswa/siswa_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi1").html(html);
					$("#tampilData1").load("module/siswa/siswa_database_1.php");
					$("#tampilData2").load("module/siswa/siswa_database_2.php");
				}
			})
		}
		
		//Edit Data
		function editData1(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/siswa/siswa_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent2").empty();
					$("#formContent1").html(html);
					$("#form1").modal();
				}
			})
		}
		
		//Edit Data
		function editData2(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/siswa/siswa_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent1").empty();
					$("#formContent2").html(html);
					$("#form2").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData(id)
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "perbaruiData");
			data.append("id", id);
			
			data.append("tanggal_pendaftaran", $("#tanggal_pendaftaran").val());
			data.append("nama_lengkap", $("#nama_lengkap").val());
			data.append("nama_panggilan", $("#nama_panggilan").val());
			data.append("nipd", $("#nipd").val());
			data.append("tanggal_masuk_sekolah", $("#tanggal_masuk_sekolah").val());
			data.append("jenis_kelamin", $("#jenis_kelamin").val());
			data.append("nisn", $("#nisn").val());
			data.append("tempat_lahir", $("#tempat_lahir").val());
			data.append("tanggal_lahir", $("#tanggal_lahir").val());
			data.append("nik", $("#nik").val());
			data.append("agama", $("#agama").val());
			data.append("alamat", $("#alamat").val());
			
			data.append("rt", $("#rt").val());
			data.append("rw", $("#rw").val());
			data.append("dusun", $("#dusun").val());
			data.append("kelurahan", $("#kelurahan").val());
			data.append("kecamatan", $("#kecamatan").val());
			data.append("kode_pos", $("#kode_pos").val());
			data.append("kewarganegaraan", $("#kewarganegaraan").val());
			data.append("jenis_tinggal", $("#jenis_tinggal").val());
			data.append("alat_transportasi", $("#alat_transportasi").val());
			data.append("nomor_telepon", $("#nomor_telepon").val());
			data.append("nomor_hp", $("#nomor_hp").val());
			
			data.append("email", $("#email").val());
			data.append("skhun", $("#skhun").val());
			data.append("penerima_kps", $("#penerima_kps").val());
			data.append("nomor_kps", $("#nomor_kps").val());
			data.append("nama_ayah", $("#nama_ayah").val());
			data.append("tahun_lahir_ayah", $("#tahun_lahir_ayah").val());
			data.append("jenjang_pendidikan_ayah", $("#jenjang_pendidikan_ayah").val());
			data.append("pekerjaan_ayah", $("#pekerjaan_ayah").val());
			data.append("penghasilan_ayah", $("#penghasilan_ayah").val());
			data.append("nik_ayah", $("#nik_ayah").val());
			data.append("kebutuhan_khusus_ayah", $("#kebutuhan_khusus_ayah").val());
			
			data.append("nama_ibu", $("#nama_ibu").val());
			data.append("tahun_lahir_ibu", $("#tahun_lahir_ibu").val());
			data.append("jenjang_pendidikan_ibu", $("#jenjang_pendidikan_ibu").val());
			data.append("pekerjaan_ibu", $("#pekerjaan_ibu").val());
			data.append("penghasilan_ibu", $("#penghasilan_ibu").val());
			data.append("nik_ibu", $("#nik_ibu").val());
			data.append("kebutuhan_khusus_ibu", $("#kebutuhan_khusus_ibu").val());
			data.append("nama_wali", $("#nama_wali").val());
			data.append("tahun_lahir_wali", $("#tahun_lahir_wali").val());
			data.append("jenjang_pendidikan_wali", $("#jenjang_pendidikan_wali").val());
			data.append("pekerjaan_wali", $("#pekerjaan_wali").val());
			
			data.append("penghasilan_wali", $("#penghasilan_wali").val());
			data.append("nik_wali", $("#nik_wali").val());
			data.append("kebutuhan_khusus_wali", $("#kebutuhan_khusus_wali").val());
			data.append("nomor_peserta_ujian_nasional", $("#nomor_peserta_ujian_nasional").val());
			data.append("nomor_seri_ijazah", $("#nomor_seri_ijazah").val());
			data.append("penerima_kip", $("#penerima_kip").val());
			data.append("nomor_kip", $("#nomor_kip").val());
			data.append("nama_kip", $("#nama_kip").val());
			data.append("nomor_kks", $("#nomor_kks").val());
			data.append("nomor_registrasi_akta_lahir", $("#nomor_registrasi_akta_lahir").val());
			data.append("bank", $("#bank").val());
			
			data.append("nomor_rekening_bank", $("#nomor_rekening_bank").val());
			data.append("rekening_atas_nama", $("#rekening_atas_nama").val());
			data.append("layak_pip", $("#layak_pip").val());
			data.append("alasan_layak_pip", $("#alasan_layak_pip").val());
			data.append("kebutuhan_khusus", $("#kebutuhan_khusus").val());
			data.append("hobi", $("#hobi").val());
			data.append("jenis_pendaftaran", $("#jenis_pendaftaran").val());
			data.append("sekolah_asal", $("#sekolah_asal").val());
			data.append("sekolah_asal_pindahan", $("#sekolah_asal_pindahan").val());
			data.append("anak_ke_berapa", $("#anak_ke_berapa").val());
			data.append("lintang", $("#lintang").val());
			data.append("bujur", $("#bujur").val());
			data.append("nomor_kk", $("#nomor_kk").val());
			
			data.append("golongan_darah", $("#golongan_darah").val());
			data.append("berat_badan", $("#berat_badan").val());
			data.append("tinggi_badan", $("#tinggi_badan").val());
			data.append("lingkar_kepala", $("#lingkar_kepala").val());
			data.append("jumlah_saudara_kandung", $("#jumlah_saudara_kandung").val());
			data.append("jumlah_saudara_tiri", $("#jumlah_saudara_tiri").val());
			data.append("jumlah_saudara_angkat", $("#jumlah_saudara_angkat").val());
			data.append("jarak_rumah", $("#jarak_rumah").val());
			data.append("keluar_karena", $("#keluar_karena").val());
			data.append("tanggal_keluar", $("#tanggal_keluar").val());
			data.append("alasan_keluar", $("#alasan_keluar").val());
			data.append("melanjutkan_di", $("#melanjutkan_di").val());
			data.append("bekerja_di", $("#bekerja_di").val());
			
			data.append("foto", $('#foto')[0].files[0]);
			
			$.ajax({
				type		: "POST",
				url			: "module/siswa/siswa_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi1").html(html);
					$("#tampilData1").load("module/siswa/siswa_database_1.php");
					$("#tampilData2").load("module/siswa/siswa_database_2.php");
				}
			})
		}
		
		//Hapus Data
		function hapusData(id)
		{
			mulaiAnimasi();
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				var mod = "hapusData";
				var id = id;
				$.ajax({
					type	: "POST",
					url		: "module/siswa/siswa_action.php",
					data	: "mod=" + mod +
							  "&id=" + id,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi1").html(html);
						$("#tampilData1").load("module/siswa/siswa_database_1.php");
						$("#tampilData2").load("module/siswa/siswa_database_2.php");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih1()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih1:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/siswa/siswa_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData1").load("module/siswa/siswa_database_1.php");
						$("#tampilData2").load("module/siswa/siswa_database_2.php");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih2()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih2:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/siswa/siswa_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi1").html(html);
						$("#tampilData1").load("module/siswa/siswa_database_1.php");
						$("#tampilData2").load("module/siswa/siswa_database_2.php");
					}
				})
			}
		}
		
		//Pindah Rombel Data Terpilih
		function pindahRombelDataTerpilih()
		{
			var konfirmasi = confirm("Pindah Rombel Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "pindahRombelDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih1:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				var id_rombel = $("#rombel").val();
				var keterangan = $("#keterangan").val();
				$.ajax({
					type	: "POST",
					url		: "module/siswa/siswa_action.php",
					data	: "mod=" + mod +
							  "&id_rombel=" + id_rombel +
							  "&keterangan=" + keterangan +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi1").html(html);
						$("#tampilData1").load("module/siswa/siswa_database_1.php");
						$("#tampilData2").load("module/siswa/siswa_database_2.php");
					}
				})
			}
		}
		
		//Proses Kelulusan Data Terpilih
		function prosesKelulusanDataTerpilih()
		{
			var konfirmasi = confirm("Proses Kelulusan Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "prosesKelulusanDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih1:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				var tanggal_kelulusan = $("#tanggal_kelulusan").val();
				$.ajax({
					type	: "POST",
					url		: "module/siswa/siswa_action.php",
					data	: "mod=" + mod +
							  "&tanggal_kelulusan=" + tanggal_kelulusan +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi1").html(html);
						$("#tampilData1").load("module/siswa/siswa_database_1.php");
						$("#tampilData2").load("module/siswa/siswa_database_2.php");
					}
				})
			}
		}
		
		//Lihat Detail
		function lihatDetail1(id)
		{
			var mod = "lihatDetail";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/siswa/detail_siswa_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent1_").html(html);
					$("#form1_").modal();
				}
			})
		}
		
		//Lihat Detail
		function lihatDetail2(id)
		{
			var mod = "lihatDetail";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/siswa/detail_siswa_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent2_").html(html);
					$("#form2_").modal();
				}
			})
		}
		
		//Tambah Data Import
		function tambahDataImport()
		{
			var mod = "tambahDataImport";
			$.ajax({
				type	: "POST",
				url		: "module/siswa/import_siswa_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent1").html(html);
					$("#form1").modal();
				}
			})
		}
		
		//Simpan Data Import
		function simpanDataImport()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanDataImport");
			data.append("file", $('#file')[0].files[0]);
			$.ajax({
				type		: "POST",
				url			: "module/siswa/siswa_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi1").html(html);
					$("#tampilData1").load("module/siswa/siswa_database_1.php");
					$("#tampilData2").load("module/siswa/siswa_database_2.php");
				}
			})
		}
		
		//Tambah Data Upload Batch
		function tambahDataUploadBatch()
		{
			var mod = "tambahDataUploadBatch";
			$.ajax({
				type	: "POST",
				url		: "module/siswa/upload_foto_batch_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent1__").html(html);
					$("#form1__").modal();
				}
			})
		}
		
		//Simpan Data Upload Batch
		function simpanDataUploadBatch()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanDataUploadBatch");
			data.append("file", $('#file')[0].files[0]);
			$.ajax({
				type		: "POST",
				url			: "module/siswa/siswa_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi1__").html(html);
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Siswa</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Siswa</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Siswa</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#data1">Siswa Aktif</a></li>
								<li><a data-toggle="tab" href="#data2">Siswa Keluar</a></li>
							</ul>
							<div class="tab-content">
								<div id="data1" class="tab-pane fade in active" style="padding-top: 10px;">
									<button type="button" class="btn btn-primary btn-md" onclick="tambahData()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah Siswa</button>
									<button type="button" class="btn btn-primary btn-md" onclick="tambahDataImport()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Import Siswa</button>
									<button type="button" class="btn btn-warning btn-md" onclick="tambahDataUploadBatch()" style="margin-right: 10px; <?=$u;?>"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Upload Foto Batch</button>
									<a class="btn btn-success btn-md" href="module/siswa/siswa_export_1.php" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-right: 10px;"></i>Export Siswa</a>
									<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
										<input type="checkbox" id="pilihSemua1" style="margin-right: 10px;">Pilih Semua
									</button>
									<button type="button" id="hapusDataTerpilih1" class="btn btn-danger" onclick="hapusDataTerpilih1()" style="<?=$d;?>">
										<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus Siswa Terpilih</span>
									</button>
									<br style="margin-bottom: 20px; <?=$u;?>"/>
									<select class="form-control" id="rombel" style="width: 200px; margin-right: 10px; display: inline; <?=$u;?>">
										<?php
										$rombel = mysql_query("SELECT rombel.*, jurusan.nama_jurusan FROM rombel LEFT JOIN jurusan ON rombel.id_jurusan = jurusan.id ORDER BY rombel.nama_rombel ASC, rombel.tingkat ASC, jurusan.nama_jurusan ASC");
										while($getRombel = mysql_fetch_array($rombel))
										{
											echo "<option value='$getRombel[id]'>$getRombel[nama_rombel]</option>";
										}
										?>
									</select>
									<select class="form-control" id="keterangan" style="width: 200px; margin-right: 10px; display: inline; <?=$u;?>">
										<option value="Siswa Baru">Siswa Baru</option>
										<option value="Naik Kelas">Naik Kelas</option>
										<option value="Mengulang">Mengulang</option>
									</select>
									<button type="button" id="pindahRombelDataTerpilih" class="btn btn-info" onclick="pindahRombelDataTerpilih()" style="<?=$u;?>">
										<i class="fa fa-exchange" aria-hidden="true" style="margin-right: 10px;"></i>Pindah Rombel Siswa Terpilih</span>
									</button>
									<br style="margin-bottom: 20px; <?=$u;?>"/>
									<div style="margin-right: 10px; display: inline-block; <?=$u;?>">
										<div class="input-group date" id="tanggal_kelulusan_" style="width: 200px;">
											<input type="text" class="form-control" id="tanggal_kelulusan" value="<?=$tanggal_sekarang;?>"/>
											<span class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</span>
										</div>
									</div>
									<div style="margin-right: 10px; display: inline-block; <?=$u;?>">
										<div class="input-group">
											<button type="button" id="prosesKelulusanDataTerpilih" class="btn btn-info" onclick="prosesKelulusanDataTerpilih()">
												<i class="fa fa-graduation-cap" aria-hidden="true" style="margin-right: 10px;"></i>Proses Kelulusan Siswa Terpilih</span>
											</button>
										</div>
									</div>
									<div class="modal fade" id="form1" role="dialog">
										<div class="modal-dialog">
											<div id="formContent1" class="modal-content"></div>
											<div id="notifikasi1" class="modal-content"></div>
										</div>
									</div>	
									<div class="modal fade" id="form1_" role="dialog">
										<div class="modal-dialog">
											<div id="formContent1_" class="modal-content"></div>
										</div>
									</div>	
									<div class="modal fade" id="form1__" role="dialog">
										<div class="modal-dialog">
											<div id="formContent1__" class="modal-content"></div>
											<div id="notifikasi1__" class="modal-content"></div>
										</div>
									</div>			
									<br/>
									<br/>  
									<div id="tampilData1" class="scrolling" onload="tampilData1()" style="padding-bottom: 45px;"></div>
								</div>
								<div id="data2" class="tab-pane fade" style="padding-top: 10px;">
									<a class="btn btn-success btn-md" href="module/siswa/siswa_export_2.php" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-right: 10px;"></i>Export Siswa</a>
									<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
										<input type="checkbox" id="pilihSemua2" style="margin-right: 10px;">Pilih Semua
									</button>
									<button type="button" id="hapusDataTerpilih2" class="btn btn-danger" onclick="hapusDataTerpilih2()" style="<?=$d;?>">
										<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus Siswa Terpilih</span>
									</button>
									<div class="modal fade" id="form2" role="dialog">
										<div class="modal-dialog">
											<div id="formContent2" class="modal-content"></div>
											<div id="notifikas2" class="modal-content"></div>
										</div>
									</div>	
									<div class="modal fade" id="form2_" role="dialog">
										<div class="modal-dialog">
											<div id="formContent2_" class="modal-content"></div>
										</div>
									</div>			
									<br/>
									<br/>  
									<div id="tampilData2" class="scrolling" onload="tampilData2()" style="padding-bottom: 45px;"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Siswa</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Siswa</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>