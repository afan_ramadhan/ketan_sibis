<?php
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "siswa";
$sTableJoin1 = "rombel";
$aColumns = array('id', 'nama_lengkap', 'nipd', 'nisn', 'jenis_kelamin', 'agama', 'nama_rombel'); 
$sIndexColumn = "$sTable.id";
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

$lakiLakiArray = array("l", "la", "lak", "laki", "laki ", "laki-", "laki-", "laki-l", "laki-la", "laki-lak", "laki-laki");
$perempuanArray = array("p", "pe", "per", "pere", "perem", "peremp", "perempu", "perempua", "perempuan");

if($_GET['sSearch'] != "")
{
	$sWhere = "WHERE
	$sTable.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = '' OR
	$sTable.nipd LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = '' OR
	$sTable.nisn LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = '' OR
	$sTable.jenis_kelamin LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = '' OR
	$sTable.agama LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = '' OR
	$sTableJoin1.nama_rombel LIKE  '%" . mysql_real_escape_string($_GET['sSearch']) . "%' AND $sTable.keluar_karena = ''";
}
else
{
	$sWhere = "WHERE $sTable.keluar_karena = ''";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 4)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(in_array($bKeyword, $lakiLakiArray))
			{
				$bValue = "L";
			}
			else if(in_array($bKeyword, $perempuanArray))
			{
				$bValue = "P";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTable.nama_lengkap, $sTable.nipd, $sTable.nisn, $sTable.jenis_kelamin, $sTable.agama, $sTableJoin1.nama_rombel
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_rombel = $sTableJoin1.id
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_rombel = $sTableJoin1.id";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>