<?php
include "../../config/database.php";

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Siswa</h4>
</div>
<div class="modal-body">
	<table class="table table-stripped table-hover data">
		<tbody>
			<?php
			$data = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$id'");
			$getData = mysql_fetch_array($data);

			$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");
			
			if($getData['foto'] == "")
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/user_kosong.jpg' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			else
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/siswa/$getData[foto]' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			
			echo "
			<tr>
				<th>Tanggal Pendaftaran</th>
				<th>:</th>
				<td>$getData[tanggal_pendaftaran]</td>
			</tr>
			<tr>
				<th style='width: 200px;'>Nama Lengkap</th>
				<th>:</th>
				<td>$getData[nama_lengkap]</td>
			</tr>
			<tr>
				<th style='width: 200px;'>Nama Panggilan</th>
				<th>:</th>
				<td>$getData[nama_panggilan]</td>
			</tr>
			<tr>
				<th>Jenis Kelamin</th>
				<th>:</th>
				<td>".($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan")."</td>
			</tr>
			<tr>
				<th>Tempat Lahir</th>
				<th>:</th>
				<td>$getData[tempat_lahir]</td>
			</tr>
			<tr>
				<th>Tanggal Lahir</th>
				<th>:</th>
				<td>$getData[tanggal_lahir]</td>
			</tr>
			<tr>
				<th>Agama</th>
				<th>:</th>
				<td>$getData[agama]</td>
			</tr>
			<tr>
				<th>Golongan Darah</th>
				<th>:</th>
				<td>$getData[golongan_darah]</td>
			</tr>
			<tr>
				<th>Berat Badan</th>
				<th>:</th>
				<td>$getData[berat_badan] kg</td>
			</tr>
			<tr>
				<th>Tinggi Badan</th>
				<th>:</th>
				<td>$getData[tinggi_badan] cm</td>
			</tr>
			<tr>
				<th>Lingkar Kepala</th>
				<th>:</th>
				<td>$getData[lingkar_kepala] cm</td>
			</tr>
			<tr>
				<th>Anak Ke Berapa</th>
				<th>:</th>
				<td>$getData[anak_ke_berapa]</td>
			</tr>
			<tr>
				<th>Jumlah Saudara Kandung</th>
				<th>:</th>
				<td>$getData[jumlah_saudara_kandung]</td>
			</tr>
			<tr>
				<th>Jumlah Saudara Tiri</th>
				<th>:</th>
				<td>$getData[jumlah_saudara_tiri]</td>
			</tr>
			<tr>
				<th>Jumlah Saudara Angkat</th>
				<th>:</th>
				<td>$getData[jumlah_saudara_angkat]</td>
			</tr>
			<tr>
				<th>Kebutuhan Khusus</th>
				<th>:</th>
				<td>$getData[kebutuhan_khusus]</td>
			</tr>
			<tr>
				<th>Hobi</th>
				<th>:</th>
				<td>$getData[hobi]</td>
			</tr>
			<tr>
				<th>NIK</th>
				<th>:</th>
				<td>$getData[nik]</td>
			</tr>
			<tr>
				<th>No. KK</th>
				<th>:</th>
				<td>$getData[nomor_kk]</td>
			</tr>
			<tr>
				<th>No. Registrasi Akta Lahir</th>
				<th>:</th>
				<td>$getData[nomor_registrasi_akta_lahir]</td>
			</tr>
			<tr>
				<th>Alamat Jalan</th>
				<th>:</th>
				<td>$getData[alamat]</td>
			</tr>
			<tr>
				<th>RT</th>
				<th>:</th>
				<td>$getData[rt]</td>
			</tr>
			<tr>
				<th>RW</th>
				<th>:</th>
				<td>$getData[rw]</td>
			</tr>
			<tr>
				<th>Dusun</th>
				<th>:</th>
				<td>$getData[dusun]</td>
			</tr>
			<tr>
				<th>Kelurahan</th>
				<th>:</th>
				<td>$getData[kelurahan]</td>
			</tr>
			<tr>
				<th>Kecamatan</th>
				<th>:</th>
				<td>$getData[kecamatan]</td>
			</tr>
			<tr>
				<th>Kode Pos</th>
				<th>:</th>
				<td>$getData[kode_pos]</td>
			</tr>
			<tr>
				<th>Kewarganegaraan</th>
				<th>:</th>
				<td>$getData[kewarganegaraan]</td>
			</tr>
			<tr>
				<th>Lintang</th>
				<th>:</th>
				<td>$getData[lintang]</td>
			</tr>
			<tr>
				<th>Bujur</th>
				<th>:</th>
				<td>$getData[bujur]</td>
			</tr>
			<tr>
				<th>Jarak Rumah</th>
				<th>:</th>
				<td>$getData[jarak_rumah] km</td>
			</tr>
			<tr>
				<th>Tempat Tinggal</th>
				<th>:</th>
				<td>$getData[jenis_tinggal]</td>
			</tr>
			<tr>
				<th>Moda Transportasi</th>
				<th>:</th>
				<td>$getData[alat_transportasi]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			
			<tr>
				<th style='border-top: none;'>Nama Ayah</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[nama_ayah]</td>
			</tr>
			<tr>
				<th>NIK Ayah</th>
				<th>:</th>
				<td>$getData[nik_ayah]</td>
			</tr>
			<tr>
				<th>Tahun Lahir Ayah</th>
				<th>:</th>
				<td>$getData[tahun_lahir_ayah]</td>
			</tr>
			<tr>
				<th>Jenjang Pendidikan Ayah</th>
				<th>:</th>
				<td>$getData[jenjang_pendidikan_ayah]</td>
			</tr>
			<tr>
				<th>Pekerjaan Ayah</th>
				<th>:</th>
				<td>$getData[pekerjaan_ayah]</td>
			</tr>
			<tr>
				<th>Penghasilan Bulanan Ayah</th>
				<th>:</th>
				<td>$getData[penghasilan_ayah]</td>
			</tr>
			<tr>
				<th>Kebutuhan Khusus Ayah</th>
				<th>:</th>
				<td>$getData[kebutuhan_khusus_ayah]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			
			<tr>
				<th style='border-top: none;'>Nama Ibu</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[nama_ibu]</td>
			</tr>
			<tr>
				<th>NIK Ibu</th>
				<th>:</th>
				<td>$getData[nik_ibu]</td>
			</tr>
			<tr>
				<th>Tahun Lahir Ibu</th>
				<th>:</th>
				<td>$getData[tahun_lahir_ibu]</td>
			</tr>
			<tr>
				<th>Jenjang Pendidikan Ibu</th>
				<th>:</th>
				<td>$getData[jenjang_pendidikan_ibu]</td>
			</tr>
			<tr>
				<th>Pekerjaan Ibu</th>
				<th>:</th>
				<td>$getData[pekerjaan_ibu]</td>
			</tr>
			<tr>
				<th>Penghasilan Bulanan Ibu</th>
				<th>:</th>
				<td>$getData[penghasilan_ibu]</td>
			</tr>
			<tr>
				<th>Kebutuhan Khusus Ibu</th>
				<th>:</th>
				<td>$getData[kebutuhan_khusus_ibu]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			
			<tr>
				<th style='border-top: none;'>Nama Wali</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[nama_wali]</td>
			</tr>
			<tr>
				<th>NIK Wali</th>
				<th>:</th>
				<td>$getData[nik_wali]</td>
			</tr>
			<tr>
				<th>Tahun Lahir Wali</th>
				<th>:</th>
				<td>$getData[tahun_lahir_wali]</td>
			</tr>
			<tr>
				<th>Jenjang Pendidikan Wali</th>
				<th>:</th>
				<td>$getData[jenjang_pendidikan_wali]</td>
			</tr>
			<tr>
				<th>Pekerjaan Wali</th>
				<th>:</th>
				<td>$getData[pekerjaan_wali]</td>
			</tr>
			<tr>
				<th>Penghasilan Bulanan Wali</th>
				<th>:</th>
				<td>$getData[penghasilan_wali]</td>
			</tr>
			<tr>
				<th>Kebutuhan Khusus Wali</th>
				<th>:</th>
				<td>$getData[kebutuhan_khusus_wali]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			<tr>
				<th style='border-top: none;'>No. Telepon Rumah</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[nomor_telepon]</td>
			</tr>
			<tr>
				<th>No. HP</th>
				<th>:</th>
				<td>$getData[nomor_hp] <a class='btn btn-success btn-sm' href='https://api.whatsapp.com/send?phone=".str_replace("08", "628", substr($getData['nomor_hp'], 0, 2)).substr($getData['nomor_hp'], 2)."' target='_blank' style='margin-left: 10px;'><i class='fa fa-whatsapp' aria-hidden='true' style='margin-right: 10px;'></i>Kirim Pesan</a></td>
			</tr>
			<tr>
				<th>Email</th>
				<th>:</th>
				<td>$getData[email] <a class='btn btn-success btn-sm' href='mailto:$getData[email]' target='_blank' style='margin-left: 10px;'><i class='fa fa-envelope-o' aria-hidden='true' style='margin-right: 10px;'></i>Kirim Pesan</a></td></td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			<tr>
				<th style='border-top: none;'>Jenis Pendaftaran</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[jenis_pendaftaran]</td>
			</tr>
			<tr>
				<th>Sekolah Asal</th>
				<th>:</th>
				<td>$getData[sekolah_asal]</td>
			</tr>
			<tr>
				<th>Sekolah Asal Pindahan</th>
				<th>:</th>
				<td>$getData[sekolah_asal_pindahan]</td>
			</tr>
			<tr>
				<th>No. Peserta Ujian Nasional</th>
				<th>:</th>
				<td>$getData[nomor_peserta_ujian_nasional]</td>
			</tr>
			<tr>
				<th>No. SKHUN</th>
				<th>:</th>
				<td>$getData[skhun]</td>
			</tr>
			<tr>
				<th>No. Seri Ijazah</th>
				<th>:</th>
				<td>$getData[nomor_seri_ijazah]</td>
			</tr>
			<tr>
				<th>NISN</th>
				<th>:</th>
				<td>$getData[nisn]</td>
			</tr>
			<tr>
				<th style='width: 1px;'>NIPD</th>
				<th style='width: 1px;'>:</th>
				<td>$getData[nipd]</td>
			</tr>
			<tr>
				<th style='width: 1px;'>Tanggal Masuk Sekolah</th>
				<th style='width: 1px;'>:</th>
				<td>$getData[tanggal_masuk_sekolah]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			<tr>
				<th style='border-top: none;'>Penerima KIP</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[penerima_kip]</td>
			</tr>
			<tr>
				<th>No. KIP</th>
				<th>:</th>
				<td>$getData[nomor_kip]</td>
			</tr>
			<tr>
				<th>Penerima KPS</th>
				<th>:</th>
				<td>$getData[penerima_kps]</td>
			</tr>
			<tr>
				<th>No. KPS</th>
				<th>:</th>
				<td>$getData[nomor_kps]</td>
			</tr>
			<tr>
				<th>No. KKS</th>
				<th>:</th>
				<td>$getData[nomor_kks]</td>
			</tr>
			<tr>
				<th>Layak PIP</th>
				<th>:</th>
				<td>$getData[layak_pip]</td>
			</tr>
			<tr>
				<th>Alasan Layak PIP</th>
				<th>:</th>
				<td>$getData[alasan_layak_pip]</td>
			</tr>
			<tr>
				<th>Bank</th>
				<th>:</th>
				<td>$getData[bank]</td>
			</tr>
			<tr>
				<th>No. Rekening Bank</th>
				<th>:</th>
				<td>$getData[nomor_rekening_bank]</td>
			</tr>
			<tr>
				<th>Rekening Atas Nama</th>
				<th>:</th>
				<td>$getData[rekening_atas_nama]</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			<tr>
				<th style='border-top: none;'>Riwayat Rombel</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>
					<ol>";
				
					$dataRiwayatRombel = mysql_query("SELECT * FROM riwayat_rombel_siswa WHERE id_siswa = '$id' ORDER BY id");
					while($getDataRiwayatRombel = mysql_fetch_array($dataRiwayatRombel))
					{
						echo "<li>$getDataRiwayatRombel[nama_rombel] - $getDataRiwayatRombel[keterangan]</li>";
					}
					echo "
					</ol>
				</td>
			</tr>
			
			<tr>
				<td colspan='3' style='border: none;'><hr/></td>
			</tr>
			
			<tr>
				<th style='border-top: none;'>Keluar Karena</th>
				<th style='border-top: none;'>:</th>
				<td style='border-top: none;'>$getData[keluar_karena]</td>
			</tr>
			<tr>
				<th>Tanggal Keluar</th>
				<th>:</th>
				<td>$getData[tanggal_keluar]</td>
			</tr>
			<tr>
				<th>Alasan Keluar</th>
				<th>:</th>
				<td>$getData[alasan_keluar]</td>
			</tr>
			<tr>
				<th>Melanjutkan Di</th>
				<th>:</th>
				<td>$getData[melanjutkan_di]</td>
			</tr>
			<tr>
				<th>Bekerja Di</th>
				<th>:</th>
				<td>$getData[bekerja_di]</td>
			</tr>";
			?>
		
		</tbody>
		
	</table>
	
</div>
<div class="modal-footer">
</div>