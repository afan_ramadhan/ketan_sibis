<?php
include "../../config/database.php";
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Import Siswa
	</h4>
</div>
<div class="modal-body">
	<p>Silahkan Export Data Siswa Melalui Aplikasi Dapodik.</p>
	<hr/>	
	<table class="table table-hover">	
		<tr>
			<td style="border: none;">
				<label class="control-label">File</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="file" name="file"/>
				<p class="help-block">File Harus Berformat Excel.</p>
			</td>
		</tr>	
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanDataImport" onclick="simpanDataImport()"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Upload</button>
</div>