<?php
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM siswa WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	$(function(){
		$('#tanggal_pendaftaran_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_lahir_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_keluar_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_masuk_sekolah_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
	})
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Siswa";}else{echo "Tambah Siswa";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Pendaftaran</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_pendaftaran_">
					<input type="text" class="form-control" id="tanggal_pendaftaran" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_pendaftaran'];}else{echo $tanggal_sekarang;} ?>"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Lengkap</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_lengkap" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_lengkap'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Panggilan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_panggilan" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_panggilan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenis Kelamin</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_kelamin">
					<option value="L" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "L" ? "selected" : "");} ?>>Laki-Laki</option>
					<option value="P" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_kelamin'] == "P" ? "selected" : "");} ?>>Perempuan</option>
					
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tempat Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tempat_lahir" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['tempat_lahir'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_lahir_">
					<input type="text" class="form-control" id="tanggal_lahir" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_lahir'];}else{echo $tanggal_sekarang;} ?>"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Agama</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="agama">
					<option value="Islam" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Islam" ? "selected" : "");} ?>>Islam</option>
					<option value="Kristen" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kristen" ? "selected" : "");} ?>>Kristen</option>
					<option value="Katholik" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Katholik" ? "selected" : "");} ?>>Katholik</option>
					<option value="Hindu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Hindu" ? "selected" : "");} ?>>Hindu</option>
					<option value="Budha" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Budha" ? "selected" : "");} ?>>Budha</option>
					<option value="Khonghucu" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Khonghucu" ? "selected" : "");} ?>>Khonghucu</option>
					<option value="Kepercayaan Kepada Tuhan YME" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Kepercayaan Kepada Tuhan YME" ? "selected" : "");} ?>>Kepercayaan Kepada Tuhan YME</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['agama'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Golongan Darah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="golongan_darah" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['golongan_darah'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Berat Badan (kg)</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="berat_badan" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['berat_badan'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tinggi Badan (cm)</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tinggi_badan" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['tinggi_badan'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Lingkar Kepala (cm)</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="lingkar_kepala" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['lingkar_kepala'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Anak Ke Berapa</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="anak_ke_berapa" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['anak_ke_berapa'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jumlah Saudara Kandung</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="jumlah_saudara_kandung" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['jumlah_saudara_kandung'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jumlah Saudara Tiri</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="jumlah_saudara_tiri" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['jumlah_saudara_tiri'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jumlah Saudara Angkat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="jumlah_saudara_angkat" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['jumlah_saudara_angkat'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kebutuhan Khusus</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kebutuhan_khusus">
					<option value="Tidak ada" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Tidak ada" ? "selected" : "");} ?>>Tidak ada</option>
					<option value="Netra" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Netra" ? "selected" : "");} ?>>Netra</option>
					<option value="Rungu" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Rungu" ? "selected" : "");} ?>>Rungu</option>
					<option value="Grahita ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Grahita ringan" ? "selected" : "");} ?>>Grahita ringan</option>
					<option value="Grahita Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Grahita Sedang" ? "selected" : "");} ?>>Grahita Sedang</option>
					<option value="Daksa Ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Daksa Ringan" ? "selected" : "");} ?>>Daksa Ringan</option>
					<option value="Daksa Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Daksa Sedang" ? "selected" : "");} ?>>Daksa Sedang</option>
					<option value="Laras" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Laras" ? "selected" : "");} ?>>Laras</option>
					<option value="Wicara" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Wicara" ? "selected" : "");} ?>>Wicara</option>
					<option value="Tuna ganda" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Tuna ganda" ? "selected" : "");} ?>>Tuna ganda</option>
					<option value="Hiper aktif" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Hiper aktif" ? "selected" : "");} ?>>Hiper aktif</option>
					<option value="Cerdas Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Cerdas Istimewa" ? "selected" : "");} ?>>Cerdas Istimewa</option>
					<option value="Bakat Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Bakat Istimewa" ? "selected" : "");} ?>>Bakat Istimewa</option>
					<option value="Kesulitan Belajar" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Kesulitan Belajar" ? "selected" : "");} ?>>Kesulitan Belajar</option>
					<option value="Narkoba" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Narkoba" ? "selected" : "");} ?>>Narkoba</option>
					<option value="Indigo" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Indigo" ? "selected" : "");} ?>>Indigo</option>
					<option value="Down Sindrome" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Down Sindrome" ? "selected" : "");} ?>>Down Sindrome</option>
					<option value="Autis" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus'] == "Autis" ? "selected" : "");} ?>>Autis</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Hobi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="hobi" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['hobi'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NIK</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nik" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nik'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. KK</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_kk" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_kk'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. Registrasi Akta Lahir</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_registrasi_akta_lahir" maxlength="30" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_registrasi_akta_lahir'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alamat Jalan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="alamat" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['alamat'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">RT</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="rt" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['rt'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">RW</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="rw" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['rw'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Dusun</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="dusun" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['dusun'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelurahan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kelurahan" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['kelurahan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kecamatan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kecamatan" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['kecamatan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kode Pos</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kode_pos" maxlength="5" value="<?php if($_POST['mod']=="editData"){echo $getData['kode_pos'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kewarganegaraan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kewarganegaraan">
					<option value="WNI" <?php if($_POST['mod']=="editData"){echo ($getData['kewarganegaraan'] == "WNI" ? "selected" : "");} ?>>WNI</option>
					<option value="WNA" <?php if($_POST['mod']=="editData"){echo ($getData['kewarganegaraan'] == "WNA" ? "selected" : "");} ?>>WNA</option>
					
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Lintang</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="lintang" maxlength="25" value="<?php if($_POST['mod']=="editData"){echo $getData['lintang'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Bujur</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="bujur" maxlength="25" value="<?php if($_POST['mod']=="editData"){echo $getData['bujur'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jarak Rumah (km)</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="jarak_rumah" maxlength="3" value="<?php if($_POST['mod']=="editData"){echo $getData['jarak_rumah'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tempat Tinggal</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_tinggal">
					<option value="Bersama orang tua" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Bersama orang tua" ? "selected" : "");} ?>>Bersama orang tua</option>
					<option value="Wali" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Wali" ? "selected" : "");} ?>>Wali</option>
					<option value="Kost" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Kost" ? "selected" : "");} ?>>Kost</option>
					<option value="Asrama" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Asrama" ? "selected" : "");} ?>>Asrama</option>
					<option value="Panti Asuhan" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Panti Asuhan" ? "selected" : "");} ?>>Panti Asuhan</option>
					<option value="Pesantren" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Pesantren" ? "selected" : "");} ?>>Pesantren</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_tinggal'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Moda Transportasi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="alat_transportasi">
					<option value="Jalan kaki" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Jalan kaki" ? "selected" : "");} ?>>Jalan kaki</option>
					<option value="Sepeda" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Sepeda" ? "selected" : "");} ?>>Sepeda</option>
					<option value="Sepeda motor" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Sepeda motor" ? "selected" : "");} ?>>Sepeda motor</option>
					<option value="Mobil/bus antar jemput" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Mobil/bus antar jemput" ? "selected" : "");} ?>>Mobil/bus antar jemput</option>
					<option value="Angkutan umum/bus/pete-pete" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Angkutan umum/bus/pete-pete" ? "selected" : "");} ?>>Angkutan umum/bus/pete-pete</option>
					<option value="Ojek" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Ojek" ? "selected" : "");} ?>>Ojek</option>
					<option value="Kereta Api" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Kereta Api" ? "selected" : "");} ?>>Kereta Api</option><option value="Andong/Bendi/Sado/ Dokar/Delman/Beca" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Andong/Bendi/Sado/ Dokar/Delman/Beca" ? "selected" : "");} ?>>Andong/Bendi/Sado/ Dokar/Delman/Beca</option>
					<option value="Perahu penyebrangan/Rakit/Getek" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Perahu penyebrangan/Rakit/Getek" ? "selected" : "");} ?>>Perahu penyebrangan/Rakit/Getek</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['alat_transportasi'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_ayah" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_ayah'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NIK Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nik_ayah" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nik_ayah'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Lahir Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tahun_lahir_ayah" maxlength="4" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_lahir_ayah'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenjang Pendidikan Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenjang_pendidikan_ayah">
					<option value="Tidak sekolah" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "Tidak sekolah" ? "selected" : "");} ?>>Tidak sekolah</option>
					<option value="Putus SD" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "Putus SD" ? "selected" : "");} ?>>Putus SD</option>
					<option value="SD / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "SD / sederajat" ? "selected" : "");} ?>>SD / sederajat</option>
					<option value="SMP / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "SMP / sederajat" ? "selected" : "");} ?>>SMP / sederajat</option>
					<option value="SMA / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "SMA / sederajat" ? "selected" : "");} ?>>SMA / sederajat</option>
					<option value="D1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "D1" ? "selected" : "");} ?>>D1</option>
					<option value="D2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "D2" ? "selected" : "");} ?>>D2</option>
					<option value="D3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "D3" ? "selected" : "");} ?>>D3</option>
					<option value="S1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "S1" ? "selected" : "");} ?>>S1</option>
					<option value="S2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "S2" ? "selected" : "");} ?>>S2</option>
					<option value="S3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ayah'] == "S3" ? "selected" : "");} ?>>S3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pekerjaan Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="pekerjaan_ayah">
					<option value="Tidak bekerja" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Tidak bekerja" ? "selected" : "");} ?>>Tidak bekerja</option>
					<option value="Nelayan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Nelayan" ? "selected" : "");} ?>>Nelayan</option>
					<option value="Petani" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Petani" ? "selected" : "");} ?>>Petani</option>
					<option value="Peternak" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Peternak" ? "selected" : "");} ?>>Peternak</option>
					<option value="PNS/TNI/Polri" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "PNS/TNI/Polri" ? "selected" : "");} ?>>PNS/TNI/Polri</option>
					<option value="Karyawan Swasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Karyawan Swasta" ? "selected" : "");} ?>>Karyawan Swasta</option>
					<option value="Pedagang Kecil" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Pedagang Kecil" ? "selected" : "");} ?>>Pedagang Kecil</option>
					<option value="Pedagang Besar" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Pedagang Besar" ? "selected" : "");} ?>>Pedagang Besar</option>
					<option value="Wiraswasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Wiraswasta" ? "selected" : "");} ?>>Wiraswasta</option>
					<option value="Wirausaha" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Wirausaha" ? "selected" : "");} ?>>Wirausaha</option>
					<option value="Buruh" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Buruh" ? "selected" : "");} ?>>Buruh</option>
					<option value="Pensiunan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Pensiunan" ? "selected" : "");} ?>>Pensiunan</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
					<option value="Sudah Meninggal" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ayah'] == "Sudah Meninggal" ? "selected" : "");} ?>>Sudah Meninggal</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penghasilan Bulanan Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penghasilan_ayah">
					<option value="Tidak Berpenghasilan" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Tidak Berpenghasilan" ? "selected" : "");} ?>>Tidak Berpenghasilan</option>
					<option value="Kurang dari Rp. 500,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Kurang dari Rp. 500,000" ? "selected" : "");} ?>>Kurang dari Rp. 500,000</option>
					<option value="Rp. 500,000 - Rp. 999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Rp. 500,000 - Rp. 999,999" ? "selected" : "");} ?>>Rp. 500,000 - Rp. 999,999</option>
					<option value="Rp. 1,000,000 - Rp. 1,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Rp. 1,000,000 - Rp. 1,999,999" ? "selected" : "");} ?>>Rp. 1,000,000 - Rp. 1,999,999</option>
					<option value="Rp. 2,000.000 - Rp. 4,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Rp. 2,000.000 - Rp. 4,999,999" ? "selected" : "");} ?>>Rp. 2,000.000 - Rp. 4,999,999</option>
					<option value="Rp. 5,000,000 - Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Rp. 5,000,000 - Rp. 20,000,000" ? "selected" : "");} ?>>Rp. 5,000,000 - Rp. 20,000,000</option>
					<option value="Lebih dari Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ayah'] == "Lebih dari Rp. 20,000,000" ? "selected" : "");} ?>>Lebih dari Rp. 20,000,000</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kebutuhan Khusus Ayah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kebutuhan_khusus_ayah">
					<option value="Tidak ada" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Tidak ada" ? "selected" : "");} ?>>Tidak ada</option>
					<option value="Netra" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Netra" ? "selected" : "");} ?>>Netra</option>
					<option value="Rungu" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Rungu" ? "selected" : "");} ?>>Rungu</option>
					<option value="Grahita ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Grahita ringan" ? "selected" : "");} ?>>Grahita ringan</option>
					<option value="Grahita Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Grahita Sedang" ? "selected" : "");} ?>>Grahita Sedang</option>
					<option value="Daksa Ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Daksa Ringan" ? "selected" : "");} ?>>Daksa Ringan</option>
					<option value="Daksa Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Daksa Sedang" ? "selected" : "");} ?>>Daksa Sedang</option>
					<option value="Laras" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Laras" ? "selected" : "");} ?>>Laras</option>
					<option value="Wicara" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Wicara" ? "selected" : "");} ?>>Wicara</option>
					<option value="Tuna ganda" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Tuna ganda" ? "selected" : "");} ?>>Tuna ganda</option>
					<option value="Hiper aktif" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Hiper aktif" ? "selected" : "");} ?>>Hiper aktif</option>
					<option value="Cerdas Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Cerdas Istimewa" ? "selected" : "");} ?>>Cerdas Istimewa</option>
					<option value="Bakat Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Bakat Istimewa" ? "selected" : "");} ?>>Bakat Istimewa</option>
					<option value="Kesulitan Belajar" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Kesulitan Belajar" ? "selected" : "");} ?>>Kesulitan Belajar</option>
					<option value="Narkoba" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Narkoba" ? "selected" : "");} ?>>Narkoba</option>
					<option value="Indigo" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Indigo" ? "selected" : "");} ?>>Indigo</option>
					<option value="Down Sindrome" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Down Sindrome" ? "selected" : "");} ?>>Down Sindrome</option>
					<option value="Autis" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ayah'] == "Autis" ? "selected" : "");} ?>>Autis</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_ibu" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_ibu'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NIK Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nik_ibu" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nik_ibu'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Lahir Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tahun_lahir_ibu" maxlength="4" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_lahir_ibu'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenjang Pendidikan Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenjang_pendidikan_ibu">
					<option value="Tidak sekolah" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "Tidak sekolah" ? "selected" : "");} ?>>Tidak sekolah</option>
					<option value="Putus SD" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "Putus SD" ? "selected" : "");} ?>>Putus SD</option>
					<option value="SD / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "SD / sederajat" ? "selected" : "");} ?>>SD / sederajat</option>
					<option value="SMP / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "SMP / sederajat" ? "selected" : "");} ?>>SMP / sederajat</option>
					<option value="SMA / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "SMA / sederajat" ? "selected" : "");} ?>>SMA / sederajat</option>
					<option value="D1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "D1" ? "selected" : "");} ?>>D1</option>
					<option value="D2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "D2" ? "selected" : "");} ?>>D2</option>
					<option value="D3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "D3" ? "selected" : "");} ?>>D3</option>
					<option value="S1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "S1" ? "selected" : "");} ?>>S1</option>
					<option value="S2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "S2" ? "selected" : "");} ?>>S2</option>
					<option value="S3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_ibu'] == "S3" ? "selected" : "");} ?>>S3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pekerjaan Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="pekerjaan_ibu">
					<option value="Tidak bekerja" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Tidak bekerja" ? "selected" : "");} ?>>Tidak bekerja</option>
					<option value="Nelayan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Nelayan" ? "selected" : "");} ?>>Nelayan</option>
					<option value="Petani" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Petani" ? "selected" : "");} ?>>Petani</option>
					<option value="Peternak" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Peternak" ? "selected" : "");} ?>>Peternak</option>
					<option value="PNS/TNI/Polri" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "PNS/TNI/Polri" ? "selected" : "");} ?>>PNS/TNI/Polri</option>
					<option value="Karyawan Swasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Karyawan Swasta" ? "selected" : "");} ?>>Karyawan Swasta</option>
					<option value="Pedagang Kecil" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Pedagang Kecil" ? "selected" : "");} ?>>Pedagang Kecil</option>
					<option value="Pedagang Besar" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Pedagang Besar" ? "selected" : "");} ?>>Pedagang Besar</option>
					<option value="Wiraswasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Wiraswasta" ? "selected" : "");} ?>>Wiraswasta</option>
					<option value="Wirausaha" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Wirausaha" ? "selected" : "");} ?>>Wirausaha</option>
					<option value="Buruh" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Buruh" ? "selected" : "");} ?>>Buruh</option>
					<option value="Pensiunan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Pensiunan" ? "selected" : "");} ?>>Pensiunan</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
					<option value="Sudah Meninggal" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_ibu'] == "Sudah Meninggal" ? "selected" : "");} ?>>Sudah Meninggal</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penghasilan Bulanan Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penghasilan_ibu">
					<option value="Tidak Berpenghasilan" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Tidak Berpenghasilan" ? "selected" : "");} ?>>Tidak Berpenghasilan</option>
					<option value="Kurang dari Rp. 500,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Kurang dari Rp. 500,000" ? "selected" : "");} ?>>Kurang dari Rp. 500,000</option>
					<option value="Rp. 500,000 - Rp. 999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Rp. 500,000 - Rp. 999,999" ? "selected" : "");} ?>>Rp. 500,000 - Rp. 999,999</option>
					<option value="Rp. 1,000,000 - Rp. 1,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Rp. 1,000,000 - Rp. 1,999,999" ? "selected" : "");} ?>>Rp. 1,000,000 - Rp. 1,999,999</option>
					<option value="Rp. 2,000.000 - Rp. 4,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Rp. 2,000.000 - Rp. 4,999,999" ? "selected" : "");} ?>>Rp. 2,000.000 - Rp. 4,999,999</option>
					<option value="Rp. 5,000,000 - Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Rp. 5,000,000 - Rp. 20,000,000" ? "selected" : "");} ?>>Rp. 5,000,000 - Rp. 20,000,000</option>
					<option value="Lebih dari Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_ibu'] == "Lebih dari Rp. 20,000,000" ? "selected" : "");} ?>>Lebih dari Rp. 20,000,000</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kebutuhan Khusus Ibu</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kebutuhan_khusus_ibu">
					<option value="Tidak ada" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Tidak ada" ? "selected" : "");} ?>>Tidak ada</option>
					<option value="Netra" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Netra" ? "selected" : "");} ?>>Netra</option>
					<option value="Rungu" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Rungu" ? "selected" : "");} ?>>Rungu</option>
					<option value="Grahita ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Grahita ringan" ? "selected" : "");} ?>>Grahita ringan</option>
					<option value="Grahita Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Grahita Sedang" ? "selected" : "");} ?>>Grahita Sedang</option>
					<option value="Daksa Ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Daksa Ringan" ? "selected" : "");} ?>>Daksa Ringan</option>
					<option value="Daksa Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Daksa Sedang" ? "selected" : "");} ?>>Daksa Sedang</option>
					<option value="Laras" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Laras" ? "selected" : "");} ?>>Laras</option>
					<option value="Wicara" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Wicara" ? "selected" : "");} ?>>Wicara</option>
					<option value="Tuna ganda" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Tuna ganda" ? "selected" : "");} ?>>Tuna ganda</option>
					<option value="Hiper aktif" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Hiper aktif" ? "selected" : "");} ?>>Hiper aktif</option>
					<option value="Cerdas Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Cerdas Istimewa" ? "selected" : "");} ?>>Cerdas Istimewa</option>
					<option value="Bakat Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Bakat Istimewa" ? "selected" : "");} ?>>Bakat Istimewa</option>
					<option value="Kesulitan Belajar" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Kesulitan Belajar" ? "selected" : "");} ?>>Kesulitan Belajar</option>
					<option value="Narkoba" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Narkoba" ? "selected" : "");} ?>>Narkoba</option>
					<option value="Indigo" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Indigo" ? "selected" : "");} ?>>Indigo</option>
					<option value="Down Sindrome" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Down Sindrome" ? "selected" : "");} ?>>Down Sindrome</option>
					<option value="Autis" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_ibu'] == "Autis" ? "selected" : "");} ?>>Autis</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_wali" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_wali'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NIK Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nik_wali" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nik_wali'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Lahir Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tahun_lahir_wali" maxlength="4" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_lahir_wali'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenjang Pendidikan Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenjang_pendidikan_wali">
					<option value="Tidak sekolah" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "Tidak sekolah" ? "selected" : "");} ?>>Tidak sekolah</option>
					<option value="Putus SD" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "Putus SD" ? "selected" : "");} ?>>Putus SD</option>
					<option value="SD / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "SD / sederajat" ? "selected" : "");} ?>>SD / sederajat</option>
					<option value="SMP / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "SMP / sederajat" ? "selected" : "");} ?>>SMP / sederajat</option>
					<option value="SMA / sederajat" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "SMA / sederajat" ? "selected" : "");} ?>>SMA / sederajat</option>
					<option value="D1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "D1" ? "selected" : "");} ?>>D1</option>
					<option value="D2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "D2" ? "selected" : "");} ?>>D2</option>
					<option value="D3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "D3" ? "selected" : "");} ?>>D3</option>
					<option value="S1" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "S1" ? "selected" : "");} ?>>S1</option>
					<option value="S2" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "S2" ? "selected" : "");} ?>>S2</option>
					<option value="S3" <?php if($_POST['mod']=="editData"){echo ($getData['jenjang_pendidikan_wali'] == "S3" ? "selected" : "");} ?>>S3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pekerjaan Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="pekerjaan_wali">
					<option value="Tidak bekerja" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Tidak bekerja" ? "selected" : "");} ?>>Tidak bekerja</option>
					<option value="Nelayan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Nelayan" ? "selected" : "");} ?>>Nelayan</option>
					<option value="Petani" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Petani" ? "selected" : "");} ?>>Petani</option>
					<option value="Peternak" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Peternak" ? "selected" : "");} ?>>Peternak</option>
					<option value="PNS/TNI/Polri" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "PNS/TNI/Polri" ? "selected" : "");} ?>>PNS/TNI/Polri</option>
					<option value="Karyawan Swasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Karyawan Swasta" ? "selected" : "");} ?>>Karyawan Swasta</option>
					<option value="Pedagang Kecil" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Pedagang Kecil" ? "selected" : "");} ?>>Pedagang Kecil</option>
					<option value="Pedagang Besar" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Pedagang Besar" ? "selected" : "");} ?>>Pedagang Besar</option>
					<option value="Wiraswasta" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Wiraswasta" ? "selected" : "");} ?>>Wiraswasta</option>
					<option value="Wirausaha" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Wirausaha" ? "selected" : "");} ?>>Wirausaha</option>
					<option value="Buruh" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Buruh" ? "selected" : "");} ?>>Buruh</option>
					<option value="Pensiunan" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Pensiunan" ? "selected" : "");} ?>>Pensiunan</option>
					<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
					<option value="Sudah Meninggal" <?php if($_POST['mod']=="editData"){echo ($getData['pekerjaan_wali'] == "Sudah Meninggal" ? "selected" : "");} ?>>Sudah Meninggal</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penghasilan Bulanan Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penghasilan_wali">
					<option value="Tidak Berpenghasilan" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Tidak Berpenghasilan" ? "selected" : "");} ?>>Tidak Berpenghasilan</option>
					<option value="Kurang dari Rp. 500,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Kurang dari Rp. 500,000" ? "selected" : "");} ?>>Kurang dari Rp. 500,000</option>
					<option value="Rp. 500,000 - Rp. 999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Rp. 500,000 - Rp. 999,999" ? "selected" : "");} ?>>Rp. 500,000 - Rp. 999,999</option>
					<option value="Rp. 1,000,000 - Rp. 1,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Rp. 1,000,000 - Rp. 1,999,999" ? "selected" : "");} ?>>Rp. 1,000,000 - Rp. 1,999,999</option>
					<option value="Rp. 2,000.000 - Rp. 4,999,999" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Rp. 2,000.000 - Rp. 4,999,999" ? "selected" : "");} ?>>Rp. 2,000.000 - Rp. 4,999,999</option>
					<option value="Rp. 5,000,000 - Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Rp. 5,000,000 - Rp. 20,000,000" ? "selected" : "");} ?>>Rp. 5,000,000 - Rp. 20,000,000</option>
					<option value="Lebih dari Rp. 20,000,000" <?php if($_POST['mod']=="editData"){echo ($getData['penghasilan_wali'] == "Lebih dari Rp. 20,000,000" ? "selected" : "");} ?>>Lebih dari Rp. 20,000,000</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kebutuhan Khusus Wali</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kebutuhan_khusus_wali">
					<option value="Tidak ada" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Tidak ada" ? "selected" : "");} ?>>Tidak ada</option>
					<option value="Netra" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Netra" ? "selected" : "");} ?>>Netra</option>
					<option value="Rungu" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Rungu" ? "selected" : "");} ?>>Rungu</option>
					<option value="Grahita ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Grahita ringan" ? "selected" : "");} ?>>Grahita ringan</option>
					<option value="Grahita Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Grahita Sedang" ? "selected" : "");} ?>>Grahita Sedang</option>
					<option value="Daksa Ringan" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Daksa Ringan" ? "selected" : "");} ?>>Daksa Ringan</option>
					<option value="Daksa Sedang" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Daksa Sedang" ? "selected" : "");} ?>>Daksa Sedang</option>
					<option value="Laras" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Laras" ? "selected" : "");} ?>>Laras</option>
					<option value="Wicara" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Wicara" ? "selected" : "");} ?>>Wicara</option>
					<option value="Tuna ganda" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Tuna ganda" ? "selected" : "");} ?>>Tuna ganda</option>
					<option value="Hiper aktif" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Hiper aktif" ? "selected" : "");} ?>>Hiper aktif</option>
					<option value="Cerdas Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Cerdas Istimewa" ? "selected" : "");} ?>>Cerdas Istimewa</option>
					<option value="Bakat Istimewa" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Bakat Istimewa" ? "selected" : "");} ?>>Bakat Istimewa</option>
					<option value="Kesulitan Belajar" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Kesulitan Belajar" ? "selected" : "");} ?>>Kesulitan Belajar</option>
					<option value="Narkoba" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Narkoba" ? "selected" : "");} ?>>Narkoba</option>
					<option value="Indigo" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Indigo" ? "selected" : "");} ?>>Indigo</option>
					<option value="Down Sindrome" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Down Sindrome" ? "selected" : "");} ?>>Down Sindrome</option>
					<option value="Autis" <?php if($_POST['mod']=="editData"){echo ($getData['kebutuhan_khusus_wali'] == "Autis" ? "selected" : "");} ?>>Autis</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">No. Telepon Rumah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_telepon" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_telepon'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. HP</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_hp" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_hp'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Email</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="email" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['email'];} ?>"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenis Pendaftaran</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_pendaftaran">
					<option value="Siswa Baru" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_pendaftaran'] == "Siswa Baru" ? "selected" : "");} ?>>Siswa Baru</option>
					<option value="Pindahan" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_pendaftaran'] == "Pindahan" ? "selected" : "");} ?>>Pindahan</option>
					<option value="Kembali Bersekolah" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_pendaftaran'] == "Kembali Bersekolah" ? "selected" : "");} ?>>Kembali Bersekolah</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Sekolah Asal</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="sekolah_asal" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['sekolah_asal'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Sekolah Asal Pindahan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="sekolah_asal_pindahan" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['sekolah_asal_pindahan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. Peserta Ujian Nasional</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_peserta_ujian_nasional" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_peserta_ujian_nasional'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. SKHUN</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="skhun" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['skhun'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. Seri Ijazah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_seri_ijazah" maxlength="30" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_seri_ijazah'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NISN</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nisn" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nisn'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">NIPD</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nipd" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nipd'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Masuk Sekolah</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_masuk_sekolah_">
					<input type="text" class="form-control" id="tanggal_masuk_sekolah" maxlength="10" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_masuk_sekolah'];} ?>"/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Penerima KIP</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penerima_kip">
					<option value="Tidak" <?php if($_POST['mod']=="editData"){echo ($getData['penerima_kip'] == "Tidak" ? "selected" : "");} ?>>Tidak</option>
					<option value="Ya" <?php if($_POST['mod']=="editData"){echo ($getData['penerima_kip'] == "Ya" ? "selected" : "");} ?>>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. KIP</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_kip" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_kip'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penerima KPS</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="penerima_kps">
					<option value="Tidak" <?php if($_POST['mod']=="editData"){echo ($getData['penerima_kps'] == "Tidak" ? "selected" : "");} ?>>Tidak</option>
					<option value="Ya" <?php if($_POST['mod']=="editData"){echo ($getData['penerima_kps'] == "Ya" ? "selected" : "");} ?>>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. KPS</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_kps" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_kps'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. KKS</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_kks" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_kks'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Layak PIP</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="layak_pip">
					<option value="Tidak" <?php if($_POST['mod']=="editData"){echo ($getData['layak_pip'] == "Tidak" ? "selected" : "");} ?>>Tidak</option>
					<option value="Ya" <?php if($_POST['mod']=="editData"){echo ($getData['layak_pip'] == "Ya" ? "selected" : "");} ?>>Ya</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alasan Layak PIP</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="alasan_layak_pip">
					<option value="" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "" ? "selected" : "");} ?>></option>
					<option value="Pemegang PKH/KPS/KKS" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Pemegang PKH/KPS/KKS" ? "selected" : "");} ?>>Pemegang PKH/KPS/KKS</option>
					<option value="Menerima BSM" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Menerima BSM" ? "selected" : "");} ?>>Menerima BSM</option>
					<option value="Yatim Piatu/Panti Asuhan/Panti Sosial" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Yatim Piatu/Panti Asuhan/Panti Sosial" ? "selected" : "");} ?>>Yatim Piatu/Panti Asuhan/Panti Sosial</option>
					<option value="Dampak Bencana Alam" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Dampak Bencana Alam" ? "selected" : "");} ?>>Dampak Bencana Alam</option>
					<option value="Pernah Drop Out" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Pernah Drop Out" ? "selected" : "");} ?>>Pernah Drop Out</option>
					<option value="Siswa Miskin/Rentan Miskin" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Siswa Miskin/Rentan Miskin" ? "selected" : "");} ?>>Siswa Miskin/Rentan Miskin</option>
					<option value="Daerah Konflik" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Daerah Konflik" ? "selected" : "");} ?>>Daerah Konflik</option>
					<option value="Keluarga Terpidana/Berada di LAPAS" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Keluarga Terpidana/Berada di LAPAS" ? "selected" : "");} ?>>Keluarga Terpidana/Berada di LAPAS</option>
					<option value="Kelainan Fisik" <?php if($_POST['mod']=="editData"){echo ($getData['alasan_layak_pip'] == "Kelainan Fisik" ? "selected" : "");} ?>>Kelainan Fisik</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Bank</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="bank" maxlength="50" value="<?php if($_POST['mod']=="editData"){echo $getData['bank'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">No. Rekening Bank</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_rekening_bank" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_rekening_bank'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Rekening Atas Nama</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="rekening_atas_nama" maxlength="75" value="<?php if($_POST['mod']=="editData"){echo $getData['rekening_atas_nama'];} ?>"/>
			</td>
		</tr>
		
		<tr>
			<td colspan="3" style="border: none;"><hr/></td>
		</tr>
		
		<tr>
			<td style="border: none;">
				<label class="control-label">Foto</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="foto" name="foto"/>
				<p class="help-block">File Gambar.</p>
			</td>
		</tr>
		
		<?php
		if($_POST['mod']=="editData")
		{
		?>
		
			<tr>
				<td colspan="3" style="border: none;"><hr/></td>
			</tr>
			
			<tr>
				<td style="border: none;">
					<label class="control-label">Keluar Karena</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="keluar_karena">
						<option value="" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "" ? "selected" : "");} ?>></option>
						<option value="Lulus" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Lulus" ? "selected" : "");} ?>>Lulus</option>
						<option value="Mutasi" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Mutasi" ? "selected" : "");} ?>>Mutasi</option>
						<option value="Dikeluarkan" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Dikeluarkan" ? "selected" : "");} ?>>Dikeluarkan</option>
						<option value="Mengundurkan Diri" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Mengundurkan Diri" ? "selected" : "");} ?>>Mengundurkan Diri</option>
						<option value="Putus Sekolah" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Putus Sekolah" ? "selected" : "");} ?>>Putus Sekolah</option>
						<option value="Wafat" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Wafat" ? "selected" : "");} ?>>Wafat</option>
						<option value="Lainnya" <?php if($_POST['mod']=="editData"){echo ($getData['keluar_karena'] == "Lainnya" ? "selected" : "");} ?>>Lainnya</option>
					</select>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Tanggal Keluar</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<div class="input-group date" id="tanggal_keluar_">
						<input type="text" class="form-control" id="tanggal_keluar" maxlength="10" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_keluar'];} ?>"/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Alasan Keluar</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="text" class="form-control" id="alasan_keluar" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['alasan_keluar'];} ?>"/>
				</td>
			</tr>
			
			<?php
			if($_POST['mod']=="editData" and $getData['keluar_karena'] == "Lulus")
			{
			?>
			
				<tr>
					<td style="border: none;">
						<label class="control-label">Melanjutkan Di</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;">
						<input type="text" class="form-control" id="melanjutkan_di" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['melanjutkan_di'];} ?>"/>
					</td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Bekerja Di</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;">
						<input type="text" class="form-control" id="bekerja_di" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['bekerja_di'];} ?>"/>
					</td>
				</tr>
			
			<?php
			}
			?>
		
		<?php
		}
		?>
		
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>