<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "siswa";

if($_POST['mod']=="simpanData")
{
	$tanggal_pendaftaran = mysql_real_escape_string($_POST['tanggal_pendaftaran']);
	$nama_lengkap = mysql_real_escape_string($_POST['nama_lengkap']);
	$nama_panggilan = mysql_real_escape_string($_POST['nama_panggilan']);
	$nipd = mysql_real_escape_string($_POST['nipd']);
	$tanggal_masuk_sekolah = mysql_real_escape_string($_POST['tanggal_masuk_sekolah']);
	$jenis_kelamin = mysql_real_escape_string($_POST['jenis_kelamin']);
	$nisn = mysql_real_escape_string($_POST['nisn']);
	$tempat_lahir = mysql_real_escape_string($_POST['tempat_lahir']);
	$tanggal_lahir = mysql_real_escape_string($_POST['tanggal_lahir']);
	$nik = mysql_real_escape_string($_POST['nik']);
	$agama = mysql_real_escape_string($_POST['agama']);
	$alamat = mysql_real_escape_string($_POST['alamat']);
	
	$rt = mysql_real_escape_string($_POST['rt']);
	$rw = mysql_real_escape_string($_POST['rw']);
	$dusun = mysql_real_escape_string($_POST['dusun']);
	$kelurahan = mysql_real_escape_string($_POST['kelurahan']);
	$kecamatan = mysql_real_escape_string($_POST['kecamatan']);
	$kode_pos = mysql_real_escape_string($_POST['kode_pos']);
	$kewarganegaraan = mysql_real_escape_string($_POST['kewarganegaraan']);
	$jenis_tinggal = mysql_real_escape_string($_POST['jenis_tinggal']);
	$alat_transportasi = mysql_real_escape_string($_POST['alat_transportasi']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$nomor_hp = mysql_real_escape_string($_POST['nomor_hp']);
	
	$email = mysql_real_escape_string($_POST['email']);
	$skhun = mysql_real_escape_string($_POST['skhun']);
	$penerima_kps = mysql_real_escape_string($_POST['penerima_kps']);
	$nomor_kps = mysql_real_escape_string($_POST['nomor_kps']);
	$nama_ayah = mysql_real_escape_string($_POST['nama_ayah']);
	$tahun_lahir_ayah = mysql_real_escape_string($_POST['tahun_lahir_ayah']);
	$jenjang_pendidikan_ayah = mysql_real_escape_string($_POST['jenjang_pendidikan_ayah']);
	$pekerjaan_ayah = mysql_real_escape_string($_POST['pekerjaan_ayah']);
	$penghasilan_ayah = mysql_real_escape_string($_POST['penghasilan_ayah']);
	$nik_ayah = mysql_real_escape_string($_POST['nik_ayah']);
	$kebutuhan_khusus_ayah = mysql_real_escape_string($_POST['kebutuhan_khusus_ayah']);
	
	$nama_ibu = mysql_real_escape_string($_POST['nama_ibu']);
	$tahun_lahir_ibu = mysql_real_escape_string($_POST['tahun_lahir_ibu']);
	$jenjang_pendidikan_ibu = mysql_real_escape_string($_POST['jenjang_pendidikan_ibu']);
	$pekerjaan_ibu = mysql_real_escape_string($_POST['pekerjaan_ibu']);
	$penghasilan_ibu = mysql_real_escape_string($_POST['penghasilan_ibu']);
	$nik_ibu = mysql_real_escape_string($_POST['nik_ibu']);
	$kebutuhan_khusus_ibu = mysql_real_escape_string($_POST['kebutuhan_khusus_ibu']);
	$nama_wali = mysql_real_escape_string($_POST['nama_wali']);
	$tahun_lahir_wali = mysql_real_escape_string($_POST['tahun_lahir_wali']);
	$jenjang_pendidikan_wali = mysql_real_escape_string($_POST['jenjang_pendidikan_wali']);
	$pekerjaan_wali = mysql_real_escape_string($_POST['pekerjaan_wali']);
	
	$penghasilan_wali = mysql_real_escape_string($_POST['penghasilan_wali']);
	$nik_wali = mysql_real_escape_string($_POST['nik_wali']);
	$kebutuhan_khusus_wali = mysql_real_escape_string($_POST['kebutuhan_khusus_wali']);
	$nomor_peserta_ujian_nasional = mysql_real_escape_string($_POST['nomor_peserta_ujian_nasional']);
	$nomor_seri_ijazah = mysql_real_escape_string($_POST['nomor_seri_ijazah']);
	$penerima_kip = mysql_real_escape_string($_POST['penerima_kip']);
	$nomor_kip = mysql_real_escape_string($_POST['nomor_kip']);
	$nama_kip = mysql_real_escape_string($_POST['nama_kip']);
	$nomor_kks = mysql_real_escape_string($_POST['nomor_kks']);
	$nomor_registrasi_akta_lahir = mysql_real_escape_string($_POST['nomor_registrasi_akta_lahir']);
	$bank = mysql_real_escape_string($_POST['bank']);
	
	$nomor_rekening_bank = mysql_real_escape_string($_POST['nomor_rekening_bank']);
	$rekening_atas_nama = mysql_real_escape_string($_POST['rekening_atas_nama']);
	$layak_pip = mysql_real_escape_string($_POST['layak_pip']);
	$alasan_layak_pip = mysql_real_escape_string($_POST['alasan_layak_pip']);
	$kebutuhan_khusus = mysql_real_escape_string($_POST['kebutuhan_khusus']);
	$hobi = mysql_real_escape_string($_POST['hobi']);
	$jenis_pendaftaran = mysql_real_escape_string($_POST['jenis_pendaftaran']);
	$sekolah_asal = mysql_real_escape_string($_POST['sekolah_asal']);
	$sekolah_asal_pindahan = mysql_real_escape_string($_POST['sekolah_asal_pindahan']);
	$anak_ke_berapa = mysql_real_escape_string($_POST['anak_ke_berapa']);
	$lintang = mysql_real_escape_string($_POST['lintang']);
	$bujur = mysql_real_escape_string($_POST['bujur']);
	$nomor_kk = mysql_real_escape_string($_POST['nomor_kk']);
	
	$golongan_darah = mysql_real_escape_string($_POST['golongan_darah']);
	$berat_badan = mysql_real_escape_string($_POST['berat_badan']);
	$tinggi_badan = mysql_real_escape_string($_POST['tinggi_badan']);
	$lingkar_kepala = mysql_real_escape_string($_POST['lingkar_kepala']);
	$jumlah_saudara_kandung = mysql_real_escape_string($_POST['jumlah_saudara_kandung']);
	$jumlah_saudara_tiri = mysql_real_escape_string($_POST['jumlah_saudara_tiri']);
	$jumlah_saudara_angkat = mysql_real_escape_string($_POST['jumlah_saudara_angkat']);
	$jarak_rumah = mysql_real_escape_string($_POST['jarak_rumah']);
	
	$foto = (isset($_FILES['foto']['name']) ? $_FILES['foto']['name'] : "");
	
	if(empty($_POST['nama_lengkap']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Lengkap Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		//cek nipd dan nisn ditiadakan dulu
		// $cekNipd = mysql_num_rows(mysql_query("SELECT nipd FROM $nama_tabel WHERE nipd = '$nipd'"));
		// $cekNisn = mysql_num_rows(mysql_query("SELECT nisn FROM $nama_tabel WHERE nisn = '$nisn'"));
		// if($cekNipd > 0)
		// {
			// echo "
			// <script>
				// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'NIPD Sudah Digunakan!');
			// </script>";
		// }
		// else if($cekNisn > 0)
		// {
			// echo "
			// <script>
				// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'NISN Sudah Digunakan!');
			// </script>";
		// }
		// else
		// {
			$simpanData = mysql_query("INSERT INTO $nama_tabel (
											tanggal_pendaftaran,
											nama_lengkap,
											nama_panggilan,
											nipd,
											tanggal_masuk_sekolah,
											jenis_kelamin,
											nisn,
											tempat_lahir,
											tanggal_lahir,
											nik,
											agama,
											alamat,
											
											rt,
											rw,
											dusun,
											kelurahan,
											kecamatan,
											kode_pos,
											kewarganegaraan,
											jenis_tinggal,
											alat_transportasi,
											nomor_telepon,
											nomor_hp,
											
											email,
											skhun,
											penerima_kps,
											nomor_kps,
											nama_ayah,
											tahun_lahir_ayah,
											jenjang_pendidikan_ayah,
											pekerjaan_ayah,
											penghasilan_ayah,
											nik_ayah,
											kebutuhan_khusus_ayah,
											
											nama_ibu,
											tahun_lahir_ibu,
											jenjang_pendidikan_ibu,
											pekerjaan_ibu,
											penghasilan_ibu,
											nik_ibu,
											kebutuhan_khusus_ibu,
											nama_wali,
											tahun_lahir_wali,
											jenjang_pendidikan_wali,
											pekerjaan_wali,
											
											penghasilan_wali,
											nik_wali,
											kebutuhan_khusus_wali,
											nomor_peserta_ujian_nasional,
											nomor_seri_ijazah,
											penerima_kip,
											nomor_kip,
											/*nama_kip,*/
											nomor_kks,
											nomor_registrasi_akta_lahir,
											bank,
											
											nomor_rekening_bank,
											rekening_atas_nama,
											layak_pip,
											alasan_layak_pip,
											kebutuhan_khusus,
											hobi,
											jenis_pendaftaran,
											sekolah_asal,
											sekolah_asal_pindahan,
											anak_ke_berapa,
											lintang,
											bujur,
											nomor_kk,
											
											golongan_darah,
											berat_badan,
											tinggi_badan,
											lingkar_kepala,
											jumlah_saudara_kandung,
											jumlah_saudara_tiri,
											jumlah_saudara_angkat,
											jarak_rumah,
											
											tanggal_ditambah,
											jam_ditambah,
											ditambah_oleh
										)
										VALUE
										(
											'$tanggal_pendaftaran',
											'$nama_lengkap',
											'$nama_panggilan',
											'$nipd',
											'$tanggal_masuk_sekolah',
											'$jenis_kelamin',
											'$nisn',
											'$tempat_lahir',
											'$tanggal_lahir',
											'$nik',
											'$agama',
											'$alamat',
											
											'$rt',
											'$rw',
											'$dusun',
											'$kelurahan',
											'$kecamatan',
											'$kode_pos',
											'$kewarganegaraan',
											'$jenis_tinggal',
											'$alat_transportasi',
											'$nomor_telepon',
											'$nomor_hp',
											
											'$email',
											'$skhun',
											'$penerima_kps',
											'$nomor_kps',
											'$nama_ayah',
											'$tahun_lahir_ayah',
											'$jenjang_pendidikan_ayah',
											'$pekerjaan_ayah',
											'$penghasilan_ayah',
											'$nik_ayah',
											'$kebutuhan_khusus_ayah',
											
											'$nama_ibu',
											'$tahun_lahir_ibu',
											'$jenjang_pendidikan_ibu',
											'$pekerjaan_ibu',
											'$penghasilan_ibu',
											'$nik_ibu',
											'$kebutuhan_khusus_ibu',
											'$nama_wali',
											'$tahun_lahir_wali',
											'$jenjang_pendidikan_wali',
											'$pekerjaan_wali',
											
											'$penghasilan_wali',
											'$nik_wali',
											'$kebutuhan_khusus_wali',
											'$nomor_peserta_ujian_nasional',
											'$nomor_seri_ijazah',
											'$penerima_kip',
											'$nomor_kip',
											/*'$nama_kip',*/
											'$nomor_kks',
											'$nomor_registrasi_akta_lahir',
											'$bank',
											
											'$nomor_rekening_bank',
											'$rekening_atas_nama',
											'$layak_pip',
											'$alasan_layak_pip',
											'$kebutuhan_khusus',
											'$hobi',
											'$jenis_pendaftaran',
											'$sekolah_asal',
											'$sekolah_asal_pindahan',
											'$anak_ke_berapa',
											'$lintang',
											'$bujur',
											'$nomor_kk',
											
											'$golongan_darah',
											'$berat_badan',
											'$tinggi_badan',
											'$lingkar_kepala',
											'$jumlah_saudara_kandung',
											'$jumlah_saudara_tiri',
											'$jumlah_saudara_angkat',
											'$jarak_rumah',
											
											'$tanggal_sekarang',
											'$jam_sekarang',
											'$_SESSION[username]'
										)");
			
			if(!empty($_FILES['foto']['name']))
			{
				$jenis_file = array("image/jpg", "image/jpeg", "image/png", "image/gif");
				if(in_array($_FILES['foto']['type'], $jenis_file))
				{
					function UploadFile($foto)
					{
						$vdir_upload = "../../images/siswa/";
						$vfile_upload = $vdir_upload . $foto;
						
						move_uploaded_file($_FILES["foto"]["tmp_name"], $vfile_upload);
					}
						
					$lokasi_file = $_FILES['foto']['tmp_name'];
					$nama_file = $_FILES['foto']['name'];
					$acak = rand(1,99999);
					$nama_file_unik = $acak . $nama_file;
					UploadFile($nama_file_unik);
					
					$siswa = mysql_fetch_array(mysql_query("SELECT MAX(id) id FROM siswa"));

					mysql_query("UPDATE siswa SET foto = '$nama_file_unik' WHERE id = '$siswa[id]'");
				}
			}
			
			if($simpanData)
			{
				$aktivitas = "Tambah Siswa";
				$keterangan = mysql_real_escape_string("Menambahkan '$nama_lengkap' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Ditambahkan!');
					$('#form1').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
				</script>
				";
			}
		// }
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	
	$tanggal_pendaftaran = mysql_real_escape_string($_POST['tanggal_pendaftaran']);
	$nama_lengkap = mysql_real_escape_string($_POST['nama_lengkap']);
	$nama_panggilan = mysql_real_escape_string($_POST['nama_panggilan']);
	$nipd = mysql_real_escape_string($_POST['nipd']);
	$tanggal_masuk_sekolah = mysql_real_escape_string($_POST['tanggal_masuk_sekolah']);
	$jenis_kelamin = mysql_real_escape_string($_POST['jenis_kelamin']);
	$nisn = mysql_real_escape_string($_POST['nisn']);
	$tempat_lahir = mysql_real_escape_string($_POST['tempat_lahir']);
	$tanggal_lahir = mysql_real_escape_string($_POST['tanggal_lahir']);
	$nik = mysql_real_escape_string($_POST['nik']);
	$agama = mysql_real_escape_string($_POST['agama']);
	$alamat = mysql_real_escape_string($_POST['alamat']);
	
	$rt = mysql_real_escape_string($_POST['rt']);
	$rw = mysql_real_escape_string($_POST['rw']);
	$dusun = mysql_real_escape_string($_POST['dusun']);
	$kelurahan = mysql_real_escape_string($_POST['kelurahan']);
	$kecamatan = mysql_real_escape_string($_POST['kecamatan']);
	$kode_pos = mysql_real_escape_string($_POST['kode_pos']);
	$kewarganegaraan = mysql_real_escape_string($_POST['kewarganegaraan']);
	$jenis_tinggal = mysql_real_escape_string($_POST['jenis_tinggal']);
	$alat_transportasi = mysql_real_escape_string($_POST['alat_transportasi']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$nomor_hp = mysql_real_escape_string($_POST['nomor_hp']);
	
	$email = mysql_real_escape_string($_POST['email']);
	$skhun = mysql_real_escape_string($_POST['skhun']);
	$penerima_kps = mysql_real_escape_string($_POST['penerima_kps']);
	$nomor_kps = mysql_real_escape_string($_POST['nomor_kps']);
	$nama_ayah = mysql_real_escape_string($_POST['nama_ayah']);
	$tahun_lahir_ayah = mysql_real_escape_string($_POST['tahun_lahir_ayah']);
	$jenjang_pendidikan_ayah = mysql_real_escape_string($_POST['jenjang_pendidikan_ayah']);
	$pekerjaan_ayah = mysql_real_escape_string($_POST['pekerjaan_ayah']);
	$penghasilan_ayah = mysql_real_escape_string($_POST['penghasilan_ayah']);
	$nik_ayah = mysql_real_escape_string($_POST['nik_ayah']);
	$kebutuhan_khusus_ayah = mysql_real_escape_string($_POST['kebutuhan_khusus_ayah']);
	
	$nama_ibu = mysql_real_escape_string($_POST['nama_ibu']);
	$tahun_lahir_ibu = mysql_real_escape_string($_POST['tahun_lahir_ibu']);
	$jenjang_pendidikan_ibu = mysql_real_escape_string($_POST['jenjang_pendidikan_ibu']);
	$pekerjaan_ibu = mysql_real_escape_string($_POST['pekerjaan_ibu']);
	$penghasilan_ibu = mysql_real_escape_string($_POST['penghasilan_ibu']);
	$nik_ibu = mysql_real_escape_string($_POST['nik_ibu']);
	$kebutuhan_khusus_ibu = mysql_real_escape_string($_POST['kebutuhan_khusus_ibu']);
	$nama_wali = mysql_real_escape_string($_POST['nama_wali']);
	$tahun_lahir_wali = mysql_real_escape_string($_POST['tahun_lahir_wali']);
	$jenjang_pendidikan_wali = mysql_real_escape_string($_POST['jenjang_pendidikan_wali']);
	$pekerjaan_wali = mysql_real_escape_string($_POST['pekerjaan_wali']);
	
	$penghasilan_wali = mysql_real_escape_string($_POST['penghasilan_wali']);
	$nik_wali = mysql_real_escape_string($_POST['nik_wali']);
	$kebutuhan_khusus_wali = mysql_real_escape_string($_POST['kebutuhan_khusus_wali']);
	$nomor_peserta_ujian_nasional = mysql_real_escape_string($_POST['nomor_peserta_ujian_nasional']);
	$nomor_seri_ijazah = mysql_real_escape_string($_POST['nomor_seri_ijazah']);
	$penerima_kip = mysql_real_escape_string($_POST['penerima_kip']);
	$nomor_kip = mysql_real_escape_string($_POST['nomor_kip']);
	$nama_kip = mysql_real_escape_string($_POST['nama_kip']);
	$nomor_kks = mysql_real_escape_string($_POST['nomor_kks']);
	$nomor_registrasi_akta_lahir = mysql_real_escape_string($_POST['nomor_registrasi_akta_lahir']);
	$bank = mysql_real_escape_string($_POST['bank']);
	
	$nomor_rekening_bank = mysql_real_escape_string($_POST['nomor_rekening_bank']);
	$rekening_atas_nama = mysql_real_escape_string($_POST['rekening_atas_nama']);
	$layak_pip = mysql_real_escape_string($_POST['layak_pip']);
	$alasan_layak_pip = mysql_real_escape_string($_POST['alasan_layak_pip']);
	$kebutuhan_khusus = mysql_real_escape_string($_POST['kebutuhan_khusus']);
	$hobi = mysql_real_escape_string($_POST['hobi']);
	$jenis_pendaftaran = mysql_real_escape_string($_POST['jenis_pendaftaran']);
	$sekolah_asal = mysql_real_escape_string($_POST['sekolah_asal']);
	$sekolah_asal_pindahan = mysql_real_escape_string($_POST['sekolah_asal_pindahan']);
	$anak_ke_berapa = mysql_real_escape_string($_POST['anak_ke_berapa']);
	$lintang = mysql_real_escape_string($_POST['lintang']);
	$bujur = mysql_real_escape_string($_POST['bujur']);
	$nomor_kk = mysql_real_escape_string($_POST['nomor_kk']);
	
	$golongan_darah = mysql_real_escape_string($_POST['golongan_darah']);
	$berat_badan = mysql_real_escape_string($_POST['berat_badan']);
	$tinggi_badan = mysql_real_escape_string($_POST['tinggi_badan']);
	$lingkar_kepala = mysql_real_escape_string($_POST['lingkar_kepala']);
	$jumlah_saudara_kandung = mysql_real_escape_string($_POST['jumlah_saudara_kandung']);
	$jumlah_saudara_tiri = mysql_real_escape_string($_POST['jumlah_saudara_tiri']);
	$jumlah_saudara_angkat = mysql_real_escape_string($_POST['jumlah_saudara_angkat']);
	$jarak_rumah = mysql_real_escape_string($_POST['jarak_rumah']);
	$keluar_karena = mysql_real_escape_string($_POST['keluar_karena']);
	$tanggal_keluar = mysql_real_escape_string($_POST['tanggal_keluar']);
	$alasan_keluar = mysql_real_escape_string($_POST['alasan_keluar']);
	$melanjutkan_di = mysql_real_escape_string($_POST['melanjutkan_di']);
	$bekerja_di = mysql_real_escape_string($_POST['bekerja_di']);
	
	$foto = (isset($_FILES['foto']['name']) ? $_FILES['foto']['name'] : "");
	
	if(empty($_POST['nama_lengkap']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Lengkap Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$siswa = mysql_query("SELECT nipd, nisn FROM $nama_tabel WHERE id = '$id'");
		$ambilSiswa = mysql_fetch_array($siswa);
		//cek nipd dan nisn ditiadakan dulu
		// $cekNipd = mysql_num_rows(mysql_query("SELECT nipd FROM $nama_tabel WHERE nipd = '$nipd'"));
		// $cekNisn = mysql_num_rows(mysql_query("SELECT nisn FROM $nama_tabel WHERE nisn = '$nisn'"));
		// if($ambilSiswa['nipd'] != $nipd and $cekNipd > 0)
		// {
			// echo "
			// <script>
				// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'NIPD Sudah Digunakan!');
			// </script>";
		// }
		// else if($ambilSiswa['nisn'] != $nisn and $cekNisn > 0)
		// {
			// echo "
			// <script>
				// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'NISN Sudah Digunakan!');
			// </script>";
		// }
		// else
		// {
			$perbaruiData = mysql_query("UPDATE $nama_tabel SET
											tanggal_pendaftaran = '$tanggal_pendaftaran',
											nama_lengkap = '$nama_lengkap',
											nama_panggilan = '$nama_panggilan',
											nipd = '$nipd',
											tanggal_masuk_sekolah = '$tanggal_masuk_sekolah',
											jenis_kelamin = '$jenis_kelamin',
											nisn = '$nisn',
											tempat_lahir = '$tempat_lahir',
											tanggal_lahir = '$tanggal_lahir',
											nik = '$nik',
											agama = '$agama',
											alamat = '$alamat',
											
											rt = '$rt',
											rw = '$rw',
											dusun = '$dusun',
											kelurahan = '$kelurahan',
											kecamatan = '$kecamatan',
											kode_pos = '$kode_pos',
											kewarganegaraan = '$kewarganegaraan',
											jenis_tinggal = '$jenis_tinggal',
											alat_transportasi = '$alat_transportasi',
											nomor_telepon = '$nomor_telepon',
											nomor_hp = '$nomor_hp',
											
											email = '$email',
											skhun = '$skhun',
											penerima_kps = '$penerima_kps',
											nomor_kps = '$nomor_kps',
											nama_ayah = '$nama_ayah',
											tahun_lahir_ayah = '$tahun_lahir_ayah',
											jenjang_pendidikan_ayah = '$jenjang_pendidikan_ayah',
											pekerjaan_ayah = '$pekerjaan_ayah',
											penghasilan_ayah = '$penghasilan_ayah',
											nik_ayah = '$nik_ayah',
											kebutuhan_khusus_ayah = '$kebutuhan_khusus_ayah',
											
											nama_ibu = '$nama_ibu',
											tahun_lahir_ibu = '$tahun_lahir_ibu',
											jenjang_pendidikan_ibu = '$jenjang_pendidikan_ibu',
											pekerjaan_ibu = '$pekerjaan_ibu',
											penghasilan_ibu = '$penghasilan_ibu',
											nik_ibu = '$nik_ibu',
											kebutuhan_khusus_ibu = '$kebutuhan_khusus_ibu',
											nama_wali = '$nama_wali',
											tahun_lahir_wali = '$tahun_lahir_wali',
											jenjang_pendidikan_wali = '$jenjang_pendidikan_wali',
											pekerjaan_wali = '$pekerjaan_wali',
											
											penghasilan_wali = '$penghasilan_wali',
											nik_wali = '$nik_wali',
											kebutuhan_khusus_wali = '$kebutuhan_khusus_wali',
											nomor_peserta_ujian_nasional = '$nomor_peserta_ujian_nasional',
											nomor_seri_ijazah = '$nomor_seri_ijazah',
											penerima_kip = '$penerima_kip',
											nomor_kip = '$nomor_kip',
											/*nama_kip = '$nama_kip',*/
											nomor_kks = '$nomor_kks',
											nomor_registrasi_akta_lahir = '$nomor_registrasi_akta_lahir',
											bank = '$bank',
											
											nomor_rekening_bank = '$nomor_rekening_bank',
											rekening_atas_nama = '$rekening_atas_nama',
											layak_pip = '$layak_pip',
											alasan_layak_pip = '$alasan_layak_pip',
											kebutuhan_khusus = '$kebutuhan_khusus',
											hobi = '$hobi',
											jenis_pendaftaran = '$jenis_pendaftaran',
											sekolah_asal = '$sekolah_asal',
											sekolah_asal_pindahan = '$sekolah_asal_pindahan',
											anak_ke_berapa = '$anak_ke_berapa',
											lintang = '$lintang',
											bujur = '$bujur',
											nomor_kk = '$nomor_kk',
											
											golongan_darah = '$golongan_darah',
											berat_badan = '$berat_badan',
											tinggi_badan = '$tinggi_badan',
											lingkar_kepala = '$lingkar_kepala',
											jumlah_saudara_kandung = '$jumlah_saudara_kandung',
											jumlah_saudara_tiri = '$jumlah_saudara_tiri',
											jumlah_saudara_angkat = '$jumlah_saudara_angkat',
											jarak_rumah = '$jarak_rumah',
											keluar_karena = '$keluar_karena',
											tanggal_keluar = '$tanggal_keluar',
											alasan_keluar = '$alasan_keluar',
											
											tanggal_diperbarui = '$tanggal_sekarang',
											jam_diperbarui = '$jam_sekarang',
											diperbarui_oleh = '$_SESSION[username]'
										WHERE id = '$id'");
			
			if(!empty($_FILES['foto']['name']))
			{
				$jenis_file = array("image/jpg", "image/jpeg", "image/png", "image/gif");
				if(in_array($_FILES['foto']['type'], $jenis_file))
				{
					$ambilSiswa = mysql_fetch_array(mysql_query("SELECT * FROM siswa WHERE id = '$id'"));
				
					if($ambilSiswa['foto'] != "")
					{
						unlink ("../../images/siswa/$ambilSiswa[foto]");		
					}
				
					function UploadFile($foto)
					{
						$vdir_upload = "../../images/siswa/";
						$vfile_upload = $vdir_upload . $foto;
						
						move_uploaded_file($_FILES["foto"]["tmp_name"], $vfile_upload);
					}
						
					$lokasi_file = $_FILES['foto']['tmp_name'];
					$nama_file = $_FILES['foto']['name'];
					$acak = rand(1,99999);
					$nama_file_unik = $acak . $nama_file;
					UploadFile($nama_file_unik);
					
					mysql_query("UPDATE siswa SET foto = '$nama_file_unik' WHERE id = '$id'");
				}
			}
			
			if($perbaruiData)
			{
				$aktivitas = "Perbarui Siswa";
				$keterangan = mysql_real_escape_string("Memperbarui '$nama_lengkap' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
					$('#form1').modal('hide');
					$('#form2').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
				</script>
				";
			}
		// }
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_lengkap FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_lengkap'];
	
	$ambilSiswa = mysql_fetch_array(mysql_query("SELECT * FROM $nama_tabel WHERE id = '$id'"));
		
	if($ambilSiswa['foto'] != "")
	{
		unlink ("../../images/siswa/$ambilSiswa[foto]");
	}
	
	$hapusRiwayatRombel = mysql_query("DELETE FROM riwayat_rombel_siswa WHERE id_siswa = '$id'");
	
	$nilai = mysql_query("SELECT * FROM nilai WHERE id_siswa = '$id'");
	while($ambilNilai = mysql_fetch_array($nilai))
	{
		$hapusNilaiDetail = mysql_query("DELETE FROM nilai_detail WHERE id_nilai = '$ambilNilai[id]'");
	}
	
	$hapusNilai = mysql_query("DELETE FROM nilai WHERE id_siswa = '$id'");
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
	
	if($hapusData)
	{
		$aktivitas = "Hapus Siswa";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$dataSiswa = mysql_query("SELECT * FROM $nama_tabel WHERE id IN ($data_terpilih)");
	while($ambilSiswa = mysql_fetch_array($dataSiswa))
	{
		if($ambilSiswa['foto'] != "")
		{
			unlink ("../../images/siswa/$ambilSiswa[foto]");	
		}
	}
	
	$hapusRiwayatRombel = mysql_query("DELETE FROM riwayat_rombel_siswa WHERE id_siswa IN ($data_terpilih)");
	
	$nilai = mysql_query("SELECT * FROM nilai WHERE id_siswa IN ($data_terpilih)");
	while($ambilNilai = mysql_fetch_array($nilai))
	{
		$hapusNilaiDetail = mysql_query("DELETE FROM nilai_detail WHERE id_nilai = '$ambilNilai[id]'");
	}
	
	$hapusNilai = mysql_query("DELETE FROM nilai WHERE id_siswa IN ($data_terpilih)");
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
	
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="pindahRombelDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	$id_rombel = $_POST['id_rombel'];
	$keterangan = $_POST['keterangan'];
	
	$nama_rombel = mysql_fetch_row(mysql_query("SELECT nama_rombel FROM rombel WHERE id = '$id_rombel'"));
	
	$id_siswa = explode(",", str_replace(" ", "", $data_terpilih));
	foreach($id_siswa as $id_siswa)
	{
		$simpanRiwayatRombelSiswa = mysql_query("INSERT INTO riwayat_rombel_siswa (id_siswa, nama_rombel, keterangan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_siswa', '$nama_rombel[0]', '$keterangan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
	}
	
	$pindahRombelDataTerpilih = mysql_query("UPDATE $nama_tabel SET id_rombel = '$id_rombel', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	
	if($pindahRombelDataTerpilih)
	{
		$aktivitas = "Perbarui Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Memperbarui Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
		</script>
		";
	}
}

if($_POST['mod']=="prosesKelulusanDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	$tanggal_kelulusan = $_POST['tanggal_kelulusan'];
	
	$prosesKelulusanDataTerpilih = mysql_query("UPDATE $nama_tabel SET keluar_karena = 'Lulus', tanggal_keluar = '$tanggal_kelulusan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	
	if($prosesKelulusanDataTerpilih)
	{
		$aktivitas = "Perbarui Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Memperbarui Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
		</script>
		";
	}
}

if($_POST['mod']=="simpanDataImport")
{
	if(!empty($_FILES['file']['name']))
	{
		$jenis_file = array("application/excel", "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		if(in_array($_FILES['file']['type'], $jenis_file))
		{
			$target_dir = "../../uploads/import/";
			$name_file = basename($_FILES['file']['name']) ;
			$target_file = $target_dir . $name_file;
			move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

			require_once "../../libraries/PHPExcel.php";
			require_once "../../libraries/PHPExcel/IOFactory.php";

			$file = $target_dir . $name_file;

			$objPHPExcel = PHPExcel_IOFactory::load($file);
			
			$success = 0;
			$error = 0;
			$keterangan_error = "";
			
			foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
			{
				$totalrow = $worksheet->getHighestRow();
				
				for($row = 7; $row <= $totalrow; $row++)
				{
					$nama_lengkap = mysql_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
					$nipd = mysql_escape_string($worksheet->getCellByColumnAndRow(2, $row)->getValue());
					$jenis_kelamin = mysql_escape_string($worksheet->getCellByColumnAndRow(3, $row)->getValue());
					$nisn = mysql_escape_string($worksheet->getCellByColumnAndRow(4, $row)->getValue());
					$tempat_lahir = mysql_escape_string($worksheet->getCellByColumnAndRow(5, $row)->getValue());
					$tanggal_lahir = mysql_escape_string($worksheet->getCellByColumnAndRow(6, $row)->getValue());
					$nik = mysql_escape_string($worksheet->getCellByColumnAndRow(7, $row)->getValue());
					$agama = mysql_escape_string($worksheet->getCellByColumnAndRow(8, $row)->getValue());
					$alamat = mysql_escape_string($worksheet->getCellByColumnAndRow(9, $row)->getValue());
					
					$rt = mysql_escape_string($worksheet->getCellByColumnAndRow(10, $row)->getValue());
					$rw = mysql_escape_string($worksheet->getCellByColumnAndRow(11, $row)->getValue());
					$dusun = mysql_escape_string($worksheet->getCellByColumnAndRow(12, $row)->getValue());
					$kelurahan = mysql_escape_string($worksheet->getCellByColumnAndRow(13, $row)->getValue());
					$kecamatan = mysql_escape_string($worksheet->getCellByColumnAndRow(14, $row)->getValue());
					$kode_pos = mysql_escape_string($worksheet->getCellByColumnAndRow(15, $row)->getValue());
					$jenis_tinggal = mysql_escape_string($worksheet->getCellByColumnAndRow(16, $row)->getValue());
					$alat_transportasi = mysql_escape_string($worksheet->getCellByColumnAndRow(17, $row)->getValue());
					$nomor_telepon = mysql_escape_string($worksheet->getCellByColumnAndRow(18, $row)->getValue());
					$nomor_hp = mysql_escape_string($worksheet->getCellByColumnAndRow(19, $row)->getValue());
					
					$email = mysql_escape_string($worksheet->getCellByColumnAndRow(20, $row)->getValue());
					$skhun = mysql_escape_string($worksheet->getCellByColumnAndRow(21, $row)->getValue());
					$penerima_kps = mysql_escape_string($worksheet->getCellByColumnAndRow(22, $row)->getValue());
					$nomor_kps = mysql_escape_string($worksheet->getCellByColumnAndRow(23, $row)->getValue());
					$nama_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(24, $row)->getValue());
					$tahun_lahir_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(25, $row)->getValue());
					$jenjang_pendidikan_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(26, $row)->getValue());
					$pekerjaan_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(27, $row)->getValue());
					$penghasilan_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(28, $row)->getValue());
					$nik_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(29, $row)->getValue());
					
					$nama_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(30, $row)->getValue());
					$tahun_lahir_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(31, $row)->getValue());
					$jenjang_pendidikan_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(32, $row)->getValue());
					$pekerjaan_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(33, $row)->getValue());
					$penghasilan_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(34, $row)->getValue());
					$nik_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(35, $row)->getValue());
					$nama_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(36, $row)->getValue());
					$tahun_lahir_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(37, $row)->getValue());
					$jenjang_pendidikan_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(38, $row)->getValue());
					$pekerjaan_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(39, $row)->getValue());
					
					$penghasilan_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(40, $row)->getValue());
					$nik_wali = mysql_escape_string($worksheet->getCellByColumnAndRow(41, $row)->getValue());
					$nomor_peserta_ujian_nasional = mysql_escape_string($worksheet->getCellByColumnAndRow(43, $row)->getValue());
					$nomor_seri_ijazah = mysql_escape_string($worksheet->getCellByColumnAndRow(44, $row)->getValue());
					$penerima_kip = mysql_escape_string($worksheet->getCellByColumnAndRow(45, $row)->getValue());
					$nomor_kip = mysql_escape_string($worksheet->getCellByColumnAndRow(46, $row)->getValue());
					$nama_kip = mysql_escape_string($worksheet->getCellByColumnAndRow(47, $row)->getValue());
					$nomor_kks = mysql_escape_string($worksheet->getCellByColumnAndRow(48, $row)->getValue());
					$nomor_registrasi_akta_lahir = mysql_escape_string($worksheet->getCellByColumnAndRow(49, $row)->getValue());
					$bank = mysql_escape_string($worksheet->getCellByColumnAndRow(50, $row)->getValue());
					
					$nomor_rekening_bank = mysql_escape_string($worksheet->getCellByColumnAndRow(51, $row)->getValue());
					$rekening_atas_nama = mysql_escape_string($worksheet->getCellByColumnAndRow(52, $row)->getValue());
					$layak_pip = mysql_escape_string($worksheet->getCellByColumnAndRow(53, $row)->getValue());
					$alasan_layak_pip = mysql_escape_string($worksheet->getCellByColumnAndRow(54, $row)->getValue());
					$kebutuhan_khusus = mysql_escape_string($worksheet->getCellByColumnAndRow(55, $row)->getValue());
					$sekolah_asal = mysql_escape_string($worksheet->getCellByColumnAndRow(56, $row)->getValue());
					$anak_ke_berapa = mysql_escape_string($worksheet->getCellByColumnAndRow(57, $row)->getValue());
					$lintang = mysql_escape_string($worksheet->getCellByColumnAndRow(58, $row)->getValue());
					$bujur = mysql_escape_string($worksheet->getCellByColumnAndRow(59, $row)->getValue());
					$nomor_kk = mysql_escape_string($worksheet->getCellByColumnAndRow(60, $row)->getValue());
					
					$berat_badan = mysql_escape_string($worksheet->getCellByColumnAndRow(61, $row)->getValue());
					$tinggi_badan = mysql_escape_string($worksheet->getCellByColumnAndRow(62, $row)->getValue());
					$lingkar_kepala = mysql_escape_string($worksheet->getCellByColumnAndRow(63, $row)->getValue());
					$jumlah_saudara_kandung = mysql_escape_string($worksheet->getCellByColumnAndRow(64, $row)->getValue());
					$jarak_rumah = mysql_escape_string($worksheet->getCellByColumnAndRow(65, $row)->getValue());
					
					$nama_rombel = mysql_escape_string($worksheet->getCellByColumnAndRow(42, $row)->getValue());
					
					if($nama_lengkap == "")
					{
						$error++;
						$keterangan_error .= "<br/>Pada Baris $row, Nama Lengkap Tidak Boleh Kosong.";
					}
					else
					{
						//cek nipd dan nisn ditiadakan dulu
						// $cekNipd = mysql_num_rows(mysql_query("SELECT nipd FROM $nama_tabel WHERE nipd = '$nipd'"));
						// $cekNisn = mysql_num_rows(mysql_query("SELECT nisn FROM $nama_tabel WHERE nisn = '$nisn'"));
						// if($cekNipd > 0)
						// {
								// $error++;
								// $keterangan_error .= "<br/>Pada Baris $row, NIPD Sudah Digunakan.";
						// }
						// else if($cekNisn > 0)
						// {
								// $error++;
								// $keterangan_error .= "<br/>Pada Baris $row, NISN Sudah Digunakan.";
						// }
						// else
						// {
							
						$nama_lengkap_filtered = str_replace(" ", "", strtolower($nama_lengkap));
						$tempat_lahir_filtered = str_replace(" ", "", strtolower($tempat_lahir));
						$nama_ibu_filtered = str_replace(" ", "", strtolower($nama_ibu));
							
						$cekSiswa = mysql_num_rows(mysql_query("SELECT * FROM $nama_tabel
																WHERE REPLACE(LOWER(nama_lengkap), ' ', '') = '$nama_lengkap_filtered'
																AND REPLACE(LOWER(tempat_lahir), ' ', '') = '$tempat_lahir_filtered'
																AND tanggal_lahir = '$tanggal_lahir'
																AND REPLACE(LOWER(nama_ibu), ' ', '') = '$nama_ibu_filtered'
																"));
						if($cekSiswa > 0)
						{
							$error++;
							$keterangan_error .= "<br/>Pada Baris $row, Siswa Sudah Ada.";
						}
						else
						{
							$nama_rombel_filtered = str_replace(" ", "", strtolower($nama_rombel));
							$id_rombel = mysql_fetch_row(mysql_query("SELECT id FROM rombel WHERE REPLACE(LOWER(nama_rombel), ' ', '') = '$nama_rombel_filtered'"));
							$simpanDataImport = mysql_query("INSERT INTO $nama_tabel (
																nama_lengkap,
																nipd,
																jenis_kelamin,
																nisn,
																tempat_lahir,
																tanggal_lahir,
																nik,
																agama,
																alamat,
																
																rt,
																rw,
																dusun,
																kelurahan,
																kecamatan,
																kode_pos,
																jenis_tinggal,
																alat_transportasi,
																nomor_telepon,
																nomor_hp,
																
																email,
																skhun,
																penerima_kps,
																nomor_kps,
																nama_ayah,
																tahun_lahir_ayah,
																jenjang_pendidikan_ayah,
																pekerjaan_ayah,
																penghasilan_ayah,
																nik_ayah,
																
																nama_ibu,
																tahun_lahir_ibu,
																jenjang_pendidikan_ibu,
																pekerjaan_ibu,
																penghasilan_ibu,
																nik_ibu,
																nama_wali,
																tahun_lahir_wali,
																jenjang_pendidikan_wali,
																pekerjaan_wali,
																
																penghasilan_wali,
																nik_wali,
																nomor_peserta_ujian_nasional,
																nomor_seri_ijazah,
																penerima_kip,
																nomor_kip,
																nama_kip,
																nomor_kks,
																nomor_registrasi_akta_lahir,
																bank,
												
																nomor_rekening_bank,
																rekening_atas_nama,
																layak_pip,
																alasan_layak_pip,
																kebutuhan_khusus,
																sekolah_asal,
																anak_ke_berapa,
																lintang,
																bujur,
																nomor_kk,
																
																berat_badan,
																tinggi_badan,
																lingkar_kepala,
																jumlah_saudara_kandung,
																jarak_rumah,
																
																id_rombel,
																
																tanggal_ditambah,
																jam_ditambah,
																ditambah_oleh
															)
															VALUE
															(
																'$nama_lengkap',
																'$nipd',
																'$jenis_kelamin',
																'$nisn',
																'$tempat_lahir',
																'$tanggal_lahir',
																'$nik',
																'$agama',
																'$alamat',
																
																'$rt',
																'$rw',
																'$dusun',
																'$kelurahan',
																'$kecamatan',
																'$kode_pos',
																'$jenis_tinggal',
																'$alat_transportasi',
																'$nomor_telepon',
																'$nomor_hp',
																
																'$email',
																'$skhun',
																'$penerima_kps',
																'$nomor_kps',
																'$nama_ayah',
																'$tahun_lahir_ayah',
																'$jenjang_pendidikan_ayah',
																'$pekerjaan_ayah',
																'$penghasilan_ayah',
																'$nik_ayah',
																
																'$nama_ibu',
																'$tahun_lahir_ibu',
																'$jenjang_pendidikan_ibu',
																'$pekerjaan_ibu',
																'$penghasilan_ibu',
																'$nik_ibu',
																'$nama_wali',
																'$tahun_lahir_wali',
																'$jenjang_pendidikan_wali',
																'$pekerjaan_wali',
																
																'$penghasilan_wali',
																'$nik_wali',
																'$nomor_peserta_ujian_nasional',
																'$nomor_seri_ijazah',
																'$penerima_kip',
																'$nomor_kip',
																'$nama_kip',
																'$nomor_kks',
																'$nomor_registrasi_akta_lahir',
																'$bank',
												
																'$nomor_rekening_bank',
																'$rekening_atas_nama',
																'$layak_pip',
																'$alasan_layak_pip',
																'$kebutuhan_khusus',
																'$sekolah_asal',
																'$anak_ke_berapa',
																'$lintang',
																'$bujur',
																'$nomor_kk',
																
																'$berat_badan',
																'$tinggi_badan',
																'$lingkar_kepala',
																'$jumlah_saudara_kandung',
																'$jarak_rumah',
																
																'$id_rombel[0]',
																
																'$tanggal_sekarang',
																'$jam_sekarang',
																'$_SESSION[username]'
															)");
							
							if($simpanDataImport)
							{
								$aktivitas = "Tambah Siswa";
								$keterangan = mysql_real_escape_string("Menambahkan '$nama_lengkap' Pada Tabel '$nama_tabel'");
								$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
								
								$success++;
							}
							else
							{
								$error++;
								$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
							}
						}
					}	
				}
			}
			
			unlink($target_file);
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Siswa Ditambahkan, $error Error! $keterangan_error');
				$('#form1').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Format File Tidak Valid!');
			</script>
			";
		}
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}

if($_POST['mod']=="simpanDataUploadBatch")
{
	if(!empty($_FILES['file']['name']))
	{
		$jenis_file = array("application/zip", "application/x-zip-compressed", "multipart/x-zip");
		if(in_array($_FILES['file']['type'], $jenis_file))
		{
			$target_dir = "../../uploads/zip/";
			$name_file = basename($_FILES['file']['name']) ;
			$target_file = $target_dir . $name_file;
			move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

			$zip = new ZipArchive;
			$fileZip = $zip->open($target_file);
			if($fileZip === TRUE)
			{
				$zip->extractTo($target_dir);
				$zip->close();
				
				unlink($target_file);
				
				$success = 0;
				$error = 0;
				$keterangan_error = "";
				
				$dataFile = glob($target_dir . "*.*");
				foreach($dataFile as $file)
				{
					$jenisFile = strtolower(pathinfo($file, PATHINFO_EXTENSION));
					$namaFile = pathinfo($file, PATHINFO_FILENAME);
					$namaFileLengkap = pathinfo($file, PATHINFO_BASENAME );
					
					$jenis_file_ = array("jpg", "jpeg", "png", "gif");
					if(!in_array($jenisFile, $jenis_file_) and $namaFileLengkap != "index.php")
					{
						$error++;
						$keterangan_error .= "<br/>Pada File $namaFileLengkap, Format File Tidak Valid.";
						unlink($target_dir . $namaFileLengkap);
					}
					else if($namaFileLengkap != "index.php")
					{
						$cekSiswa = mysql_num_rows(mysql_query("SELECT nipd FROM siswa WHERE nipd = '$namaFile'"));
						if($cekSiswa > 0 and $namaFileLengkap != "index.php")
						{
							$ambilSiswa = mysql_fetch_array(mysql_query("SELECT * FROM siswa WHERE nipd = '$namaFile'"));
				
							if($ambilSiswa['foto'] != "")
							{
								unlink ("../../images/siswa/$ambilSiswa[foto]");		
							}
							
							rename($target_dir . $namaFileLengkap, "../../images/siswa/$namaFileLengkap");
							$perbaruiData = mysql_query("UPDATE siswa SET foto = '$namaFileLengkap' WHERE nipd = '$namaFile'");
							
							if($perbaruiData)
							{
								$aktivitas = "Perbarui Siswa";
								$keterangan = mysql_real_escape_string("Memperbarui '$ambilSiswa[nama_lengkap]' Pada Tabel '$nama_tabel'");
								$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
							
								$success++;
							}
							else
							{
								$error++;
							}
						}
						else
						{
							$error++;
							$keterangan_error .= "<br/>Pada File $namaFileLengkap, Siswa Dengan NIPD $namaFile Tidak Ditemukan.";
							unlink($target_dir . $namaFileLengkap);
						}
					}
				}
			}
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Foto Diupload, $error Error! $keterangan_error');
				$('#form1__').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Format File Tidak Valid!');
			</script>
			";
		}
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}
?>