<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "tahun_ajar";

if($_POST['mod']=="simpanData")
{
	$tahun_ajar = mysql_real_escape_string($_POST['tahun_ajar']);
	$semester = mysql_real_escape_string($_POST['semester']);
	
	if(empty($_POST['tahun_ajar']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (tahun_ajar, semester, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$tahun_ajar', '$semester', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Tahun Ajar";
			$keterangan = mysql_real_escape_string("Menambahkan '$tahun_ajar' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$tahun_ajar = mysql_real_escape_string($_POST['tahun_ajar']);
	$semester = mysql_real_escape_string($_POST['semester']);
	
	if(empty($_POST['tahun_ajar']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET tahun_ajar = '$tahun_ajar', semester = '$semester', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Tahun Ajar";
			$keterangan = mysql_real_escape_string("Memperbarui '$tahun_ajar' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT tahun_ajar FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['tahun_ajar'];
	
	$cekTahunAjar = mysql_num_rows(mysql_query("SELECT id_tahun_ajar FROM nilai WHERE id_tahun_ajar = '$id'"));
	if($cekTahunAjar > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
			
		if($hapusData)
		{
			$aktivitas = "Hapus Tahun Ajar";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekTahunAjar = mysql_num_rows(mysql_query("SELECT id_tahun_ajar FROM nilai WHERE id_tahun_ajar IN ($data_terpilih)"));
	if($cekTahunAjar > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Tahun Ajar Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Tahun Ajar Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tahun Ajar Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}
?>