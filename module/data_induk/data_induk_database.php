<?php
session_start();
include "../../config/database.php";

$nama_menu = "data_induk";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		$('.data tfoot .filter').each(function(){
			var title = $(this).text();
			$(this).html( '<input type="text"/>' );
		});
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 1, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/data_induk/data.php",
			"aoColumns": [
				{
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "<input type='checkbox' id='" + data + "' class='terpilih' onclick='tampilTombolHapus(" + data + ")'/>";
						
						return aksi;
					}
				},
				null,
				null,
				null,
				{
					"mData": "4",
					"mRender": function(data)
					{	
						if(data == "L")
						{
							return "Laki-Laki";
						}
						else if(data == "P")
						{
							return "Perempuan";
						}
					}
				},
				null,
				null,
				null,
				{
					"bSortable": false,
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "";
						aksi += "<div class='dropdown'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown'>Action.. <span class='caret'></span></button><ul class='dropdown-menu' style='padding: 10px;'>";
						aksi += "<li style='<?=$r;?>'><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail(" + data + ")' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>Lihat Detail</button></li>";
						aksi += "</ul></div>";
						
						return aksi;
					}
				}
			]
		});
		
		table.columns().every(function(){
			var that = this;
 
			$('input', this.footer()).on('keyup change',function(){
				if (that.search() !== this.value){
					that
					.search(this.value)
					.draw();
				}
			});
		});
	});
	
	//Disable Tombol Hapus Terpilih
	$(document).ready(function(){
		$('#formatCetak').addClass("disabled").prop('disabled', true);
		$('#CetakDataTerpilih').addClass("disabled").prop('disabled', true);
	});
	
	//Select All
	$(document).ready(function(){
		$('#pilihSemua').change(function(){
			if($('#pilihSemua').is(':checked'))
			{
				$('.terpilih').prop('checked', true);
				$('#formatCetak').removeClass("disabled").prop('disabled', false);
				$('#CetakDataTerpilih').removeClass("disabled").prop('disabled', false);
			}
			else
			{
				$('.terpilih').prop('checked', false);
				$('#formatCetak').addClass("disabled").prop('disabled', true);
				$('#CetakDataTerpilih').addClass("disabled").prop('disabled', true);
			}
		});
	});
	
	//Tampil Tombol Hapus Terpilih
	var banyaknya = 0;
	
	function tampilTombolHapus(id)
	{
		var terpilih = $("#"+id+":checked").val();
		
		if(terpilih == "on")
		{
			banyaknya += 1;
		}
		else
		{
			banyaknya -= 1;
		}
		
		if(banyaknya > 0)
		{
			$('#formatCetak').removeClass("disabled").prop('disabled', false);
			$('#CetakDataTerpilih').removeClass("disabled").prop('disabled', false);
		}
		else
		{
			$('#formatCetak').addClass("disabled").prop('disabled', true);
			$('#CetakDataTerpilih').addClass("disabled").prop('disabled', true);
		}
	}
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<th style="width: 1px;">#</th>
			<th>Nama Lengkap</th>
			<th>NIPD</th>
			<th>NISN</th>
			<th>Jenis Kelamin</th>
			<th>Agama</th>
			<th>Rombel</th>
			<th>Keluar Karena</th>
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<tfoot>
		<tr>
			<th>#</th>
			<th class="filter">Nama Lengkap</th>
			<th class="filter">NIPD</th>
			<th class="filter">NISN</th>
			<th class="filter">Jenis Kelamin</th>
			<th class="filter">Agama</th>
			<th class="filter">Rombel</th>
			<th class="filter">Keluar Karena</th>
			<th>#</th>
		</tr>
	</tfoot>
	
</table>