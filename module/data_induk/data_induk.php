<?php
$nama_menu = "data_induk";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Data Induk";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/data_induk/data_induk_database.php");
		})
		
		function lihatDetail(id)
		{
			var mod = "lihatDetail";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/data_induk/data_induk_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Cetak
		function cetakData(id)
		{
			mulaiAnimasi();
			var mod = "CetakDataTerpilih";
			var format_cetak = "Semua";
			var data_terpilih = id;
			$.ajax({
				type	: "POST",
				url		: "module/data_induk/data_induk_cetak.php",
				data	: "mod=" + mod +
						  "&format_cetak=" + format_cetak +
						  "&data_terpilih=" + data_terpilih,
				success: function(data)
				{
					stopAnimasi();
					// var printPage = window.open("about:blank");
					var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
					var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
					var printPage = window.open("", "Cetak Data Induk", "left=" + width / 4 + ", top=" + height + ", width=" + width / 2 + ", height=" + height);
					printPage.document.open();
					printPage.document.write(data);
					printPage.document.close();
				}
			})
		}
		
		//Hapus Cetak Terpilih
		function CetakDataTerpilih()
		{
			mulaiAnimasi();
			var mod = "CetakDataTerpilih";
			var format_cetak = $("#formatCetak").val();
			var data_terpilih = new Array();
			$(".terpilih:checked").each(function(){
				data_terpilih.push($(this).attr("id"));
			});	
			$.ajax({
				type	: "POST",
				url		: "module/data_induk/data_induk_cetak.php",
				data	: "mod=" + mod +
						  "&format_cetak=" + format_cetak +
						  "&data_terpilih=" + data_terpilih,
				success: function(data)
				{
					stopAnimasi();
					// var printPage = window.open("about:blank");
					var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
					var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
					var printPage = window.open("", "Cetak Data Induk", "left=" + width / 4 + ", top=" + height + ", width=" + width / 2 + ", height=" + height);
					printPage.document.open();
					printPage.document.write(data);
					printPage.document.close();
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Induk</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Data Induk</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Data Induk</h3>
						</div>
						<div class="box-body">
							<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$r;?>">
								<input type="checkbox" id="pilihSemua" style="margin-right: 10px;">Pilih Semua
							</button>
							<select class="form-control" id="formatCetak" style="width: 200px; margin-right: 10px; display: inline; <?=$r;?>">
								<option value="Semua">Semua</option>
								<option value="Biodata">Biodata</option>
								<option value="Transkrip Nilai">Transkrip Nilai</option>
							</select>
							<button type="button" id="CetakDataTerpilih" class="btn btn-default" onclick="CetakDataTerpilih()" style="<?=$r;?>">
								<i class="fa fa-print" aria-hidden="true" style="margin-right: 10px;"></i>Cetak Data Terpilih</span>
							</button>
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog modal-lg">
									<div id="formContent" class="modal-content"></div>
									<div id="notifikasi" class="modal-content"></div>
								</div>
							</div>			
							<br/>
							<br/>  
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Induk</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Data Induk</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>