<?php
ob_start();
require_once "../../config/database.php";
require_once "../../libraries/fungsi_waktu.php";

$id = $_GET['id'];

$konfigurasi = mysql_query("SELECT konfigurasi.*, user.nama_lengkap, user.nip FROM konfigurasi LEFT JOIN user ON konfigurasi.id_user = user.id WHERE konfigurasi.id = '1'");
$getKonfigurasi = mysql_fetch_array($konfigurasi);

$data = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$id'");
$getData = mysql_fetch_array($data);

$namaFile = "data_induk-" . $getData['nama_lengkap'] . ".pdf";
?>

<style>
	h3 {
		margin-top: 0;
		margin-bottom: 22px;
		font-size: 12pt;
		font-wiight: bold;
		line-height: 0;
		text-transform: uppercase;
	}
	
	p {
		margin-top: 5px;
		margin-bottom: 15px;
		font-size: 10pt;
		line-height: 0;
	}
	
	h4 {
		margin-top: 0;
		line-height: 0;
	}
	
	.header-line {
		border-bottom: 5px double #000000;
	}
	
	table tr td {
		vertical-align: top;
	}
	
	.table-nilai {
		margin-top: 10px;
		border-collapse: collapse;
	}

	.table-nilai th, .table-nilai td {
		padding: 5px;
		text-align: center;
		font-size: 11px;
		vertical-align: middle;
		border: 1px solid #000000;
	}
</style>
			

<page backtop="5mm" backbottom="5mm" backleft="5mm" backright="5mm">

	<page_header>
	</page_header>

	<page_footer>
	</page_footer>
	
	<table class="table-header">
		<tr>
			<td align="left" style="width: 135px; vertical-align: middle;">
				<img src="../../images/provinsi_lampung.png" style="height: 130px;"/>
			</td>
			<td align="center" style="width: 452px;">
				<h3>PEMERINTAH PROVINSI LAMPUNG</h3>
				<h3>DINAS PENDIDIKAN DAN KEBUDAYAAN</h3>
				<h3 style="margin-bottom: 20px;"><?=$getKonfigurasi['nama_instansi'];?></h3>
				<p>Alamat : <?=$getKonfigurasi['alamat'];?>&nbsp;&nbsp;Kode Pos : <?=$getKonfigurasi['kode_pos'];?></p>
				<p>Telepon : <?=$getKonfigurasi['nomor_telepon'];?>&nbsp;&nbsp;Email : <?=$getKonfigurasi['email'];?></p>
				<p>Website : <?=$getKonfigurasi['website'];?></p>
			</td>
			<td align="right" style="width: 135px; vertical-align: middle;">
				<img src="../../images/konfigurasi/<?=$getKonfigurasi['logo'];?>" style="height: 130px;"/>
			</td>
		</tr>
	</table>
	
	<hr class="header-line"/>
	
	<br/>
	
	<h4 align="center"><b>LEMBAR BUKU INDUK SISWA</b></h4>
	
	<br/>
	<br/>
	<br/>
	
	<table>
		
		<?php
		function jarakNomor($nomor)
		{
			return ($nomor > 9 ? 10 : 15);
		}
					
		$urutanA = 1;
		$urutanB = 1;
		$urutanC = 1;
		$urutanD = 1;
		$urutanE = 1;
		$urutanF = 1;
		$urutanG = 1;
		$urutanH = 1;
		$urutanI = 1;
					
		$data = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$id'");
		$getData = mysql_fetch_array($data);

		$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");
					
		echo "
		<tr>
			<td colspan='3' style='width: 742px;'>
				<table width='400px;'>
					<tr>
						<th style='width: 220px;'>NOMOR INDUK PESERTA DIDIK</th>
						<th style='width: 10px;'>:</th>
						<td style='border-bottom: 1px dotted #333 !important;'>$getData[nipd]</td>
					</tr>
					<tr>
						<th>NOMOR INDUK SISWA NASIONAL</th>
						<th>:</th>
						<td style='border-bottom: 1px dotted #333 !important;'>$getData[nisn]</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='3'><br/></td>
		</tr>";
		
		if($getData['foto']=="")
		{
			echo "
			<tr>
				<td colspan='3' align='center'>
					<img src='../../images/user_kosong.jpg' style='width: 150px; margin: 20px;'/>
				</td>
			</tr>";
		}
		else
		{
			echo "
			<tr>
				<td colspan='3' align='center'>
					<img src='../../images/siswa/$getData[foto]' style='width: 150px; margin: 20px;'/>
				</td>
			</tr>";
		}
		
		echo "
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>A.<span style='margin-left: 10px;'>KETERANGAN DIRI SISWA</span></th>
		</tr>
					
			<tr>
				<td style='width: 215px;'><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Nama Lengkap</td>
				<td style='width: 10px;'>:</td>
				<td style='width: 497px;'>$getData[nama_lengkap]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Nama Panggilan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_panggilan]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Jenis Kelamin</td>
				<td>:</td>
				<td>".($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan")."</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Tempat Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[tempat_lahir]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Tanggal Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_lahir'])."</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Agama</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[agama]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Golongan Darah</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[golongan_darah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Berat Badan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[berat_badan] kg</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Tinggi Badan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[tinggi_badan] cm</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Lingkar Kepala</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[lingkar_kepala] cm</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Anak Ke Berapa</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[anak_ke_berapa]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Jumlah Saudara Kandung</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_kandung]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Jumlah Saudara Tiri</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_tiri]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Jumlah Saudara Angkat</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_angkat]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Kebutuhan Khusus</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;Hobi</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[hobi]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;NIK</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;No. KK</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kk]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>&nbsp;&nbsp;No. Registrasi Akta Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_registrasi_akta_lahir]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>B.<span style='margin-left: 10px;'>KETERANGAN TEMPAT TINGGAL</span></th>
		</tr>

			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Alamat Jalan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[alamat]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;RT</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[rt]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;RW</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[rw]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Dusun</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[dusun]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Kelurahan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kelurahan]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Kecamatan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kecamatan]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Kode Pos</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kode_pos]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Kewarganegaraan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kewarganegaraan]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Lintang</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[lintang]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Bujur</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[bujur]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Jarak Rumah</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jarak_rumah] km</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Tempat Tinggal</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenis_tinggal]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Moda Transportasi</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[alat_transportasi]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;No. Telepon Rumah</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_telepon]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;No. HP</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_hp]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>&nbsp;&nbsp;Email</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[email]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>C.<span style='margin-left: 10px;'>KETERANGAN AYAH KANDUNG</span></th>
		</tr>
		
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Nama</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;NIK</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Tahun Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Jenjang Pendidikan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Pekerjaan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Penghasilan Bulanan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_ayah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>&nbsp;&nbsp;Kebutuhan Khusus</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_ayah]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>D.<span style='margin-left: 10px;'>KETERANGAN IBU KANDUNG</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Nama</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;NIK</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Tahun Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Jenjang Pendidikan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Pekerjaan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Penghasilan Bulanan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_ibu]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>&nbsp;&nbsp;Kebutuhan Khusus</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_ibu]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>E.<span style='margin-left: 10px;'>KETERANGAN WALI</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Nama</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;NIK</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Tahun Lahir</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Jenjang Pendidikan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Pekerjaan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Penghasilan Bulanan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_wali]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>&nbsp;&nbsp;Kebutuhan Khusus</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_wali]</td>
			</tr>
			
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>F.<span style='margin-left: 10px;'>KETERANGAN PENDIDIKAN</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Tanggal Pendaftaran</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_pendaftaran'])."</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Jenis Pendaftaran</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenis_pendaftaran]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Sekolah Asal</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[sekolah_asal]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Sekolah Asal Pindahan</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[sekolah_asal_pindahan]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;No. Peserta Ujian Nasional</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_peserta_ujian_nasional]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;No. SKHUN</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[skhun]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;No. Seri Ijazah</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_seri_ijazah]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;NISN</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nisn]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;NIPD</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nipd]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Tanggal Masuk Sekolah</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_masuk_sekolah'])."</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>&nbsp;&nbsp;Riwayat Rombel</td>
				<td>:</td>
				<td>";
					$no = 1;
					$dataRiwayatRombel = mysql_query("SELECT * FROM riwayat_rombel_siswa WHERE id_siswa = '$id' ORDER BY id");
					while($getDataRiwayatRombel = mysql_fetch_array($dataRiwayatRombel))
					{
						echo "$no. $getDataRiwayatRombel[nama_rombel] ($getDataRiwayatRombel[keterangan]) <br/>";
						$no++;
					}
						
					echo "
				</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>G.<span style='margin-left: 10px;'>KETERANGAN KESEJAHTERAAN</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Penerima KIP</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[penerima_kip]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;No. KIP</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kip]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Penerima KPS</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[penerima_kps]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;No. KPS</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kps]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;No. KKS</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kks]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Layak PIP</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[layak_pip]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Alasan Layak PIP</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[alasan_layak_pip]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Bank</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[bank]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;No. Rekening Bank</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_rekening_bank]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>&nbsp;&nbsp;Rekening Atas Nama</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[rekening_atas_nama]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>H.<span style='margin-left: 10px;'>KETERANGAN LULUS / KELUAR</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>&nbsp;&nbsp;Keluar Karena</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[keluar_karena]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>&nbsp;&nbsp;Tanggal Keluar</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_keluar'])."</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>&nbsp;&nbsp;Alasan Keluar</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[alasan_keluar]</td>
			</tr>
		
		<tr>
			<td colspan='3'></td>
		</tr>
		<tr>
			<th colspan='3'>I.<span style='margin-left: 10px;'>KETERANGAN SETELAH LULUS</span></th>
		</tr>
					
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanI)."px;'>".$urutanI++.".</span>&nbsp;&nbsp;Melanjutkan Di</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[melanjutkan_di]</td>
			</tr>
			<tr>
				<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanI)."px;'>".$urutanI++.".</span>&nbsp;&nbsp;Bekerja Di</td>
				<td>:</td>
				<td style='border-bottom: 1px dotted #333 !important;'>$getData[bekerja_di]</td>
			</tr>";
			?>
				
	</table>
	
	<div style="width: 250px; margin-top: 100px; margin-left: 495px;">
		<p align="center"><?=str_replace("Kec.", "", $getKonfigurasi['kecamatan']);?>, <?=tanggal_indonesia($tanggal_sekarang);?></p>
		<p align="center">Kepala <?=$getKonfigurasi['nama_instansi'];?></p>
		<p align="center" style="margin-top: 80px; font-weight: bold;"><?=$getKonfigurasi['nama_lengkap'];?></p>
		<p align="center">NIP. <?=$getKonfigurasi['nip'];?></p>
	</div>

</page>

<page backtop="5mm" backbottom="5mm" backleft="5mm" backright="5mm">

	<page_header>
	</page_header>

	<page_footer>
	</page_footer>
	
	<table class="table-header">
		<tr>
			<td align="left" style="width: 135px; vertical-align: middle;">
				<img src="../../images/provinsi_lampung.png" style="height: 130px;"/>
			</td>
			<td align="center" style="width: 452px;">
				<h3>PEMERINTAH PROVINSI LAMPUNG</h3>
				<h3>DINAS PENDIDIKAN DAN KEBUDAYAAN</h3>
				<h3 style="margin-bottom: 20px;"><?=$getKonfigurasi['nama_instansi'];?></h3>
				<p>Alamat : <?=$getKonfigurasi['alamat'];?>&nbsp;&nbsp;Kode Pos : <?=$getKonfigurasi['kode_pos'];?></p>
				<p>Telepon : <?=$getKonfigurasi['nomor_telepon'];?>&nbsp;&nbsp;Email : <?=$getKonfigurasi['email'];?></p>
				<p>Website : <?=$getKonfigurasi['website'];?></p>
			</td>
			<td align="right" style="width: 135px; vertical-align: middle;">
				<img src="../../images/konfigurasi/<?=$getKonfigurasi['logo'];?>" style="height: 130px;"/>
			</td>
		</tr>
	</table>
	
	<hr class="header-line"/>
	
	<br/>
	
	<h4 align="center"><b>TRANSKRIP NILAI</b></h4>
	
	<br/>
	<br/>
	<br/>
	
	<table width="400px;">
		<tr>
			<th style="width: 220px;">NOMOR INDUK PESERTA DIDIK</th>
			<th style="width: 10px;">:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nipd'];?></td>
		</tr>
		<tr>
			<th>NOMOR INDUK SISWA NASIONAL</th>
			<th>:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nisn'];?></td>
		</tr>
		<tr>
			<th style="width: 220px;">NAMA LENGKAP</th>
			<th style="width: 10px;">:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nama_lengkap'];?></td>
		</tr>
		<tr>
			<th style="width: 220px;">JENIS KELAMIN</th>
			<th style="width: 10px;">:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");?></td>
		</tr>
		<tr>
			<th style="width: 220px;">TEMPAT LAHIR</th>
			<th style="width: 10px;">:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['tempat_lahir'];?></td>
		</tr>
		<tr>
			<th style="width: 220px;">TANGGAL LAHIR</th>
			<th style="width: 10px;">:</th>
			<td style="border-bottom: 1px dotted #333 !important;"><?=tanggal_indonesia($getData['tanggal_lahir']);?></td>
		</tr>
	</table>
	
	<br/>
	
	<?php
	$arrayTahunAjar = array();
	$arraySemester = array();
	$arrayMapel = array();
	$arrayNilaiP = array();
	$arrayNilaiK = array();
	$arrayJumlahNilaiP = array();
	$arrayJumlahNilaiK = array();
	
	$dataNilaiDetail = mysql_query("SELECT tahun_ajar.tahun_ajar, tahun_ajar.semester, nilai_detail.kelompok_mapel, nilai_detail.kode_mapel, nilai_detail.nama_mapel, nilai_detail.kkm, nilai_detail.nilai_p,
	nilai_detail.nilai_k, nilai_detail.urutan_mapel
	FROM nilai_detail
	LEFT JOIN nilai ON nilai_detail.id_nilai = nilai.id
	LEFT JOIN tahun_ajar ON nilai.id_tahun_ajar = tahun_ajar.id
	WHERE nilai.id_siswa = $id
	ORDER BY tahun_ajar.tahun_ajar ASC, tahun_ajar.semester ASC, nilai_detail.urutan_mapel");
	while($ambilDataNilaiDetail = mysql_fetch_assoc($dataNilaiDetail))
	{
		$arrayTahunAjar[] = $ambilDataNilaiDetail['tahun_ajar'];
		$arraySemuaSemester[] = $ambilDataNilaiDetail['tahun_ajar'] . "/" . $ambilDataNilaiDetail['semester'];
		$arraySemester[$ambilDataNilaiDetail['tahun_ajar']][] = $ambilDataNilaiDetail['semester'];
		$arrayMapel[] = $ambilDataNilaiDetail['nama_mapel'];
		$arrayNilaiP[$ambilDataNilaiDetail['nama_mapel']][$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_p'];
		$arrayNilaiK[$ambilDataNilaiDetail['nama_mapel']][$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_k'];
		$arrayJumlahNilaiP[$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_p'];
		$arrayJumlahNilaiK[$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_k'];
	}
	
	$arrayTahunAjarUnique = array_unique($arrayTahunAjar);
	$arraySemuaSemesterUnique = array_unique($arraySemuaSemester);
	$arrayMapelUnique = array_unique($arrayMapel);
	?>
	
	<table class="table-nilai">
		<tr>
			<th rowspan="4">No.</th>
			<th rowspan="4" style="width: 135px;">Mata Pelajaran</th>
			<th colspan="<?=count($arraySemuaSemesterUnique) * 2;?>">Nilai</th>
			<th rowspan="4">Jumlah<br/>Permapel</th>
			<th rowspan="4">Rata-Rata<br/>Permapel</th>
		</tr>
		<tr>
			<?php
			$i = 1;
			foreach($arrayTahunAjarUnique as $listTahunAjar)
			{
				$leftBorder = ($i == 1 ? "style='border-left: none;'" : "");
				$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
				$jumlahSemester = count($arraySemesterUnique) * 2;
				echo "<th colspan='$jumlahSemester' $leftBorder>$listTahunAjar</th>";
				$i++;
			}
			?>
		</tr>
		<tr>
			<?php
			$i = 1;
			foreach($arrayTahunAjarUnique as $listTahunAjar)
			{
				$leftBorder = ($i == 1 ? "style='border-left: none;'" : "");
				$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
				foreach($arraySemesterUnique as $listSemester)
				{
					echo "<th colspan='2' $leftBorder>$listSemester</th>";
				}
				$i++;
			}
			?>
		</tr>
		<tr>
			<?php
			$i = 1;
			foreach($arrayTahunAjarUnique as $listTahunAjar)
			{
				$leftBorder = ($i == 1 ? "style='border-left: none;'" : "");
				$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
				foreach($arraySemesterUnique as $listSemester)
				{
					echo "
					<th $leftBorder>P</th>
					<th>K</th>";
				}
				$i++;
			}
			?>
		</tr>
			
		<?php
		$no = 1;
		foreach($arrayMapelUnique as $listMapel)
		{
		?>

			<tr>
				<th><?=$no;?></th>
				<th style="text-align: left !important;"><?=$listMapel;?></th>
				<?php
				$jumlahNilaiPermapel = 0;
				$jumlahDataPermapel = 0;
				
				foreach($arrayTahunAjarUnique as $listTahunAjar)
				{
					$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
					foreach($arraySemesterUnique as $listSemester)
					{
					$nilai_p = (isset($arrayNilaiP[$listMapel][$listTahunAjar]) ? $arrayNilaiP[$listMapel][$listTahunAjar][$listSemester][0] : "");
					$nilai_k = (isset($arrayNilaiK[$listMapel][$listTahunAjar]) ? $arrayNilaiK[$listMapel][$listTahunAjar][$listSemester][0] : "");
					echo "
					<td>$nilai_p</td>
					<td>$nilai_k</td>";
					
					if(isset($arrayNilaiP[$listMapel][$listTahunAjar]))
					{
						$jumlahNilaiPermapel += $arrayNilaiP[$listMapel][$listTahunAjar][$listSemester][0];
						$jumlahDataPermapel++;
					}
					
					if(isset($arrayNilaiK[$listMapel][$listTahunAjar]))
					{
						$jumlahNilaiPermapel += $arrayNilaiK[$listMapel][$listTahunAjar][$listSemester][0];
						$jumlahDataPermapel++;
					}
					}
				}
				?>
				<th><?=$jumlahNilaiPermapel;?></th>
				<th><?=round($jumlahNilaiPermapel / $jumlahDataPermapel);?></th>
			</tr>
			
		<?php
			$no++;
		}
		?>
			
			
		<tr>
			<th colspan="2">Jumlah</th>
			<?php
			foreach($arrayTahunAjarUnique as $listTahunAjar)
			{
				$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
				foreach($arraySemesterUnique as $listSemester)
				{
					$jumlahNilaiPPertahunAjar = array_sum($arrayJumlahNilaiP[$listTahunAjar][$listSemester]);
					$jumlahNilaiKPertahunAjar = array_sum($arrayJumlahNilaiK[$listTahunAjar][$listSemester]);
					echo "
					<th>$jumlahNilaiPPertahunAjar</th>
					<th>$jumlahNilaiKPertahunAjar</th>";
				}
			}
			?>
		</tr>
		<tr>
			<th colspan="2">Rata-Rata</th>
			<?php
			foreach($arrayTahunAjarUnique as $listTahunAjar)
			{
				$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
				foreach($arraySemesterUnique as $listSemester)
				{
					$rataRataNilaiPPertahunAjar = round(array_sum($arrayJumlahNilaiP[$listTahunAjar][$listSemester]) / count($arrayJumlahNilaiP[$listTahunAjar][$listSemester]));
					$rataRataNilaiKPertahunAjar = round(array_sum($arrayJumlahNilaiK[$listTahunAjar][$listSemester]) / count($arrayJumlahNilaiK[$listTahunAjar][$listSemester]));
					echo "
					<th>$rataRataNilaiPPertahunAjar</th>
					<th>$rataRataNilaiKPertahunAjar</th>";
				}
			}
			?>
		</tr>
	</table>
	
	<div style="width: 250px; margin-top: 100px; margin-left: 495px;">
		<p align="center"><?=str_replace("Kec.", "", $getKonfigurasi['kecamatan']);?>, <?=tanggal_indonesia($tanggal_sekarang);?></p>
		<p align="center">Kepala <?=$getKonfigurasi['nama_instansi'];?></p>
		<p align="center" style="margin-top: 80px; font-weight: bold;"><?=$getKonfigurasi['nama_lengkap'];?></p>
		<p align="center">NIP. <?=$getKonfigurasi['nip'];?></p>
	</div>
	
</page>

<?php
$content = ob_get_clean();
require_once(dirname(__FILE__).'/../../libraries/html2pdf/html2pdf.class.php');
try
{
	$html2pdf = new HTML2PDF('P','LEGAL','en', false, 'ISO-8859-15',array(0, 0, 0, 0));
	$html2pdf->setDefaultFont('Arial');
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
	$html2pdf->Output($namaFile);
}
catch(HTML2PDF_exception $e)
{
	echo $e;
}
?>