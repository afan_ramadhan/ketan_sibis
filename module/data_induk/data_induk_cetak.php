<?php
error_reporting(0);
ob_start();
require_once "../../config/database.php";
require_once "../../libraries/fungsi_waktu.php";

$format_cetak = $_POST['format_cetak'];
$data_terpilih = $_POST['data_terpilih'];

$konfigurasi = mysql_query("SELECT konfigurasi.*, user.nama_lengkap, user.nip FROM konfigurasi LEFT JOIN user ON konfigurasi.id_user = user.id WHERE konfigurasi.id = '1'");
$getKonfigurasi = mysql_fetch_array($konfigurasi);

function jarakNomor($nomor)
{
	return ($nomor > 9 ? 10 : 15);
}

if($format_cetak == "Semua")
{
	$showBiodata = "";
	$showTranskripNilai = "";
}
else if($format_cetak == "Biodata")
{
	$showBiodata = "";
	$showTranskripNilai = "style='display: none;'";
}
else if($format_cetak == "Transkrip Nilai")
{
	$showBiodata = "style='display: none;'";
	$showTranskripNilai = "";
}
?>

<!DOCTYPE html>

<html>
	
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?=$getKonfigurasi['nama_aplikasi'];?> <?=$getKonfigurasi['versi'];?> Manajemen | Data Induk</title>
		
		<link rel="icon" type="image/png" href="images/ketanware_2.png">
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
		
		<style>
			.table-header tr td {
				border-top: none !important;
				border-bottom: 6px double #333333 !important;
			}
			
			.table-header tr td h3 {
				font-weight: bold;
				line-height: 8px;
				text-transform: uppercase;
			}
			
			.table-header tr td p {
				line-height: 8px;
			}
			
			.no-border tr th, .no-border tr td {
				border: none !important;
			}
			
			.table-nilai tr th, .table-nilai tr td {
				padding: 5px !important;
				text-align: center;
				font-size: 12px;
				vertical-align: middle !important;
				border: 2px solid #333333 !important;
			}
		</style>
	
	</head>

	<body>
		
		<div id="print-area" style="width: 100%; padding: 20px;">
		
			<?php
			$data = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id IN ($data_terpilih)");
			while($getData = mysql_fetch_array($data))
			{
				$id = $getData['id'];
			?>
				
				<div <?=$showBiodata;?>>
			
					<table class="table table-header">
						<tr>
							<td align="left" style="width: 150px; vertical-align: middle;">
								<img src="images/provinsi_lampung.png" style="max-width: 150px; height: 130px;"/>
							</td>
							<td align="center">
								<h3>PEMERINTAH PROVINSI LAMPUNG</h3>
								<h3>DINAS PENDIDIKAN DAN KEBUDAYAAN</h3>
								<h3 style="margin-bottom: 20px;"><?=$getKonfigurasi['nama_instansi'];?></h3>
								<p>Alamat : <?=$getKonfigurasi['alamat'];?>&nbsp;&nbsp;Kode Pos : <?=$getKonfigurasi['kode_pos'];?></p>
								<p>Telepon : <?=$getKonfigurasi['nomor_telepon'];?>&nbsp;&nbsp;Email : <?=$getKonfigurasi['email'];?></p>
								<p>Website : <?=$getKonfigurasi['website'];?></p>
							</td>
							<td align="right" style="width: 150px; vertical-align: middle;">
								<img src="images/konfigurasi/<?=$getKonfigurasi['logo'];?>" style="max-width: 150px; height: 130px;"/>
							</td>
						</tr>
					</table>
					<table class="table no-border">
						<tbody>
						
							<?php
							$urutanA = 1;
							$urutanB = 1;
							$urutanC = 1;
							$urutanD = 1;
							$urutanE = 1;
							$urutanF = 1;
							$urutanG = 1;
							$urutanH = 1;
							$urutanI = 1;
								
							$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");
								
							echo "
							<tr>
								<th colspan='3'>
									<h4 align='center'><b>LEMBAR BUKU INDUK SISWA</b></h4>
									<br/>
									<center>
										<table width='400px;'>
											<tr>
												<th style='width: 220px;'>NOMOR INDUK PESERTA DIDIK</th>
												<th style='width: 10px;'>:</th>
												<td style='border-bottom: 1px dotted #333 !important;'>$getData[nipd]</td>
											</tr>
											<tr>
												<th>NOMOR INDUK SISWA NASIONAL</th>
												<th>:</th>
												<td style='border-bottom: 1px dotted #333 !important;'>$getData[nisn]</td>
											</tr>
										</table>
									</center>
								</th>
							</tr>";
								
							if($getData['foto']=="")
							{
								echo "
								<tr>
									<td colspan='3'>
										<center><img class='img-thumbnail' src='images/user_kosong.jpg' style='width: 250px; margin: 20px;'/></center>
									</td>
								</tr>";
							}
							else
							{
								echo "
								<tr>
									<td colspan='3'>
										<center><img class='img-thumbnail' src='images/siswa/$getData[foto]' style='width: 250px; margin: 20px;'/></center>
									</td>
								</tr>";
							}
								
							echo "
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>A.</span>KETERANGAN DIRI SISWA</th>
							</tr>
								
							<tr>
								<td style='width: 300px;'><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Nama Lengkap</td>
								<td style='width: 10px;'>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_lengkap]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Nama Panggilan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_panggilan]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Jenis Kelamin</td>
								<td>:</td>
								<td>".($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan")."</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Tempat Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[tempat_lahir]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Tanggal Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_lahir'])."</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Agama</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[agama]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Golongan Darah</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[golongan_darah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Berat Badan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[berat_badan] kg</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Tinggi Badan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[tinggi_badan] cm</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Lingkar Kepala</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[lingkar_kepala] cm</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Anak Ke Berapa</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[anak_ke_berapa]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Jumlah Saudara Kandung</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_kandung]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Jumlah Saudara Tiri</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_tiri]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Jumlah Saudara Angkat</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jumlah_saudara_angkat]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Kebutuhan Khusus</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>Hobi</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[hobi]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>NIK</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>No. KK</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kk]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanA)."px;'>".$urutanA++.".</span>No. Registrasi Akta Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_registrasi_akta_lahir]</td>
							</tr>
								
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>B.</span>KETERANGAN TEMPAT TINGGAL</th>
							</tr>
								
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Alamat Jalan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[alamat]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>RT</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[rt]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>RW</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[rw]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Dusun</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[dusun]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Kelurahan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kelurahan]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Kecamatan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kecamatan]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Kode Pos</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kode_pos]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Kewarganegaraan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kewarganegaraan]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Lintang</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[lintang]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Bujur</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[bujur]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Jarak Rumah</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jarak_rumah] km</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Tempat Tinggal</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenis_tinggal]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Moda Transportasi</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[alat_transportasi]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>No. Telepon Rumah</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_telepon]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>No. HP</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_hp]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanB)."px;'>".$urutanB++.".</span>Email</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[email]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>C.</span>KETERANGAN AYAH KANDUNG</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Nama</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>NIK</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Tahun Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Jenjang Pendidikan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Pekerjaan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Penghasilan Bulanan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_ayah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanC)."px;'>".$urutanC++.".</span>Kebutuhan Khusus</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_ayah]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>D.</span>KETERANGAN IBU KANDUNG</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Nama</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>NIK</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Tahun Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Jenjang Pendidikan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Pekerjaan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Penghasilan Bulanan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_ibu]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanD)."px;'>".$urutanD++.".</span>Kebutuhan Khusus</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_ibu]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>E.</span>KETERANGAN WALI</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Nama</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nama_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>NIK</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nik_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Tahun Lahir</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[tahun_lahir_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Jenjang Pendidikan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenjang_pendidikan_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Pekerjaan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[pekerjaan_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Penghasilan Bulanan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[penghasilan_wali]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanE)."px;'>".$urutanE++.".</span>Kebutuhan Khusus</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[kebutuhan_khusus_wali]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>F.</span>KETERANGAN PENDIDIKAN</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Tanggal Pendaftaran</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_pendaftaran'])."</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Jenis Pendaftaran</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[jenis_pendaftaran]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Sekolah Asal</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[sekolah_asal]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Sekolah Asal Pindahan</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[sekolah_asal_pindahan]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>No. Peserta Ujian Nasional</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_peserta_ujian_nasional]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>No. SKHUN</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[skhun]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>No. Seri Ijazah</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_seri_ijazah]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>NISN</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nisn]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>NIPD</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nipd]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Tanggal Masuk Sekolah</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_masuk_sekolah'])."</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanF)."px;'>".$urutanF++.".</span>Riwayat Rombel</td>
								<td>:</td>
								<td>
									<ol style='padding-left: 15px;'>";
								
									$dataRiwayatRombel = mysql_query("SELECT * FROM riwayat_rombel_siswa WHERE id_siswa = '$id' ORDER BY id");
									while($getDataRiwayatRombel = mysql_fetch_array($dataRiwayatRombel))
									{
										echo "<li>$getDataRiwayatRombel[nama_rombel] ($getDataRiwayatRombel[keterangan])</li>";
									}
									
									echo "
									</ol>
								</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>G.</span>KETERANGAN KESEJAHTERAAN</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Penerima KIP</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[penerima_kip]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>No. KIP</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kip]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Penerima KPS</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[penerima_kps]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>No. KPS</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kps]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>No. KKS</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_kks]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Layak PIP</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[layak_pip]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Alasan Layak PIP</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[alasan_layak_pip]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Bank</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[bank]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>No. Rekening Bank</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[nomor_rekening_bank]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanG)."px;'>".$urutanG++.".</span>Rekening Atas Nama</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[rekening_atas_nama]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>H.</span>KETERANGAN LULUS / KELUAR</th>
							</tr>
							
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>Keluar Karena</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[keluar_karena]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>Tanggal Keluar</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>".tanggal_indonesia($getData['tanggal_keluar'])."</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanH)."px;'>".$urutanH++.".</span>Alasan Keluar</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[alasan_keluar]</td>
							</tr>
							
							<tr>
								<th colspan='3'><span style='margin-right: 10px;'>I.</span>KETERANGAN SETELAH LULUS</th>
							</tr>
										
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanI)."px;'>".$urutanI++.".</span>Melanjutkan Di</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[melanjutkan_di]</td>
							</tr>
							<tr>
								<td><span style='margin-left: 20px; margin-right: ".jarakNomor($urutanI)."px;'>".$urutanI++.".</span>Bekerja Di</td>
								<td>:</td>
								<td style='border-bottom: 1px dotted #333 !important;'>$getData[bekerja_di]</td>
							</tr>";
							?>
							
						</tbody>
					</table>
						
					<div style="width: 300px; margin-top: 100px; margin-bottom: 40px; float: right;">
						<p align="center"><?=str_replace("Kec.", "", $getKonfigurasi['kecamatan']);?>, <?=tanggal_indonesia($tanggal_sekarang);?></p>
						<p align="center">Kepala <?=$getKonfigurasi['nama_instansi'];?></p>
						<p align="center" style="margin-top: 80px; font-weight: bold;"><?=$getKonfigurasi['nama_lengkap'];?></p>
						<p align="center">NIP. <?=$getKonfigurasi['nip'];?></p>
					</div>
						
					<div style="clear: right; page-break-after: always;"></div>
				
				</div>
				
				<div <?=$showTranskripNilai;?>>
				
					<table class="table table-header">
						<tr>
							<td align="left" style="width: 150px; vertical-align: middle;">
								<img src="images/provinsi_lampung.png" style="max-width: 150px; height: 130px;"/>
							</td>
							<td align="center">
								<h3>PEMERINTAH PROVINSI LAMPUNG</h3>
								<h3>DINAS PENDIDIKAN DAN KEBUDAYAAN</h3>
								<h3 style="margin-bottom: 20px;"><?=$getKonfigurasi['nama_instansi'];?></h3>
								<p>Alamat : <?=$getKonfigurasi['alamat'];?>&nbsp;&nbsp;Kode Pos : <?=$getKonfigurasi['kode_pos'];?></p>
								<p>Telepon : <?=$getKonfigurasi['nomor_telepon'];?>&nbsp;&nbsp;Email : <?=$getKonfigurasi['email'];?></p>
								<p>Website : <?=$getKonfigurasi['website'];?></p>
							</td>
							<td align="right" style="width: 150px; vertical-align: middle;">
								<img src="images/konfigurasi/<?=$getKonfigurasi['logo'];?>" style="max-width: 150px; height: 130px;"/>
							</td>
						</tr>
					</table>
					<table class="table no-border">
						<tbody>
							<tr>
								<th colspan="3">
									<h4 align="center"><b>TRANSKRIP NILAI</b></h4>
								</th>
							</tr>
							<tr>
								<th style="width: 250px;">NOMOR INDUK PESERTA DIDIK</th>
								<th style="width: 10px;">:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nipd'];?></td>
							</tr>
							<tr>
								<th>NOMOR INDUK SISWA NASIONAL</th>
								<th>:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nisn'];?></td>
							</tr>
							<tr>
								<th style="width: 220px;">NAMA LENGKAP</th>
								<th style="width: 10px;">:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['nama_lengkap'];?></td>
							</tr>
							<tr>
								<th style="width: 220px;">JENIS KELAMIN</th>
								<th style="width: 10px;">:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=($getData['jenis_kelamin'] == "L" ? "Laki-Laki" : "Perempuan");?></td>
							</tr>
							<tr>
								<th style="width: 220px;">TEMPAT LAHIR</th>
								<th style="width: 10px;">:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=$getData['tempat_lahir'];?></td>
							</tr>
							<tr>
								<th style="width: 220px;">TANGGAL LAHIR</th>
								<th style="width: 10px;">:</th>
								<td style="border-bottom: 1px dotted #333 !important;"><?=tanggal_indonesia($getData['tanggal_lahir']);?></td>
							</tr>
						</tbody>
					</table>
					
					<br/>
						
					<?php
					$arrayTahunAjar = array();
					$arraySemester = array();
					$arrayMapel = array();
					$arrayNilaiP = array();
					$arrayNilaiK = array();
					$arrayJumlahNilaiP = array();
					$arrayJumlahNilaiK = array();
					
					$dataNilaiDetail = mysql_query("SELECT tahun_ajar.tahun_ajar, tahun_ajar.semester, nilai_detail.kelompok_mapel, nilai_detail.kode_mapel, nilai_detail.nama_mapel, nilai_detail.kkm, nilai_detail.nilai_p,
					nilai_detail.nilai_k, nilai_detail.urutan_mapel
					FROM nilai_detail
					LEFT JOIN nilai ON nilai_detail.id_nilai = nilai.id
					LEFT JOIN tahun_ajar ON nilai.id_tahun_ajar = tahun_ajar.id
					WHERE nilai.id_siswa = $id
					ORDER BY tahun_ajar.tahun_ajar ASC, tahun_ajar.semester ASC, nilai_detail.urutan_mapel");
					while($ambilDataNilaiDetail = mysql_fetch_assoc($dataNilaiDetail))
					{
						$arrayTahunAjar[] = $ambilDataNilaiDetail['tahun_ajar'];
						$arraySemuaSemester[] = $ambilDataNilaiDetail['tahun_ajar'] . "/" . $ambilDataNilaiDetail['semester'];
						$arraySemester[$ambilDataNilaiDetail['tahun_ajar']][] = $ambilDataNilaiDetail['semester'];
						$arrayMapel[] = $ambilDataNilaiDetail['nama_mapel'];
						$arrayNilaiP[$ambilDataNilaiDetail['nama_mapel']][$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_p'];
						$arrayNilaiK[$ambilDataNilaiDetail['nama_mapel']][$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_k'];
						$arrayJumlahNilaiP[$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_p'];
						$arrayJumlahNilaiK[$ambilDataNilaiDetail['tahun_ajar']][$ambilDataNilaiDetail['semester']][] = $ambilDataNilaiDetail['nilai_k'];
					}
						
					$arrayTahunAjarUnique = array_unique($arrayTahunAjar);
					$arraySemuaSemesterUnique = array_unique($arraySemuaSemester);
					$arrayMapelUnique = array_unique($arrayMapel);
					?>
						
					<table class="table table-nilai">
						<tr>
							<th rowspan="4">No.</th>
							<th rowspan="4">Mata Pelajaran</th>
							<th colspan="<?=count($arraySemuaSemesterUnique) * 2;?>">Nilai</th>
							<th rowspan="4">Jumlah Permapel</th>
							<th rowspan="4">Rata-Rata Permapel</th>
						</tr>
						<tr>
							<?php
							foreach($arrayTahunAjarUnique as $listTahunAjar)
							{
								$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
								$jumlahSemester = count($arraySemesterUnique) * 2;
								echo "<th colspan='$jumlahSemester'>$listTahunAjar</th>";
							}
							?>
						</tr>
						<tr>
							<?php
							foreach($arrayTahunAjarUnique as $listTahunAjar)
							{
								$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
								foreach($arraySemesterUnique as $listSemester)
								{
									echo "<th colspan='2'>$listSemester</th>";
								}
							}
							?>
						</tr>
						<tr>			
							<?php
							foreach($arrayTahunAjarUnique as $listTahunAjar)
							{
								$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
								foreach($arraySemesterUnique as $listSemester)
								{
									echo "
									<th>P</th>
									<th>K</th>";
								}
							}
							?>
						</tr>
							
						<?php
						$no = 1;
						foreach($arrayMapelUnique as $listMapel)
						{
						?>

							<tr>
								<th><?=$no;?></th>
								<th style="text-align: left !important;"><?=$listMapel;?></th>
								<?php
								$jumlahNilaiPermapel = 0;
								$jumlahDataPermapel = 0;
								
								foreach($arrayTahunAjarUnique as $listTahunAjar)
								{
									$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
									foreach($arraySemesterUnique as $listSemester)
									{
										$nilai_p = (isset($arrayNilaiP[$listMapel][$listTahunAjar]) ? $arrayNilaiP[$listMapel][$listTahunAjar][$listSemester][0] : "");
										$nilai_k = (isset($arrayNilaiK[$listMapel][$listTahunAjar]) ? $arrayNilaiK[$listMapel][$listTahunAjar][$listSemester][0] : "");
										echo "
										<td>$nilai_p</td>
										<td>$nilai_k</td>";
											
										if(isset($arrayNilaiP[$listMapel][$listTahunAjar]))
										{
											$jumlahNilaiPermapel += $arrayNilaiP[$listMapel][$listTahunAjar][$listSemester][0];
											$jumlahDataPermapel++;
										}
											
										if(isset($arrayNilaiK[$listMapel][$listTahunAjar]))
										{
											$jumlahNilaiPermapel += $arrayNilaiK[$listMapel][$listTahunAjar][$listSemester][0];
											$jumlahDataPermapel++;
										}
									}
								}
								?>
								<th><?=$jumlahNilaiPermapel;?></th>
								<th><?=round($jumlahNilaiPermapel / $jumlahDataPermapel);?></th>
							</tr>
							
						<?php
							$no++;
						}
						?>
											
						<tr>
							<th colspan="2">Jumlah</th>
							<?php
							foreach($arrayTahunAjarUnique as $listTahunAjar)
							{
								$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
								foreach($arraySemesterUnique as $listSemester)
								{
									$jumlahNilaiPPertahunAjar = array_sum($arrayJumlahNilaiP[$listTahunAjar][$listSemester]);
									$jumlahNilaiKPertahunAjar = array_sum($arrayJumlahNilaiK[$listTahunAjar][$listSemester]);
									echo "
									<th>$jumlahNilaiPPertahunAjar</th>
									<th>$jumlahNilaiKPertahunAjar</th>";
								}
							}
							?>
						</tr>
						<tr>
							<th colspan="2">Rata-Rata</th>
							<?php
							foreach($arrayTahunAjarUnique as $listTahunAjar)
							{
								$arraySemesterUnique = array_unique($arraySemester[$listTahunAjar]);
								foreach($arraySemesterUnique as $listSemester)
								{
									$rataRataNilaiPPertahunAjar = round(array_sum($arrayJumlahNilaiP[$listTahunAjar][$listSemester]) / count($arrayJumlahNilaiP[$listTahunAjar][$listSemester]));
									$rataRataNilaiKPertahunAjar = round(array_sum($arrayJumlahNilaiK[$listTahunAjar][$listSemester]) / count($arrayJumlahNilaiK[$listTahunAjar][$listSemester]));
									echo "
									<th>$rataRataNilaiPPertahunAjar</th>
									<th>$rataRataNilaiKPertahunAjar</th>";
								}
							}
							?>
						</tr>
					</table>
						
					<div style="width: 300px; margin-top: 50px; margin-bottom: 20px; float: right;">
						<p align="center"><?=str_replace("Kec.", "", $getKonfigurasi['kecamatan']);?>, <?=tanggal_indonesia($tanggal_sekarang);?></p>
						<p align="center">Kepala <?=$getKonfigurasi['nama_instansi'];?></p>
						<p align="center" style="margin-top: 80px; font-weight: bold;"><?=$getKonfigurasi['nama_lengkap'];?></p>
						<p align="center">NIP. <?=$getKonfigurasi['nip'];?></p>
					</div>
						
					<div style="clear: right; page-break-after: always;"></div>
				
				</div>
				
			<?php
			}
			?>
			
		</div>
		
		<script src="assets/plugins/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
		<script>
			$(function(){
				var printContents = document.getElementById('print-area').innerHTML;
				var originalContents = document.body.innerHTML;
				document.body.innerHTML = printContents;
				window.print();
				document.body.innerHTML = originalContents;
			});
			
			window.addEventListener('afterprint', (event) => {
				setTimeout(function(){
					window.close();
				}, 1000);
			});
		</script>
	
	</body>
	
</html>