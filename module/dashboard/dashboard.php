<?php
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]'");
while($getHakAkses = mysql_fetch_array($hakAkses))
{
	$dashboardWidget[$getHakAkses['nama_menu']]['r'] = $getHakAkses['r'];
	$dashboardWidget[$getHakAkses['nama_menu']]['s'] = $getHakAkses['s'];
}

$jumlahJurusan = mysql_num_rows(mysql_query("SELECT * FROM jurusan"));
$jumlahTahunAjar = mysql_num_rows(mysql_query("SELECT * FROM tahun_ajar"));
$jumlahKurikulum = mysql_num_rows(mysql_query("SELECT * FROM kurikulum"));
$jumlahRombel = mysql_num_rows(mysql_query("SELECT * FROM rombel"));
$jumlahSiswa = mysql_num_rows(mysql_query("SELECT * FROM siswa"));
$jumlahSiswaAktif = mysql_num_rows(mysql_query("SELECT * FROM siswa WHERE keluar_karena = ''"));
$jumlahSiswaLulus = mysql_num_rows(mysql_query("SELECT * FROM siswa WHERE keluar_karena = 'Lulus'"));
$jumlahSiswaKeluar = mysql_num_rows(mysql_query("SELECT * FROM siswa WHERE keluar_karena NOT IN ('', 'Lulus')"));
$jumlahNilai = mysql_num_rows(mysql_query("SELECT * FROM nilai"));
$jumlahLevel = mysql_num_rows(mysql_query("SELECT * FROM level"));
$jumlahUser = mysql_num_rows(mysql_query("SELECT * FROM user"));

function background($skin)
{
	if($skin == "skin-blue" or $skin == "skin-blue-light")
	{
		return "bg-aqua";
	}
	else if($skin == "skin-black" or $skin == "skin-black-light")
	{
		return "bg-gray";
	}
	else if($skin == "skin-purple" or $skin == "skin-purple-light")
	{
		return "bg-fuchsia";
	}
	else if($skin == "skin-green" or $skin == "skin-green-light")
	{
		return "bg-lime";
	}
	else if($skin == "skin-red" or $skin == "skin-red-light")
	{
		return "bg-maroon";
	}
	else if($skin == "skin-yellow" or $skin == "skin-yellow-light")
	{
		return "bg-red";
	}
}
?>

<script language="JavaScript">
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Dashboard";
	}
	GantiTitle();
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Dashboard</h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Total Data</h3>
					</div>
					<div class="box-body">
						
						<!--
						
						<h3>Selamat Datang Di Aplikasi K - SIBIS (KetanWare - Sistem Informasi Buku Induk Siswa)</h3>
						
						<p>Halaman berikut ini akan menampilkan laporan progres pengembangan aplikasi.</p>
						<p>Keterangan :  <i class="fa fa-check" aria-hidden="true"></i> (Fitur yang sudah berjalan)</p>
						
						<?php
						$totalModul = 12;
						$modulSelesai = 12;
						$persentaseSelesai = round(($modulSelesai / $totalModul) * 100);
						?>
						
						
						<div class="progress">
							<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$persentaseSelesai;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$persentaseSelesai;?>%">
								<?=$persentaseSelesai;?> % Progres Pengembangan K - SIBIS
							</div>
						</div>
						
						<ol>
							<li>Dashboard <i class="fa fa-check" aria-hidden="true"></i></li>
							<li>
								Master
								<br/>
								<ul>
									<li>
										Jurusan <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi data jurusan yang ada pada instansi pendidikan.
									</li>
									<li>
										Tahun Ajar <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi data tahun ajar yang ada pada instansi pendidikan.
									</li>
									<li>
										Kurikulum <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi data kurikulum yang ada pada instansi pendidikan.
									</li>
									<li>
										Rombel <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi data rombel yang ada pada instansi pendidikan.
									</li>
									<li>
										Siswa <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi data rombel yang ada pada instansi pendidikan.
									</li>
								</ul>
							</li>
							<li>
								Nilai Siswa <i class="fa fa-check" aria-hidden="true"></i>
								<br/>
								Pada menu ini berisi data penilaian siswa yang ada pada instansi pendidikan.
							</li>
							<li>
								Data Induk <i class="fa fa-check" aria-hidden="true"></i>
								<br/>
								Pada menu ini berisi seluruh data induk siswa yang ada pada instansi pendidikan.
							</li>
							<li>
								Manajemen Akses
								<br/>
								<ul>
									<li>
										Level <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi level untuk mengatur hak akses aplikasi K - SIBIS.
									</li>
									<li>
										User <i class="fa fa-check" aria-hidden="true"></i>
										<br/>
										Pada menu ini berisi user atau pengguna aplikasi K - SIBIS.
									</li>
								</ul>
							</li>
							<li>
								Riwayat Aktivitas <i class="fa fa-check" aria-hidden="true"></i>
								<br/>
								Pada menu ini berisi semua riwayat aktivitas user pada aplikasi K - SIBIS.
							</li>
							<li>
								Konfigurasi <i class="fa fa-check" aria-hidden="true"></i>
								<br/>
								Pada menu ini berisi konfigurasi dasar aplikasi K - SIBIS.
							</li>
						</ol>
						
						<hr/>
						
						-->
						
						<div class="row">
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['jurusan']['r']) or $dashboardWidget['jurusan']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahJurusan;?></h3>
										<p>Total Jurusan</p>
									</div>
									<div class="icon">
										<i class="fa fa-diamond"></i>
									</div>
									<a href="?mod=jurusan" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['tahun_ajar']['r']) or $dashboardWidget['tahun_ajar']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahTahunAjar;?></h3>
										<p>Total Tahun Ajar</p>
									</div>
									<div class="icon">
										<i class="fa fa-calendar-o"></i>
									</div>
									<a href="?mod=tahun_ajar" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['kurikulum']['r']) or $dashboardWidget['kurikulum']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahKurikulum;?></h3>
										<p>Total Kurikulum</p>
									</div>
									<div class="icon">
										<i class="fa fa-book"></i>
									</div>
									<a href="?mod=kurikulum" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['rombel']['r']) or $dashboardWidget['rombel']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahRombel;?></h3>
										<p>Total Rombel</p>
									</div>
									<div class="icon">
										<i class="fa fa-suitcase"></i>
									</div>
									<a href="?mod=rombel" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['siswa']['r']) or $dashboardWidget['siswa']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahSiswa;?></h3>
										<p>Total Siswa</p>
									</div>
									<div class="icon">
										<i class="fa fa-users"></i>
									</div>
									<a href="?mod=siswa" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['nilai']['r']) or $dashboardWidget['nilai']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahNilai;?></h3>
										<p>Total Nilai</p>
									</div>
									<div class="icon">
										<i class="fa fa-percent"></i>
									</div>
									<a href="?mod=nilai" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
						
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['level']['r']) or $dashboardWidget['level']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahLevel;?></h3>
										<p>Total Level</p>
									</div>
									<div class="icon">
										<i class="fa fa-id-card-o"></i>
									</div>
									<a href="?mod=level" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?php if(!isset($dashboardWidget['user']['r']) or $dashboardWidget['user']['r'] != 1){echo "display: none;";} ?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahUser;?></h3>
										<p>Total User</p>
									</div>
									<div class="icon">
										<i class="fa fa-group"></i>
									</div>
									<a href="?mod=user" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-12 col-xs-12" style="<?php if(!isset($dashboardWidget['siswa']['r']) or $dashboardWidget['siswa']['r'] != 1){echo "display: none;";} ?>">
								<div id="statistik_siswa"></div>
								<script>
									Highcharts.chart('statistik_siswa', {
										chart: {
											plotBackgroundColor: null,
											plotBorderWidth: null,
											plotShadow: false,
											type: 'pie'
										},
										title: {
											text: "Statistik Siswa"
										},
										tooltip: {
											pointFormat: '{series.name}: <b>{point.y}</b>'
										},
										accessibility: {
											point: {
												valueSuffix: ''
											}
										},
										plotOptions: {
											pie: {
												allowPointSelect: true,
												cursor: 'pointer',
												dataLabels: {
													enabled: true,
													format: '<b>{point.name}</b>: {point.y}',
													connectorColor: '#333333'
												}
											}
										},
										credits: {
											text: 'Ketanware',
											href: '#'
										},
										series: [{
											name: 'Jumlah',
											data: [
												{
													name: 'Siswa Aktif', 
													y: <?=$jumlahSiswaAktif;?>,
													color: '#7EB0D2'
												},
												{
													name: 'Siswa Lulus', 
													y: <?=$jumlahSiswaLulus;?>,
													color: '#9BE079'
												},
												{
													name: 'Siswa Keluar', 
													y: <?=$jumlahSiswaKeluar;?>,
													color: '#E88273'
												}
											]
										}]
									});
								</script>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>