<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "kurikulum";

if($_POST['mod']=="simpanData")
{
	$nama_kurikulum = mysql_real_escape_string($_POST['nama_kurikulum']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tingkat = mysql_real_escape_string($_POST['tingkat']);
	$id_jurusan = $_POST['id_jurusan'];
	
	if(empty($_POST['nama_kurikulum']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kurikulum Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tingkat']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tingkat Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_jurusan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jurusan Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (nama_kurikulum, keterangan, tingkat, id_jurusan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_kurikulum', '$keterangan', '$tingkat', '$id_jurusan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Kurikulum";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_kurikulum' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_kurikulum = mysql_real_escape_string($_POST['nama_kurikulum']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tingkat = mysql_real_escape_string($_POST['tingkat']);
	$id_jurusan = $_POST['id_jurusan'];
	
	if(empty($_POST['nama_kurikulum']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kurikulum Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tingkat']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tingkat Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['id_jurusan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jurusan Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_kurikulum = '$nama_kurikulum', keterangan = '$keterangan', tingkat = '$tingkat', id_jurusan = '$id_jurusan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Kurikulum";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_kurikulum' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_kurikulum FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_kurikulum'];
	
	$cekKurikulum = mysql_num_rows(mysql_query("SELECT id_kurikulum FROM nilai WHERE id_kurikulum = '$id'"));
	if($cekKurikulum > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusMapel = mysql_query("DELETE FROM mapel WHERE id_kurikulum = '$id'");
		
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Kurikulum";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekKurikulum = mysql_num_rows(mysql_query("SELECT id_kurikulum FROM nilai WHERE id_kurikulum IN ($data_terpilih)"));
	if($cekKurikulum > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusMapel = mysql_query("DELETE FROM mapel WHERE id_kurikulum IN ($data_terpilih)");
		
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Kurikulum Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Kurikulum Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kurikulum Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="simpanMapel")
{
	$id_kurikulum = $_POST['id_kurikulum'];
	
	if(isset($_POST['kelompok_mapel']))
	{
		$hapusMapelKurikulum = mysql_query("DELETE FROM mapel WHERE id_kurikulum = '$id_kurikulum'");
		
		$x = 0;
		$tersimpan = 0;
		foreach($_POST['kelompok_mapel'] as $kelompok_mapel)
		{	
			$kode_mapel = mysql_real_escape_string($_POST['kode_mapel'][$x]);
			$nama_mapel = mysql_real_escape_string($_POST['nama_mapel'][$x]);
			$kkm = mysql_real_escape_string($_POST['kkm'][$x]);
			$urutan_mapel = mysql_real_escape_string($_POST['urutan_mapel'][$x]);
			
			if(!empty($kode_mapel) and !empty($nama_mapel) and !empty($kkm) and !empty($urutan_mapel))
			{
				$simpanData = mysql_query("INSERT INTO mapel (id_kurikulum, kelompok_mapel, kode_mapel, nama_mapel, kkm, urutan_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kurikulum', '$kelompok_mapel', '$kode_mapel', '$nama_mapel', '$kkm', '$urutan_mapel', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
					
				if($simpanData)
				{
					$aktivitas = "Tambah Mapel Kurikulum";
					$keterangan = mysql_real_escape_string("Menambahkan '$nama_mapel' Pada 'id_kurikulum = $id_kurikulum' Pada Tabel 'mapel'");
					$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
					$tersimpan++;
				}
			}
			
			$x++;
		}
		
		if($tersimpan > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Ditambahkan!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
		
	}
	else
	{
		$cekMapelKurikulum = mysql_num_rows(mysql_query("SELECT id FROM mapel WHERE id_kurikulum = '$id_kurikulum'"));
		if($cekMapelKurikulum > 0)
		{
			$hapusMapelKurikulum = mysql_query("DELETE FROM mapel WHERE id_kurikulum = '$id_kurikulum'");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}
?>