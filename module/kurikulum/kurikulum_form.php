<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM kurikulum WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Kurikulum";}else{echo "Tambah Kurikulum";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Kurikulum</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_kurikulum" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_kurikulum'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="keterangan" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['keterangan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tingkat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tingkat" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['tingkat'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jurusan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_jurusan">
					<?php
					$jurusan = mysql_query("SELECT * FROM jurusan ORDER BY nama_jurusan");
					while($getJurusan = mysql_fetch_array($jurusan))
					{
						$selected = ($getData['id_jurusan'] == $getJurusan['id'] ? "selected" : "");
					?>
						<option value="<?=$getJurusan['id'];?>" <?=$selected;?>><?=$getJurusan['nama_jurusan'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>