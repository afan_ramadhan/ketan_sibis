<?php
include "../../config/database.php";

if($_POST['mod']=="tambahMapel")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM kurikulum WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	function salinMapel()
	{
		var mod = "salinMapel";
		var id_kurikulum = $("#id_kurikulum_").val();
		$.ajax({
			type	: "POST",
			url		: "module/kurikulum/kurikulum_response.php",
			data	: "mod=" + mod +
					  "&id_kurikulum=" + id_kurikulum,
			success: function(response)
			{
				const data = JSON.parse(response);
				
				var rowBaru = "";
				var i;
                
				for(i = 0; i < data.length; i++)
				{
					var nomor = i + 1;
					
					rowBaru += "<tr id='barisKe" + nomor + "'>";
					rowBaru += "	<td class='baris' align='center'>" + nomor + "</td>";
					rowBaru += "	<td>";
					rowBaru += "		<select class='form-control' name='kelompok_mapel[]' required>";
					rowBaru += "			<option value='1' " + ((data[i].kelompok_mapel == 1) ? "selected" : "") + ">Kelompok A</option>";
					rowBaru += "			<option value='2' " + ((data[i].kelompok_mapel == 2) ? "selected" : "") + ">Kelompok B</option>";
					rowBaru += "			<option value='3' " + ((data[i].kelompok_mapel == 3) ? "selected" : "") + ">Kelompok C</option>";
					rowBaru += "			<option value='4' " + ((data[i].kelompok_mapel == 4) ? "selected" : "") + ">Kelompok D</option>";
					rowBaru += "		</select>";
					rowBaru += "	</td>";
					rowBaru += "	<td>";
					rowBaru += "		<input type='text' class='form-control' name='kode_mapel[]' maxlength='20' value='" + data[i].kode_mapel + "'/>";
					rowBaru += "	</td>";
					rowBaru += "	<td>";
					rowBaru += "		<input type='text' class='form-control' name='nama_mapel[]' maxlength='100' value='" + data[i].nama_mapel + "'/>";
					rowBaru += "	</td>";
					rowBaru += "	<td>";
					rowBaru += "		<input type='text' class='form-control' name='kkm[]' maxlength='2' value='" + data[i].kkm + "' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>";
					rowBaru += "	</td>";
					rowBaru += "	<td>";
					rowBaru += "		<input type='text' class='form-control' name='urutan_mapel[]' maxlength='2' value='" + data[i].urutan_mapel + "' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>";
					rowBaru += "	</td>";
					rowBaru += "	<td>";
					rowBaru += "		<button type='button' class='btn btn-danger btn-sm' onclick='hapusRow(" + nomor + ")'><i class='fa fa-trash' aria-hidden='true' style='margin-right: 10px;'></i>Hapus</button>";
					rowBaru += "	</td>";
					rowBaru += "</tr>";
                } 
			
				$("#dataMapel").empty(rowBaru);
				$("#dataMapel").append(rowBaru);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Daftar Mapel</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<select class="form-control" id="id_kurikulum_">
					<?php
					$kurikulum = mysql_query("SELECT * FROM kurikulum WHERE id <> '$id' ORDER BY nama_kurikulum");
					while($getKurikulum = mysql_fetch_array($kurikulum))
					{
					?>
						<option value="<?=$getKurikulum['id'];?>"><?=$getKurikulum['nama_kurikulum'];?> | <?=$getKurikulum['tingkat'];?></option>
					<?php
					}
					?>
				</select>
			</td>
			<td>
				<button type="button" class="btn btn-default" id="salinMapel" onclick="salinMapel()"><i class="fa fa-files-o" aria-hidden="true" style="margin-right: 10px;"></i>Salin Mapel</button>
			</td>
		</tr>
	</table>
	<div class="detail scrolling">
		<form id="formMapel">
			<table class="table table-bordered table-hover data_custom_1">

				<thead>
					<tr>
						<th style="width: 1px;">No.</th>
						<th>Kelompok Mapel</th>
						<th>Kode Mapel</th>
						<th>Nama Mapel</th>
						<th>KKM</th>
						<th>Urutan Mapel</th>
						<th>#</th>
					</tr>
				</thead>
				
				<input type="hidden" name="mod" value="simpanMapel">
				<input type="hidden" name="id_kurikulum" value="<?=$id;?>">

				<tbody id="dataMapel">

					<?php
					$x = 1;
					$mapel = mysql_query("SELECT * FROM mapel WHERE id_kurikulum = '$id' ORDER BY kelompok_mapel, urutan_mapel");
					while($getMapel = mysql_fetch_array($mapel))
					{
						echo "
						<tr id='barisKe$x'>
							<td class='baris' align='center'>$x</td>
							<td>
								<select class='form-control' name='kelompok_mapel[]'>
									<option value='1' ".(($getMapel['kelompok_mapel'] == 1) ? "selected" : "").">Kelompok A</option>
									<option value='2' ".(($getMapel['kelompok_mapel'] == 2) ? "selected" : "").">Kelompok B</option>
									<option value='3' ".(($getMapel['kelompok_mapel'] == 3) ? "selected" : "").">Kelompok C</option>
									<option value='4' ".(($getMapel['kelompok_mapel'] == 4) ? "selected" : "").">Kelompok D</option>
								</select>
							</td>
							<td>
								<input type='text' class='form-control' name='kode_mapel[]' maxlength='100' value='$getMapel[kode_mapel]'/>
							</td>
							<td>
								<input type='text' class='form-control' name='nama_mapel[]' maxlength='100' value='$getMapel[nama_mapel]'/>
							</td>
							<td>
								<input type='text' class='form-control' name='kkm[]' maxlength='2' value='$getMapel[kkm]' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
							</td>
							<td>
								<input type='text' class='form-control' name='urutan_mapel[]' maxlength='2' value='$getMapel[urutan_mapel]' onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
							</td>
							<td>
								<button type='button' class='btn btn-danger btn-sm' onclick='hapusRow($x)'><i class='fa fa-trash' aria-hidden='true' style='margin-right: 10px;'></i>Hapus</button>
							</td>
						</tr>";
						$x++;
					}
					?>
					
				</tbody>
				
			</table>
		</form>
		<button type="button" class="btn btn-default btn-block" onclick="tambahRow()"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah Mapel</button>
	</div>
	
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="tambahMapel")
	{
		echo "<button type='button' class='btn btn-success' id='simpanMapel' onclick='simpanMapel()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>