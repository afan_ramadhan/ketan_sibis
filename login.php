<?php
include "config/database.php";
$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);
?>

<!DOCTYPE html>

<html>

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title></title>
		
		<link rel="icon" type="image/png" href="images/ketanware_2.png">
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">
		<link href="assets/plugins/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/icofont/css/icofont.css" rel="stylesheet" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/alertifyjs/css/alertify.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/alertifyjs/css/themes/default.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/particleground/css/style.css" type="text/css" rel="stylesheet"/>
		<link href="assets/ramadhan_afan/css/ramadhan_afan.css" type="text/css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" type="text/css" rel="stylesheet">
		
	</head>

	<body id="background-login-manajemen">

		<script src="assets/plugins/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/plugins/alertifyjs/alertify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/particleground/jquery.particleground.js" type="text/javascript"></script>
		<script src="assets/plugins/preloaders/jquery.preloaders.js" type="text/javascript"></script>
		<script src="assets/ramadhan_afan/js/ramadhan_afan.js" type="text/javascript"></script>
		
		<script>	
			//Particle Background
			document.addEventListener('DOMContentLoaded', function(){
				particleground(document.getElementById('particles'),{
					dotColor: '#464646',
					lineColor: '#DDDDDD'
				});
				var intro = document.getElementById('intro');
				intro.style.marginTop = - intro.offsetHeight / 2 + 'px';
			}, false);
			
			//Animasi Loading
			$(function(){
				$('.loading').click(function(){
					$.preloader.start();
					setTimeout(function(){$.preloader.stop();}, 2000);
				});
			});
			
			//Ganti Title
			function GantiTitle(){
				document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> | Login Manajemen";
			}
			GantiTitle();
			
			//Login
			function login(){
				var mod = "login";
				var username = $("#username").val();
				var password = $("#password").val();
				$.ajax({
					type	: "POST",
					url		: "verifikasi.php",
					data	: "mod=" + mod +
							  "&username=" + username +
							  "&password=" + password,
					success: function(html)
					{
						$("#notifikasi").html(html);
					}
				})
			}
		</script>
		
		<div id="particles">
			<div id="form-login">
				<!--h2 align="center"><b>Ketan<font class="custom-font-1">Ware</font></b></h2-->
				<img class="img-responsive" src="images/ketanware_3.png"/>
				<h4 align="center"><?=$lihat_konfigurasi['nama_aplikasi'];?> <small><?=$lihat_konfigurasi['versi'];?></small> | Manajemen</h4>
				<br/>
				<div class="form-group">
					<input type="text" id="username" class="form-control input-lg tanpa_spasi" placeholder="Username" required autofocus>
				</div>
				<div class="form-group">
					<input type="password" id="password" class="form-control input-lg tanpa_spasi" placeholder="Password" autocomplete="off" required>
				</div>
				<button type="button" class="btn btn-primary btn-lg btn-block loading" onclick="login()">Login</button>
			</div>
			<div id="notifikasi"></div>
		</div>

		<script>
			var input = document.getElementById("password");
			input.addEventListener("keyup", function(event){
				if(event.keyCode === 13)
				{
					login();
				}
			});
			
			$("input.tanpa_spasi").on({
				keydown: function(e){
				if (e.which === 32)
				return false;
				},
				change: function() {
				this.value = this.value.replace(/\s/g, "");
				}
			});
		</script>

	</body>

</html>