<?php
if($lihat_konfigurasi['kode_aktivasi'] == $_SESSION['verifikasi_aplikasi'])
{
	if(isset($_GET['mod']))
	{
		switch($_GET['mod'])
		{
			case '404':
				include("module/404/404.php");
				break;
			case 'level':
				include("module/level/level.php");
				break;
			case 'user':
				include("module/user/user.php");
				break;
			case 'profil':
				include("module/profil/profil.php");
				break;
			case 'konfigurasi':
				include("module/konfigurasi/konfigurasi.php");
				break;
			case 'riwayat':
				include("module/riwayat/riwayat.php");
				break;
			case 'jurusan':
				include("module/jurusan/jurusan.php");
				break;
			case 'tahun_ajar':
				include("module/tahun_ajar/tahun_ajar.php");
				break;
			case 'kurikulum':
				include("module/kurikulum/kurikulum.php");
				break;
			case 'rombel':
				include("module/rombel/rombel.php");
				break;
			case 'siswa':
				include("module/siswa/siswa.php");
				break;
			case 'nilai':
				include("module/nilai/nilai.php");
				break;
			case 'data_induk':
				include("module/data_induk/data_induk.php");
				break;
			default:
				include ("module/dashboard/dashboard.php");
				break;
		}   
	}
	else
	{
		include ("module/dashboard/dashboard.php");
	}
}
else
{
	include ("module/404/404.php");
}
?>	