<aside class="control-sidebar control-sidebar-dark">
	<div class="tab-content">
		<h3 class="control-sidebar-heading" style="margin-top: 0; font-weight: bold;"><?=$lihat_konfigurasi['nama_instansi'];?></h3>
		<ul class="control-sidebar-menu">
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-map-marker bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Alamat</h4>
						<p><?=$lihat_konfigurasi['alamat'];?></p>
					</div>
				</a>
			</li>
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-address-book bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Telepon</h4>
						<p><?=$lihat_konfigurasi['nomor_telepon'];?></p>
					</div>
				</a>
			</li>
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-at bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Email</h4>
						<p><?=$lihat_konfigurasi['email'];?></p>
					</div>
				</a>
			</li>
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-calendar-o bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Website</h4>
						<p><?=$lihat_konfigurasi['website'];?></p>
					</div>
				</a>
			</li>
		</ul>
		<h3 class="control-sidebar-heading" style="font-weight: bold;">Bantuan & Dukungan</h3>
		<ul class="control-sidebar-menu">
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-code bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Developer</h4>
						<p>KetanWare</p>
					</div>
				</a>
			</li>
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-address-book bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Telepon</h4>
						<p>0896 0991 8353</p>
					</div>
				</a>
			</li>
			<li>
				<a href="javascript:;">
					<i class="menu-icon fa fa-at bg-red"></i>
					<div class="menu-info">
						<h4 class="control-sidebar-subheading">Email</h4>
						<p>ramadhan.afan@gmail.com</p>
					</div>
				</a>
			</li>
		</ul>
	</div>
</aside>