<?php
session_start();
include "config/database.php";

if(empty($_SESSION['id']) and empty($_SESSION['username']) and empty($_SESSION['level']))
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else if($_SESSION['level'] != "manajemen")
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else
{
	$blokir = mysql_query("SELECT blokir FROM user WHERE username = '$_SESSION[username]'");
	$getBlokir = mysql_fetch_array($blokir);
	if($getBlokir['blokir'] == "Y")
	{
		session_destroy();
		
		echo "
		<script>
			window.location.href='login.php';
		</script>";
	}
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$ambilTema = mysql_fetch_array(mysql_query("SELECT tema FROM user WHERE id = '$_SESSION[id]'"));
?>

<!DOCTYPE html>

<html>
	
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen</title>
			
		<link rel="icon" type="image/png" href="images/ketanware_2.png">
		<link href="assets/plugins/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/bootstrap/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/calendar/css/pickmeup.css" rel="stylesheet" type="text/css"/>
		<link href="assets/plugins/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/icofont/css/icofont.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/datatables/datatables.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/alertifyjs/css/alertify.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/alertifyjs/css/themes/default.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/ionicons/css/ionicons.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/plugins/monthly/css/monthly.css" type="text/css" rel="stylesheet"/>
		<link href="assets/adminlte/css/AdminLTE.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/adminlte/css/skins/_all-skins.min.css" type="text/css" rel="stylesheet"/>
		<link href="assets/ramadhan_afan/css/ramadhan_afan.css" type="text/css" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" type="text/css" rel="stylesheet">

	</head>

	<body class="hold-transition <?=$ambilTema[0];?> fixed sidebar-mini">
		
		<script src="assets/plugins/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
		<script src="assets/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="assets/plugins/bootstrap/js/moment.js" type="text/javascript"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
		<script src="assets/plugins/fastclick/lib/fastclick.js"></script>
		<script src="assets/adminlte/js/adminlte.min.js" type="text/javascript"></script>
		<script src="assets/plugins/calendar/js/jquery.pickmeup.js" type="text/javascript"></script>
		<script src="assets/plugins/calendar/js/demo.js" type="text/javascript"></script>	
		<script src="assets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
		<script src="assets/plugins/alertifyjs/alertify.min.js" type="text/javascript"></script>
		<script src="assets/plugins/preloaders/jquery.preloaders.js" type="text/javascript"></script>
		<script src="assets/plugins/monthly/js/monthly.js" type="text/javascript"></script>
		<script src="assets/ramadhan_afan/js/ramadhan_afan.js" type="text/javascript"></script>		
			
		<script>
			//Animasi Loading
			$(function(){
				$('.loading').click(function(){
					$.preloader.start();
					setTimeout(function(){$.preloader.stop();}, 2000);
				});
			});
			
			function mulaiAnimasi()
			{
				$.preloader.start();
			}
			
			function stopAnimasi()
			{
				$.preloader.stop();
			}
		</script>
		
		<div class="wrapper">
		
			<?php
			include "header.php";
			include "menu.php";
			include "content.php";
			include "footer.php";
			include "sidebar.php";
			?>
			
		</div>

	</body>
	
</html>